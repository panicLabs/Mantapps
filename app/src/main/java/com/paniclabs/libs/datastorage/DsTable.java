package com.paniclabs.libs.datastorage;

/**
 * @author panicLabs on 18/1/17.
 */
public class DsTable<T> {
    T content;
    @SuppressWarnings("DsDeclaration") DsTable() {}

    DsTable(T content) {
        this.content = content;
    }
}
