package com.paniclabs.libs.datastorage;

import java.util.List;

/**
 * @author panicLabs on 18/1/17.
 */

public interface Storage {
    List<String> getAll();
    <E> void insert(String key, E value);
    <E> E select(String key);
    boolean isExist(String key);
    void removeIfExist(String key);
    void removeAll();
}
