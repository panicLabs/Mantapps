package com.paniclabs.libs.views;

import android.view.animation.Animation;

/**
 * @author paniclabs.
 * @created on 21.02.2017.
 * @email panic.inc.dev@gmail.com
 * @projectName Mantapps
 */
public class CustomAnimationListener implements Animation.AnimationListener {

    @Override
    public void onAnimationStart(Animation animation) {

    }

    @Override
    public void onAnimationEnd(Animation animation) {

    }

    @Override
    public void onAnimationRepeat(Animation animation) {

    }

}
