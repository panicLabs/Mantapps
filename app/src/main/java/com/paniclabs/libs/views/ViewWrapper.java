package com.paniclabs.libs.views;

import android.support.v7.widget.RecyclerView;
import android.view.View;

/**
 * @author paniclabs.
 * @created on 6/13/17.
 * @email panic.inc.dev@gmail.com
 * @projectName Mantapps
 */
public class ViewWrapper<V extends View> extends RecyclerView.ViewHolder{

    public ViewWrapper(V itemView) {
        super(itemView);
    }
}
