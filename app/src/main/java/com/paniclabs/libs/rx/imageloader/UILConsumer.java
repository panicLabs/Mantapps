package com.paniclabs.libs.rx.imageloader;

import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;

/**
 * @author ali@pergikuliner
 * @created 6/13/17.
 * @project Mantapps.
 */

public abstract class UILConsumer implements Observer<RxUILObservableBuilder.RXUILObject> {
    @Override
    public void onSubscribe(Disposable d) {}

    @Override
    public void onComplete() {}

}
