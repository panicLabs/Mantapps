package com.paniclabs.libs.particleview;

/**
 * @author panicLabs on 09/01/17.
 */
public class Particle {

    public float x;
    public float y;
    public float radius;

    public Particle() {
    }

    public Particle(float x, float y, float radius) {
        this.x = x;
        this.y = y;
        this.radius = radius;
    }
}
