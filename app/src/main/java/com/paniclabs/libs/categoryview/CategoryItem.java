package com.paniclabs.libs.categoryview;

import android.graphics.drawable.Drawable;

import com.mantapps.reader.model.Category;

import java.util.List;

/**
 * Created by viventhraarao on 22/11/2016.
 */

public interface CategoryItem {

    String getName();
    Drawable getImage();
    List<Category> getSubCategoryItems();

}
