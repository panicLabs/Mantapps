package com.paniclabs.libs.categoryview;

/**
 * Created by viventhraarao on 22/11/2016.
 */

public interface VRCategoryItemClickListener {

    void onCategoryItemClicked(CategoryItem categoryItem);

}
