package com.paniclabs.libs.categoryview;

/**
 * @author panicLabs on 01/02/17.
 */
public interface OnCategoryItemClickListener {
    void onCategoryItemSelected(String name, int position, boolean expanded);
}
