//package com.paniclabs.libs.utils;
//
//import com.google.gson.Gson;
//import com.google.gson.reflect.TypeToken;
//import com.mantapps.reader.model.Category;
//
//import java.util.ArrayList;
//import java.util.List;
//
///**
// * @author paniclabs.
// * @created on 1/30/17.
// * @email panic.inc.dev@gmail.com
// * @projectName mtps-n
// * @packageName com.paniclabs.libs.utils.
// * @className ${CLASS}.
// */
//public class CacheUtil {
//
//    private static ACache mAcache;
//    private static Gson gson = new Gson();
//
//    public CacheUtil(ACache aCache){
//        CacheUtil.mAcache = aCache;
//    }
//
//    public static CacheUtil init(ACache aCache){
//        CacheUtil.mAcache = aCache;
//        return new CacheUtil(mAcache);
//    }
//
//    public static void addSearchCache(List<Category> searchModels){
//        String json = gson.toJson(searchModels);
//        mAcache.put("sh",json);
//    }
//
//    public static List<Category> addList(String key){
//        List<Category> searchModels;
//        if(mAcache.getAsString("sh")!=null){
//            searchModels = gson.fromJson(mAcache.getAsString("sh"),new TypeToken<List<Category>>() {
//            }.getType());
//            for (int i=0;i<searchModels.size();i++){
//                if(searchModels.get(i).getKey().equals(key)){
//                    return searchModels;
//                }
//            }
//            Category searchModel = new Category(searchModels.size());
//            searchModel.setKey(key);
//            searchModels.add(searchModel);
//            return searchModels;
//        }
//        else {
//            return searchModels = new ArrayList<>();
//        }
//    }
//
//    public static List<Category> getList(){
//        List<Category> searchModels;
//        if(mAcache.getAsString("sh")!=null){
//            searchModels = gson.fromJson(mAcache.getAsString("sh"),new TypeToken<List<Category>>() {
//            }.getType());
//            return searchModels;
//        }
//        else {
//            return searchModels = new ArrayList<>();
//        }
//    }
//}
