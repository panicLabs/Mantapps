package com.mantapps.reader.glide;

/**
 * @author panicLabs on 05/01/17.
 */

public interface ImageLoaderListener {
    void onSuccess();

    void onError();
}