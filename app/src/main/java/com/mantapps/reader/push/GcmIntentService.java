///*
// * Copyright (C) 2013 The Android Open Source Project
// *
// * Licensed under the Apache License, Version 2.0 (the "License");
// * you may not use this file except in compliance with the License.
// * You may obtain a copy of the License at
// *
// *      http://www.apache.org/licenses/LICENSE-2.0
// *
// * Unless required by applicable law or agreed to in writing, software
// * distributed under the License is distributed on an "AS IS" BASIS,
// * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// * See the License for the specific language governing permissions and
// * limitations under the License.
// */
//
//package com.mantapps.reader.push;
//
//import com.google.android.gms.gcm.GoogleCloudMessaging;
//
//import android.app.IntentService;
//import android.app.NotificationManager;
//import android.app.PendingIntent;
//import android.app.admin.DevicePolicyManager;
//import android.content.ComponentName;
//import android.content.Context;
//import android.content.Intent;
//import android.media.AudioManager;
//import android.net.Uri;
//import android.os.Bundle;
//import android.support.v4.app.NotificationCompat;
//import android.util.Log;
//
//import com.mantapps.reader.app.MainActivity;
//import com.mantapps.reader.R;
//
//import java.util.Random;
//
///**
// * This {@code IntentService} does the actual handling of the GCM message.
// * {@code GcmBroadcastReceiver} (a {@code WakefulBroadcastReceiver}) holds a
// * partial wake lock for this service while the service does its work. When the
// * service is finished, it calls {@code completeWakefulIntent()} to release the
// * wake lock.
// */
//public class GcmIntentService extends IntentService {
//
//    public static final int NOTIFICATION_ID = 1;
//    private NotificationManager mNotificationManager;
//    NotificationCompat.Builder builder;
//    private AudioManager mAudioManager;
//    private static int mOrigVolume;
//    private int mMaxVolume;
//    private Uri mAlarmSound;
//    //private MediaPlayer mMediaPlayer;
//
//    public GcmIntentService() {
//        super("GcmIntentService");
//    }
//
//    public static final String TAG = "GCM Demo";
//    DevicePolicyManager mDPM;
//    ComponentName mDeviceAdminSample;
//
//    /**
//     * Helper to determine if we are an active admin
//     */
//    private boolean isActiveAdmin() {
//        return mDPM.isAdminActive(mDeviceAdminSample);
//    }
//
//    @Override
//    protected void onHandleIntent(Intent intent) {
//
//        Bundle extras = intent.getExtras();
//        GoogleCloudMessaging gcm = GoogleCloudMessaging.getInstance(this);
//        // The getMessageType() intent parameter must be the intent you received
//        // in your BroadcastReceiver.
//        String messageType = gcm.getMessageType(intent);
//
//        if (!extras.isEmpty()) {  // has effect of unparcelling Bundle
//            /*
//             * Filter messages based on message type. Since it is likely that GCM will be
//             * extended in the future with new message types, just ignore any message types you're
//             * not interested in, or that you don't recognize.
//             */
//            if (GoogleCloudMessaging.MESSAGE_TYPE_SEND_ERROR.equals(messageType)) {
//                Log.e(TAG, "Send error: " + extras.toString());
//                //sendNotification("Send error: " + extras.toString());
//            } else if (GoogleCloudMessaging.MESSAGE_TYPE_DELETED.equals(messageType)) {
//                Log.e(TAG, "Deleted messages on server: " + extras.toString());
//                //sendNotification("Deleted messages on server: " + extras.toString());
//                // If it's a regular GCM message, do some work.
//            } else if (GoogleCloudMessaging.MESSAGE_TYPE_MESSAGE.equals(messageType)) {
//                // This loop represents the service doing some work.
//                // Post notification of received message.
//                String msg = extras.getString("message");
//                sendNotification(msg);
//                Log.i(TAG, "Received: " + extras.toString());
//            }
//        }
//        // Release the wake lock provided by the WakefulBroadcastReceiver.
//        GcmBroadcastReceiver.completeWakefulIntent(intent);
//    }
//
//    // Put the message into a notification and post it.
//    // This is just one simple example of what you might choose to do with
//    // a GCM message.
//    private void sendNotification(String msg) {
//
//        Random random = new Random(System.currentTimeMillis());
//        int flagid = random.nextInt();
//        NotificationManager mNotificationManager = (NotificationManager)
//                getSystemService(Context.NOTIFICATION_SERVICE);
//        Intent intent = new Intent(this, MainActivity.class);
//        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//        intent.setFlags(Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
//        intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
//        intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
//        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//
//        PendingIntent contentIntent = PendingIntent.getActivity(this, flagid, intent
//                , PendingIntent.FLAG_CANCEL_CURRENT);
//
//        NotificationCompat.Builder mBuilder =
//                new NotificationCompat.Builder(this).setDefaults(NotificationCompat.DEFAULT_ALL)
//                        .setAutoCancel(true)
//                        .setSmallIcon(R.drawable.ic_launcher)
//                        .setContentTitle(getResources().getString(R.string.app_name))
//                        .setStyle(new NotificationCompat.BigTextStyle()
//                                .bigText(msg))
//                        .setContentText(msg);
//
//        mBuilder.setContentIntent(contentIntent);
//        mNotificationManager.cancel(flagid);
//        mNotificationManager.notify(flagid, mBuilder.build());
//
//    }
//
//}
