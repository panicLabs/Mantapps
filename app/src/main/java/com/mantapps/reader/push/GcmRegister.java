//package com.mantapps.reader.push;
//
//import com.google.android.gms.gcm.GoogleCloudMessaging;
//
//import android.content.Context;
//import android.content.SharedPreferences;
//import android.content.pm.PackageInfo;
//import android.content.pm.PackageManager;
//import android.os.Handler;
//import android.util.Log;
//
//import com.mantapps.reader.app.AppController;
//import com.mantapps.reader.push.model.PushModel;
//import com.mantapps.reader.util.StringUtils;
//
//import java.io.IOException;
//import java.util.concurrent.atomic.AtomicInteger;
//
///**
// * activation.himax.co.id/gcm/pushreg.php?deviceid&userid post这两个字段到服务器
// * Created by gus on 15/1/4.
// */
//public class GcmRegister {
//
//    public static final String GCM_REGISTER_PREFERENCE = "gcm_preference";
//    public static final String PROPERTY_REG_ID = "registration_id";
//    public static final String PROPERTY_USER_ID = "user_id";
//    private static final String PROPERTY_APP_VERSION = "appVersion";
//    private final String TAG = GcmRegister.class.getName();
//
//    String SENDER_ID = "";// "Your-Sender-ID"; 284590976530
//
//    GoogleCloudMessaging gcm;
//    AtomicInteger msgId = new AtomicInteger();
//    String regid;
//    Handler h = new Handler();
//
//    public void registerPush(String userid) {
//        if (StringUtils.isEmpty(userid)) {
//            return;
//        }
//        gcm = GoogleCloudMessaging.getInstance(AppController.getInstance());
//        regid = getRegistrationId(AppController.getInstance());
//
//        //if (regid.isEmpty()) {
//            registerInBackground(userid);
//        //} else {
//        //    registerRequest(userid, regid);
//        //}
//    }
//
//    /**
//     * Stores the registration ID and the app versionCode in the application's
//     * {@code SharedPreferences}.
//     *
//     * @param context application's context.
//     * @param regId registration ID
//     */
//    private void storeRegistrationId(Context context, String regId) {
//        final SharedPreferences prefs = getGcmPreferences(context);
//        int appVersion = getAppVersion(context);
//        Log.i(TAG, "Saving regId on app version " + appVersion);
//        SharedPreferences.Editor editor = prefs.edit();
//        editor.putString(PROPERTY_REG_ID, regId);
//        editor.putInt(PROPERTY_APP_VERSION, appVersion);
//        editor.commit();
//    }
//
//    private void storeUserid(Context context, String userid) {
//        final SharedPreferences prefs = getGcmPreferences(context);
//        int appVersion = getAppVersion(context);
//        Log.i(TAG, "Saving userid on app version " + appVersion);
//        SharedPreferences.Editor editor = prefs.edit();
//        editor.putString(PROPERTY_USER_ID, userid);
//        editor.commit();
//    }
//
//    public String getUserid(Context context) {
//        final SharedPreferences prefs = getGcmPreferences(context);
//        String userid = prefs.getString(PROPERTY_USER_ID, "");
//        if (userid.isEmpty()) {
//            Log.i(TAG, "Registration not found.");
//            return "";
//        }
//        // Check if app was updated; if so, it must clear the registration ID
//        // since the existing regID is not guaranteed to work with the new
//        // app version.
//        int registeredVersion = prefs.getInt(PROPERTY_APP_VERSION, Integer.MIN_VALUE);
//        int currentVersion = getAppVersion(context);
//        if (registeredVersion != currentVersion) {
//            Log.i(TAG, "App version changed.");
//            return "";
//        }
//        return userid;
//    }
//
//    /**
//     * Gets the current registration ID for application on GCM service, if there is one.
//     * <p>
//     * If result is empty, the app needs to register.
//     *
//     * @return registration ID, or empty string if there is no existing
//     *         registration ID.
//     */
//    private String getRegistrationId(Context context) {
//        final SharedPreferences prefs = getGcmPreferences(context);
//        String registrationId = prefs.getString(PROPERTY_REG_ID, "");
//        if (registrationId.isEmpty()) {
//            Log.i(TAG, "Registration not found.");
//            return "";
//        }
//        // Check if app was updated; if so, it must clear the registration ID
//        // since the existing regID is not guaranteed to work with the new
//        // app version.
//        int registeredVersion = prefs.getInt(PROPERTY_APP_VERSION, Integer.MIN_VALUE);
//        int currentVersion = getAppVersion(context);
//        if (registeredVersion != currentVersion) {
//            Log.i(TAG, "App version changed.");
//            return "";
//        }
//        return registrationId;
//    }
//
//    /**
//     * @return Application's version code from the {@code PackageManager}.
//     */
//    private static int getAppVersion(Context context) {
//        try {
//            PackageInfo packageInfo = context.getPackageManager()
//                    .getPackageInfo(context.getPackageName(), 0);
//            return packageInfo.versionCode;
//        } catch (PackageManager.NameNotFoundException e) {
//            // should never happen
//            throw new RuntimeException("Could not get package name: " + e);
//        }
//    }
//
//    /**
//     * @return Application's {@code SharedPreferences}.
//     */
//    private SharedPreferences getGcmPreferences(Context context) {
//        // This sample app persists the registration ID in shared preferences, but
//        // how you store the regID in your app is up to you.
//        return context.getSharedPreferences(GCM_REGISTER_PREFERENCE,
//                Context.MODE_PRIVATE);
//    }
//
//    //注册成功
//    private void registerRequest(final String userid, final String pushid) {
//        //TODO:处理注册成功
//        //new PushModel().register(userid, pushid);
//        new PushModel().register(pushid);
//    }
//
//    /**
//     * Registers the application with GCM servers asynchronously.
//     * <p>
//     * Stores the registration ID and the app versionCode in the application's
//     * shared preferences.
//     */
//    private void registerInBackground(final String userid) {
//
//        new Thread(new Runnable() {
//            @Override
//            public void run() {
//                String msg = "";
//                try {
//                    if (gcm == null) {
//                        gcm = GoogleCloudMessaging.getInstance(AppController.getInstance());
//                    }
//
//                    try {
//                        SENDER_ID = AppController.getInstance().getPackageManager().getApplicationInfo(
//                                AppController.getInstance().getPackageName(),PackageManager.GET_META_DATA).
//                                metaData.getString("com.google.gms.send.id");
//                    } catch (PackageManager.NameNotFoundException e) {
//                        e.printStackTrace();
//                    }
//                    Log.i(TAG, "send_id:" + SENDER_ID);
//                    regid = gcm.register(SENDER_ID);
//                    msg = "Device registered, registration ID=" + regid;
//                    Log.i(TAG, msg);
//                    storeRegistrationId(AppController.getInstance(), regid);
//                    storeUserid(AppController.getInstance(), userid);
//                    h.post(new Runnable() {
//                        @Override
//                        public void run() {
//                            registerRequest(userid, regid);
//                        }
//                    });
//                    // For this demo: we don't need to send it because the device will send
//                    // upstream messages to a server that echo back the message using the
//                    // 'from' address in the message.
//
//                    // Persist the regID - no need to register again.
//
//                } catch (IOException ex) {
//                    msg = "Error :" + ex.getMessage();
//                    Log.e(TAG, msg);
//                    // If there is an error, don't just keep trying to register.
//                    // Require the user to click a button again, or perform
//                    // exponential back-off.
//                }
//            }
//        }).start();
//
//    }
//
//}
