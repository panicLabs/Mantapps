package com.mantapps.reader.push.model;

import com.elvishew.xlog.XLog;
import com.mantapps.reader.api.MantappsApi;
import com.mantapps.reader.app.AppController;
import com.mantapps.reader.util.Config;
import com.mantapps.reader.util.RestUtil;

import org.json.JSONObject;

import java.io.IOException;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by shaolin on 2015/7/6.
 */
public class PushModel {
    private static final String TAG = PushModel.class.getName();
    /**
     * 登录push服务
     */
    public void register(String devid) {
        String url = Config.PUSH + "device_token=" + devid + "&device_type=android&channels_id=1";
        RestUtil.getInstance().create(MantappsApi.class).getGCM(url).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()){
                    try {
                        XLog.w(TAG + response.body().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }

}
