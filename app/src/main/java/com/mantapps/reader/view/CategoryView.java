package com.mantapps.reader.view;

import com.mantapps.reader.model.Category;
import com.mantapps.reader.model.Post;

import java.util.ArrayList;

/**
 * Created by shaolin on 2015/8/12.
 */
public interface CategoryView {

    public void showProgress();

    public void hideProgress();

    public void onSuccessCategories(ArrayList<Category> firstLevel, ArrayList<Category> secondLevel);

    public void onSuccessPosts(ArrayList<Post> list);

    public void onFailure(String errMsg);

}
