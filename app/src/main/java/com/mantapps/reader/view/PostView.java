package com.mantapps.reader.view;

/**
 * Created by gus on 15/8/19.
 */
public interface PostView {

    public void showProgress();

    public void onRefreshData();

    public void hideProgress();
}
