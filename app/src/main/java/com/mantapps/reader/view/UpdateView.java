package com.mantapps.reader.view;

import com.mantapps.reader.model.VersionInfo;

/**
 * Created by shaolin on 2015/8/26.
 */
public interface UpdateView {

    public void showProgress();

    public void hideProgress();

    public void onSuccess(VersionInfo info);

    public void onFailure(String errMsg);

}
