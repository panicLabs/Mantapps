package com.mantapps.reader.view;

/**
 * Created by gus on 15/8/13.
 */
public interface CategoryImageView {
    public void onSuccessCategoryTempPicUrl(String url);
}
