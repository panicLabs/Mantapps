////
//// Source code recreated from a .class file by IntelliJ IDEA
//// (powered by Fernflower decompiler)
////
//
//package com.mantapps.reader.view;
//
//import android.content.Context;
//import android.graphics.Bitmap;
//import android.text.TextUtils;
//import android.util.AttributeSet;
//import android.view.animation.Animation;
//import android.widget.ImageView;
//
//
///**
// * 显示图片时会有动画效果
// */
//public class DisplayerAnimationNetworkImageView extends ImageView {
//    private String mUrl;
//    private int mDefaultImageId;
//    private int mErrorImageId;
//
//    private Animation imageDisplayerAnimation;
//    public DisplayerAnimationNetworkImageView(Context context) {
//        this(context, (AttributeSet)null);
//    }
//
//    public DisplayerAnimationNetworkImageView(Context context, AttributeSet attrs) {
//        this(context, attrs, 0);
//    }
//
//    public DisplayerAnimationNetworkImageView(Context context, AttributeSet attrs, int defStyle) {
//        super(context, attrs, defStyle);
//    }
//
//    public void setImageUrl(String url, ImageLoader imageLoader) {
//        this.mUrl = url;
//        this.mImageLoader = imageLoader;
//        this.loadImageIfNecessary(false);
//    }
//
//    public void setDefaultImageResId(int defaultImage) {
//        this.mDefaultImageId = defaultImage;
//    }
//
//    public void setErrorImageResId(int errorImage) {
//        this.mErrorImageId = errorImage;
//    }
//
//    void loadImageIfNecessary(final boolean isInLayoutPass) {
//        int width = this.getWidth();
//        int height = this.getHeight();
//        boolean wrapWidth = false;
//        boolean wrapHeight = false;
//        if(this.getLayoutParams() != null) {
//            wrapWidth = this.getLayoutParams().width == -2;
//            wrapHeight = this.getLayoutParams().height == -2;
//        }
//
//        boolean isFullyWrapContent = wrapWidth && wrapHeight;
//        if(width != 0 || height != 0 || isFullyWrapContent) {
//            if(TextUtils.isEmpty(this.mUrl)) {
//                if(this.mImageContainer != null) {
//                    this.mImageContainer.cancelRequest();
//                    this.mImageContainer = null;
//                }
//
//                this.setDefaultImageOrNull();
//            } else {
//                if(this.mImageContainer != null && this.mImageContainer.getRequestUrl() != null) {
//                    if(this.mImageContainer.getRequestUrl().equals(this.mUrl)) {
//                        return;
//                    }
//
//                    this.mImageContainer.cancelRequest();
//                    this.setDefaultImageOrNull();
//                }
//
//                int maxWidth = wrapWidth?0:width;
//                int maxHeight = wrapHeight?0:height;
//                ImageContainer newContainer = this.mImageLoader.get(this.mUrl, new ImageListener() {
//                    public void onErrorResponse(VolleyError error) {
//                        if(DisplayerAnimationNetworkImageView.this.mErrorImageId != 0) {
//                            DisplayerAnimationNetworkImageView.this.setImageResource(DisplayerAnimationNetworkImageView.this.mErrorImageId);
//                        }
//
//                    }
//
//                    public void onResponse(final ImageContainer response, boolean isImmediate) {
//                        if(isImmediate && isInLayoutPass) {
//                            DisplayerAnimationNetworkImageView.this.post(new Runnable() {
//                                public void run() {
//                                    onResponse(response, false);
//                                }
//                            });
//                        } else {
//                            if(response.getBitmap() != null) {
//                                DisplayerAnimationNetworkImageView.this.setImageBitmap(response.getBitmap());
//                                if (imageDisplayerAnimation != null) {
//                                    startAnimation(imageDisplayerAnimation);
//                                }
//                            } else if(DisplayerAnimationNetworkImageView.this.mDefaultImageId != 0) {
//                                DisplayerAnimationNetworkImageView.this.setImageResource(DisplayerAnimationNetworkImageView.this.mDefaultImageId);
//                            }
//
//                        }
//                    }
//                }, maxWidth, maxHeight);
//                this.mImageContainer = newContainer;
//            }
//        }
//    }
//
//    private void setDefaultImageOrNull() {
//        if(this.mDefaultImageId != 0) {
//            this.setImageResource(this.mDefaultImageId);
//        } else {
//            this.setImageBitmap((Bitmap)null);
//        }
//
//    }
//
//    /**
//     * 设置图片显示动画
//     */
//    public void setImageDisplayerAnimation(Animation animation) {
//        imageDisplayerAnimation = animation;
//    }
//
//    protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
//        super.onLayout(changed, left, top, right, bottom);
//        this.loadImageIfNecessary(true);
//    }
//
//    protected void onDetachedFromWindow() {
//        if(this.mImageContainer != null) {
//            this.mImageContainer.cancelRequest();
//            this.setImageBitmap((Bitmap)null);
//            this.mImageContainer = null;
//        }
//
//        super.onDetachedFromWindow();
//    }
//
//    protected void drawableStateChanged() {
//        super.drawableStateChanged();
//        this.invalidate();
//    }
//}
