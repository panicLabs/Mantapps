package com.mantapps.reader.view;

import android.view.View;

/**
 * Created by paniclabs on 11/16/16.
 */

public interface OnWebViewClicked {
    void onClick(View view, String url);

    void onOpenWeb(String extra);
}
