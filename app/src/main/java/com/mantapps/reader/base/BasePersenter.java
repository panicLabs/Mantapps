package com.mantapps.reader.base;

/**
 * @author panicLabs
 * on 2017/1/10.
 */

public interface BasePersenter {

    void start();

}
