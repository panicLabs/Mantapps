package com.mantapps.reader.base;

/**
 * @author paniclabs.
 * @created on 1/24/17.
 * @email panic.inc.dev@gmail.com
 * @projectName mantapps_mvp
 * @packageName com.mantapps.reader.base.
 * @className ${CLASS}.
 */
public interface BasePresenter {
    void start();
}
