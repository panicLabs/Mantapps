package com.mantapps.reader.base;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.mantapps.reader.R;

import butterknife.ButterKnife;


/**
 * @author panicLabs
 * on 2017/1/8.
 */
public abstract class BaseFragment extends Fragment {
    protected BaseActivity mActivity;

    protected boolean mHidden;

    protected abstract void initView(View view, Bundle savedInstanceState);

    protected abstract int getLayoutId();

    protected BaseActivity getHoldingActivity() {
        return mActivity;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(getLayoutId(), container, false);
        ButterKnife.bind(this,view);
        setPresenter();
        initView(view, savedInstanceState);
        return view;
    }

    protected abstract void setPresenter();

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        Activity act = this.getActivity();
        if (act instanceof BaseActivity) {
            this.mActivity = (BaseActivity) act;
        }
    }

    protected void addFragment(BaseFragment fragment) {
        if (null != fragment) {
            getHoldingActivity().addFragment(fragment);
        }
    }

    protected void removeFragment() {
        getHoldingActivity().removeFragment();
    }


    public boolean getHiddenState() {
        return mHidden;
    }

    @Override
    public void onResume() {
        super.onResume();
        mHidden = true;
    }

    @Override
    public void onPause() {
        super.onPause();
        mHidden = false;
    }

    @Override
    public void startActivity(Intent intent) {
        super.startActivity(intent);
        getActivity().overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_right);
    }

    @Override
    public void startActivity(Intent intent, @Nullable Bundle options) {
        super.startActivity(intent, options);
        getActivity().overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_right);
    }

    protected void replaceFragment(int id,BaseFragment fragment) {
        if (fragment != null) {
            getActivity().getSupportFragmentManager().beginTransaction()
                    .replace(id, fragment, fragment.getClass().getSimpleName())
                    .addToBackStack(fragment.getClass().getSimpleName())
                    .commitAllowingStateLoss();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }
}
