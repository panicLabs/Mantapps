package com.mantapps.reader.base;

/**
 * @author panicLabs
 * on 2017/1/12.
 */

public interface SecondPersenter<T> extends BasePersenter {

    void loadData(T t);

}
