package com.mantapps.reader.base;

/**
 * @author panicLabs
 * on 2017/1/12.
 */

public interface SecondView<T> extends BaseView<T> {
    void hideDialog();

    void showDialog();
}
