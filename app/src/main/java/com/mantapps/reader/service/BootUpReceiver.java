package com.mantapps.reader.service;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

/**
 * @author paniclabs.
 * @created on 1/31/17.
 * @email panic.inc.dev@gmail.com
 * @projectName mtps-n
 * @packageName com.mantapps.reader.service.
 * @className ${CLASS}.
 */
public class BootUpReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        Intent serviceStartIntent = new Intent(context, FeedService.class);
        context.startService(serviceStartIntent);
    }
}
