package com.mantapps.reader.service;

/**
 * @author paniclabs.
 * @created on 1/31/17.
 * @email panic.inc.dev@gmail.com
 * @projectName mtps-n
 * @packageName com.mantapps.reader.service.
 * @className ${CLASS}.
 */
public class FeedDownloadFailedException extends Exception{
    public FeedDownloadFailedException() {
        super();
    }

    public FeedDownloadFailedException(String message) {
        super(message);
    }

    public FeedDownloadFailedException(String detailMessage, Throwable throwable) {
        super(detailMessage, throwable);
    }

    public FeedDownloadFailedException(Throwable throwable) {
        super(throwable);
    }
}