//package com.mantapps.reader.mvp.home;
//
//import android.app.Activity;
//import android.app.ProgressDialog;
//import android.os.Bundle;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//import android.widget.ImageView;
//import android.widget.TextView;
//
//import com.elvishew.xlog.XLog;
//import com.mantapps.reader.R;
//import com.mantapps.reader.base.BaseFragment;
//import com.mantapps.reader.model.Post;
//import com.mantapps.reader.mvp.adapter.HomeAdapter;
//import com.mantapps.reader.view.LoadingView;
//import com.mtburn.android.sdk.AppDavis;
//import com.mtburn.android.sdk.instream.ADVSInstreamAdPlacer;
//import com.mtburn.android.sdk.instream.ADVSInstreamAdPlacerListener;
//import com.mtburn.android.sdk.instream.InstreamAdViewBinderImpl;
//import com.mtburn.android.sdk.model.ADVSInstreamInfoModel;
//
//import java.util.ArrayList;
//import java.util.HashMap;
//import java.util.List;
//
//import butterknife.BindView;
//
///**
//* Created by MVPHelper on 2017/01/26
//*/
//
//public class HomeFragment extends BaseFragment implements HomeContract.View, ADVSInstreamAdPlacerListener {
//
//	private HomeContract.Presenter mPresenter;
//
////	@BindView(R.id.id_flipview)
////	FlipView mFlipView;
//	@BindView(R.id.loadingView)
//    LoadingView progressBar;
//
//	// (2) Define ADVSInstreamAdPlacer
//	private ADVSInstreamAdPlacer adPlacer;
//	private List<Object> news;
//	private HomeAdapter mAdapter;
//    private OnHomeSelectedListener mListener;
//    private int mPage = 0;
//    private ProgressDialog dialog;
//    private List<Post> mList ;
//	private int flipto;
//
//
//	public interface OnHomeSelectedListener {
//        void onFlipSelected(ImageView id_photo, HashMap<String, String> map);
//    }
//
//
//
//    @Override
//	protected void initView(View view, Bundle savedInstanceState) {
//		news = new ArrayList<>();
//        mList = new ArrayList<>();
//		setupSDK();
//		mAdapter = new HomeAdapter(getActivity(),news,adPlacer,mListener);
//		mPresenter.start();
//        dialog = new ProgressDialog(getActivity());
//        dialog.setMessage("Loading...");
//        dialog.setCancelable(false);
//        dialog.setCanceledOnTouchOutside(true);
//		/** Initialize Native Ad views **/
////		imgFreeApp = (ImageView) view.findViewById(R.id.imgFreeApp);
////		txtFreeApp = (TextView) view.findViewById(R.id.txtFreeApp);
////		if (txtFreeApp != null) {
////			txtFreeApp.setText("Loading Native Ad...");
////		}
//
////		mFlipView.setVisibility(View.VISIBLE);
////		mFlipView.setAdapter(mAdapter);
////		mFlipView.requestLayout();
////        mFlipView.setOnFlipListener(new FlipView.OnFlipListener() {
////            @Override
////            public void onFlippedToPage(FlipView v, int position, long id) {
////                XLog.e("position " + position +
////                "\nsize = " + (mList.size() - 1));
//////                if (position == 0) {
//////                    swipeRefreshLayout.setEnabled(true);
//////                } else {
//////                    swipeRefreshLayout.setEnabled(false);
//////                }
////                if (position == flipto - 1) {
////					flipto = mList.size() * 2;
////                    mPresenter.loadMore(++mPage);
////                }
////            }
////        });
//
//	}
//
//	private void setupSDK() {
//		// (3) Initialize AppDavis SDK
//		AppDavis.init(getContext(), getString(R.string.media_id));
//		// (4) Generate ADVSInstreamAdPlacer
//		adPlacer = AppDavis.createInstreamAdPlacer(getContext(),
//				getString(R.string.spot_id));
//		// (5) Allocate the advertising project information to your chosen
//		// View (refer to the following parameter items used to display
//		// custom in-feed advertisements)
//
//		InstreamAdViewBinderImpl adViewBinder;
//		adViewBinder= new InstreamAdViewBinderImpl(getContext()){
//			@Override
//			public View createView(ViewGroup parent, int layoutId)
//			{
//				View view = LayoutInflater.from(getContext())
//						.inflate(R.layout.custom_instream_ad_view, parent, false);
//				AdViewHolder holder = new AdViewHolder(view);
//				view.setTag(holder);
//				return view;
//			}
//			@Override
//			public void bindAdData(View v, ADVSInstreamInfoModel adData) {
//				AdViewHolder holder = (AdViewHolder) v.getTag();
//
//				holder.setData(adData);
//				loadAdImage(adData, holder.adImage, null);
//			}
//		};
//
//		adPlacer.registerAdViewBinder(adViewBinder);
//
//		adPlacer.setAdListener(this);
//
//		// (6) Start loading the in-feed advertisement
//
//	}
//
//	@Override
//	protected int getLayoutId() {
//		return R.layout.fragment_flip_new;
//	}
//
//    @Override
//    protected void setPresenter() {
//        mPresenter = new HomePresenter(this);
//    }
//
//    @Override
//	public void tampilkanData(List<Post> posts) {
//        mList = posts;
//		XLog.w(String.valueOf(posts.size()));
//		mAdapter.dataSourceList.addAll(posts);
//		mAdapter.notifyDataSetChanged();
//		adPlacer.loadAd();
//
//	}
//
//	@Override
//	public void onErrorView() {
//	//	mFlipView.setVisibility(View.GONE);
//	}
//
//	@Override
//	public void hideLoading() {
//        progressBar.setLoading(false);
//		//progressBar.setVisibility(View.GONE);
//	}
//
//	@Override
//	public void showLoading() {
//		progressBar.setLoading(true);
//	}
//
//    @Override
//    public void showDialog() {
//        dialog.show();
//    }
//
//    @Override
//    public void hideDialog() {
//        dialog.dismiss();
//    }
//
//	@Override
//	public void onLoadMoreSukses(ArrayList<Post> posts) {
//		mAdapter.dataSourceList.addAll(posts);
//		mAdapter.notifyDataSetChanged();
//		//mFlipView.smoothFlipTo(flipto);
//	}
//
//
//	@Override
//	public void onAdsLoaded(List<? extends ADVSInstreamInfoModel>items) {
//		XLog.w("onAdsLoaded: ");
//
//		int i = 1;
//		for (ADVSInstreamInfoModel adData : items) {
//			mAdapter.dataSourceList.add(i, adData);
//			i += 3;
//		}
////		mAdapter.notifyDataSetChanged();
//	}
//	@Override
//	public void onAdMainImageLoaded(String imageUrl) {
//		XLog.w(imageUrl);
//	}
//	@Override
//	public void onAdIconImageLoaded(String imageUrl) {
//		XLog.w(imageUrl);
//	}
//	@Override
//	public void onAdsLoadedFail(String errorString) {
//		XLog.w(errorString);
//	}
//	@Override
//	public void onAdImageLoadedFail(String imageUrl, String errorString) {
//		XLog.w(errorString);
//	}
//	@Override
//	public void onAdClicked(String redirectURL) {
//		XLog.w(redirectURL);
//	}
//
//
//	// (7) Use  ViewHolder pattern to improve performance.
//	static class AdViewHolder {
//		TextView advertiserName;
//		TextView adText;
//		ImageView adImage;
//
//		public AdViewHolder(View convertView) {
//			advertiserName = (TextView) convertView.findViewById(R.id.custom_instream_advertiser_name);
//			adText = (TextView) convertView.findViewById(R.id.custom_instream_ad_text);
//			adImage = (ImageView) convertView.findViewById(R.id.custom_instream_ad_image);
//		}
//
//		void setData(ADVSInstreamInfoModel adData) {
//			advertiserName.setText(adData.title());
//			adText.setText(adData.content());
//		}
//	}
//
//    @Override
//    public void onDestroy() {
//        super.onDestroy();
//    }
//
//    @Override
//    public void onAttach(Activity activity) {
//        super.onAttach(activity);
//        try {
//            mListener = (OnHomeSelectedListener) activity;
//        } catch (ClassCastException e) {
//            throw new ClassCastException(activity.toString()
//                    + " must implement OnFragmentInteractionListener");
//        }
//    }
//
//	/**
//	 * Part of the activity's life cycle, StartAppAd should be integrated here.
//	 */
//	@Override
//	public void onResume() {
//		super.onResume();
//		//startAppAd.onResume();
//	}
//
//	/**
//	 * Part of the activity's life cycle, StartAppAd should be integrated here
//	 * for the home button exit ad integration.
//	 */
//	@Override
//	public void onPause() {
//		super.onPause();
//		//startAppAd.onPause();
//	}
//
//}