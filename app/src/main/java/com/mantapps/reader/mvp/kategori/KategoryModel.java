package com.mantapps.reader.mvp.kategori;

import com.google.gson.annotations.SerializedName;
import com.mantapps.reader.model.Post;

import java.util.List;

/**
 * @author paniclabs.
 * @created on 6/14/17.
 * @email panic.inc.dev@gmail.com
 * @projectName Mantapps
 */
public class KategoryModel {

    @SerializedName("posts")
    private List<Post> posts;

    public List<Post> getPosts() {
        return posts;
    }

    public void setPosts(List<Post> posts) {
        this.posts = posts;
    }
}
