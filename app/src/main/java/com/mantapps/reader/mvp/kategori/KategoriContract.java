package com.mantapps.reader.mvp.kategori;

import com.mantapps.reader.base.BasePresenter;
import com.mantapps.reader.base.BaseView;
import com.mantapps.reader.model.Post;
import com.mantapps.reader.mvp.category.CategoryContract;
import com.mantapps.reader.mvp.category.CategoryPresenter;
import com.mantapps.reader.mvp.category.model.CategoryIndexResponse;

import java.util.ArrayList;

/**
 * @author ali@pergikuliner
 * @created 6/13/17.
 * @project Mantapps.
 */

public class KategoriContract {

    interface View extends BaseView<KategoriContract.Presenter> {
        void tampilkanRespone(CategoryIndexResponse response1);

        void onImgResponse(ArrayList<Post> list);

        void tampilkanError(int code, String msg);
    }

    interface Presenter extends BasePresenter {
        void loadData(int id);

        void setImgUrl(int id);

        void getPosts(int id, String page, String count);
    }

}
