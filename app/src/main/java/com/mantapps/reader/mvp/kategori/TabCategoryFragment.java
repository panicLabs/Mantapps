package com.mantapps.reader.mvp.kategori;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;

import com.mantapps.reader.R;
import com.mantapps.reader.base.BaseFragment;
import com.mantapps.reader.util.Config;

import butterknife.BindView;

/**
 * @author paniclabs.
 * @created on 6/18/17.
 * @email panic.inc.dev@gmail.com
 * @projectName Mantapps
 */
public class TabCategoryFragment extends BaseFragment {
    @BindView(R.id.tabslayout)
    TabLayout mTablayout;
    @BindView(R.id.main_vp)ViewPager mViewPager;

    String[] titles = {
            "Brand",
            "News",
            "Tech"
    };

    private Fragment[] fragments;


    @Override
    protected void initView(View view, Bundle savedInstanceState) {
        initFragments();
        initTabVP();
    }

    @Override
    protected int getLayoutId() {
        return R.layout.tab_category;
    }

    @Override
    protected void setPresenter() {

    }

    private void initTabVP() {
        mViewPager.setOffscreenPageLimit(3);
        mViewPager.setAdapter(new FragmentPagerAdapter(getChildFragmentManager()) {
            @Override
            public Fragment getItem(int position) {
                return fragments[position];
            }

            @Override
            public int getCount() {
                return fragments.length;
            }

            @Override
            public CharSequence getPageTitle(int position) {return titles[position];
            }
        });
        mTablayout.setupWithViewPager(mViewPager);
    }

    private void initFragments() {
        fragments = new Fragment[titles.length];
        for (int i = 0; i < titles.length; i++) {
            CategoryDetailFragment contentFragment = new CategoryDetailFragment();
            fragments[i] = contentFragment;
            Bundle bundle = new Bundle();
            int id = 0;
            switch (titles[i]){
                case "Brand":
                    id = Config.BRAND_ID;
                    break;
                case "News":
                    id = Config.NEWS_ID;
                    break;
                case "Tech":
                    id = Config.TECH_ID;
                    break;
            }
            bundle.putInt("by", id);
            fragments[i].setArguments(bundle);
        }
    }
}
