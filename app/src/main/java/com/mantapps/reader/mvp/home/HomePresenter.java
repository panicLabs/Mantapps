//package com.mantapps.reader.mvp.home;
//
//
//import android.widget.Toast;
//
//import com.elvishew.xlog.XLog;
//import com.mantapps.reader.api.MantappsApi;
//import com.mantapps.reader.app.AppController;
//import com.mantapps.reader.net.HttpEx;
//import com.mantapps.reader.util.Config;
//import com.mantapps.reader.util.JSONParser;
//import com.mantapps.reader.util.RestUtil;
//
//import org.json.JSONException;
//import org.json.JSONObject;
//
//import java.io.IOException;
//
//import okhttp3.ResponseBody;
//import retrofit2.Call;
//import retrofit2.Callback;
//import retrofit2.Response;
//
///**
//* Created by MVPHelper on 2017/01/26
//*/
//
//public class HomePresenter implements HomeContract.Presenter {
//
//	private final HomeContract.View mHomeView;
//
//	public HomePresenter(HomeContract.View homeView) {
//        this.mHomeView = homeView;
//
//    }
//	@Override
//	public void start() {
//        String url = Config.POSTS_URL + "?json=get_posts&page=" + 0;
//        mHomeView.showLoading();
//        RestUtil.getInstance().create(MantappsApi.class).getRecentPost(url).enqueue(new Callback<ResponseBody>() {
//            @Override
//            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
//                mHomeView.hideLoading();
//                if(response.isSuccessful()){
//                    JSONObject jsonObject = null;
//                    try {
//                        jsonObject = new JSONObject(response.body().string());
//                        XLog.w(jsonObject.toString());
//                        mHomeView.tampilkanData(JSONParser.parsePosts(jsonObject));
//                    } catch (JSONException | IOException e) {
//                        e.printStackTrace();
//                        mHomeView.onErrorView();
//                    }
//                }else {
//                    mHomeView.onErrorView();
//                }
//            }
//
//            @Override
//            public void onFailure(Call<ResponseBody> call, Throwable t) {
//                mHomeView.hideLoading();
//                if (t instanceof HttpEx) {
//                    switch (((HttpEx) t).response().code()) {
//                        case 404:
//                            mHomeView.onErrorView();
//                            Toast.makeText(AppController.getInstance(), "Tidak ada jaringan", Toast.LENGTH_SHORT).show();
//                            //mTextView.setText("Thingy not found");
//                            break;
//                        case 500:
//                            mHomeView.onErrorView();
//                            Toast.makeText(AppController.getInstance(), "Server sedang bermasalah", Toast.LENGTH_SHORT).show();
//                            break;
//                    }
//                } else {
//                    mHomeView.onErrorView();
//                    Toast.makeText(AppController.getInstance(), "Tidak ada jaringan", Toast.LENGTH_SHORT).show();
//                }
//            }
//        });
//	}
//
//    @Override
//	public void loadMore(int page) {
//        String url = Config.POSTS_URL + "?json=get_posts&page=" + page;
//		mHomeView.showDialog();
//        RestUtil.getInstance().create(MantappsApi.class).getRecentPost(url).enqueue(new Callback<ResponseBody>() {
//            @Override
//            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
//                mHomeView.hideDialog();
//                if(response.isSuccessful()){
//                    JSONObject jsonObject = null;
//					try {
//						jsonObject = new JSONObject(response.body().string());
//						XLog.w(jsonObject.toString());
//						mHomeView.onLoadMoreSukses(JSONParser.parsePosts(jsonObject));
//					} catch (JSONException | IOException e) {
//						e.printStackTrace();
//						mHomeView.onErrorView();
//					}
//                }else {
//                    mHomeView.onErrorView();
//                }
//            }
//
//            @Override
//            public void onFailure(Call<ResponseBody> call, Throwable t) {
//                mHomeView.hideDialog();
//                if (t instanceof HttpEx) {
//                    switch (((HttpEx) t).response().code()) {
//                        case 404:
//                            mHomeView.onErrorView();
//                            Toast.makeText(AppController.getInstance(), "Tidak ada jaringan", Toast.LENGTH_SHORT).show();
//                            //mTextView.setText("Thingy not found");
//                            break;
//                        case 500:
//                            mHomeView.onErrorView();
//                            Toast.makeText(AppController.getInstance(), "Server sedang bermasalah", Toast.LENGTH_SHORT).show();
//                            break;
//                    }
//                } else {
//                    mHomeView.onErrorView();
//                    Toast.makeText(AppController.getInstance(), "Tidak ada jaringan", Toast.LENGTH_SHORT).show();
//                }
//            }
//        });
//	}
//}