package com.mantapps.reader.mvp.kategori;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.TextView;

import com.mantapps.reader.R;
import com.mantapps.reader.base.BaseFragment;
import com.mantapps.reader.util.Config;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * @author ali@pergikuliner
 * @created 6/13/17.
 * @project Mantapps.
 */

public class KategoriFragment extends BaseFragment {

    @BindView(R.id.tv1) TextView txt1;
    @BindView(R.id.tv2) TextView txt2;
    @BindView(R.id.tv3) TextView txt3;

    private String[] stringId = {"Brand","News","Tech"};

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected void initView(View view, Bundle savedInstanceState) {

    }

    @Override
    protected int getLayoutId() {
        return R.layout.kategori_frag;
    }

    @Override
    protected void setPresenter() {

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @OnClick(R.id.tv1)
    void onRowOne(){
        CategoryDetailFragment cdf = new CategoryDetailFragment();
        Bundle args = new Bundle();
        args.putInt("by", Config.BRAND_ID);
        cdf.setArguments(args);

        replaceFragment(R.id.frag_container,cdf);
    }

    @OnClick(R.id.tv2)
    void onRowTwo(){
        CategoryDetailFragment cdf = new CategoryDetailFragment();
        Bundle args = new Bundle();
        args.putInt("by", Config.NEWS_ID);
        cdf.setArguments(args);

        replaceFragment(R.id.frag_container,cdf);
    }

    @OnClick(R.id.tv3)
    void onRowThree(){
        CategoryDetailFragment cdf = new CategoryDetailFragment();
        Bundle args = new Bundle();
        args.putInt("by", Config.TECH_ID);
        cdf.setArguments(args);

        replaceFragment(R.id.frag_container,cdf);
    }
}
