package com.mantapps.reader.mvp.newhome;

import android.util.Log;

import com.elvishew.xlog.XLog;
import com.mantapps.reader.model.Post;
import com.mantapps.reader.util.Config;
import com.mantapps.reader.util.JSONParser;
import com.mantapps.reader.util.RestUtil;
import com.paniclabs.libs.datastorage.DS;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;
import okhttp3.ResponseBody;
import retrofit2.Response;

/**
 * @author paniclabs.
 * @created on 1/28/17.
 * @email panic.inc.dev@gmail.com
 * @projectName mantapps_mvp
 * @packageName com.mantapps.reader.mvp.home.
 * @className ${CLASS}.
 */
public class NewHomePresenter implements NewHomeContract.Presenter {

    private final CompositeDisposable compositeDisposable;
    private NewHomeContract.View mHomeView;

    public NewHomePresenter(NewHomeContract.View homeView) {
        mHomeView = homeView;
        compositeDisposable = new CompositeDisposable();
    }
    @Override
    public void start() {
            loadData();
    }

    @Override
    public void loadData() {
        List<Post> postList = new ArrayList<>();
        mHomeView.showLoading();
        if(DS.grab("HOMELIST")!= null){
            postList = DS.grab("HOMELIST");
            mHomeView.hideLoading();
            mHomeView.tampilkanData(postList);
            compositeDisposable.add(Observable.just("test")
                    .delay(7,TimeUnit.SECONDS)
                    // Run on a background thread
                    .subscribeOn(Schedulers.io())
                    // Be notified on the main thread
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(aLong -> {
                        XLog.w("ISREFESIING NEW DATA");
                        mHomeView.isRefreshing(false);
                        loadNewData();
                    }));
        }else {
            loadDatas();
        }

    }

    private void loadDatas() {
        String url = Config.POSTS_URL + "?json=get_posts&page=" + 0;
        Disposable disp = RestUtil.withRx().getRecentPosts(url)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .map(response -> {
                    List<Post> list = new ArrayList<>();
                    if (response.isSuccessful()) {
                        JSONObject jsonObject = null;
                        try {
                            jsonObject = new JSONObject(response.body().string());
                            DS.entry("HOMELIST", JSONParser.parsePosts(jsonObject));
                            list = JSONParser.parsePosts(jsonObject);
                        } catch (JSONException | IOException e) {
                            e.printStackTrace();
                        }
                    }
                    return list;
                })
                .subscribe(posts -> {
                    mHomeView.hideLoading();
                    XLog.e("tampilkanData done ");
                    mHomeView.isRefreshing(false);
                    mHomeView.tampilkanData(posts);
                }, throwable -> {
                    mHomeView.hideLoading();
                    mHomeView.isRefreshing(false);
                    XLog.e("Error", throwable);
                    mHomeView.onErrorView();
                });

        compositeDisposable.add(disp);
    }


    @Override
    public void loadNewData() {
        String url = Config.POSTS_URL + "?json=get_posts&page=" + 0;
        Disposable disp = RestUtil.withRx().getRecentPosts(url)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .map(response -> {
                    List<Post> list = new ArrayList<>();
                    if (response.isSuccessful()) {
                        JSONObject jsonObject = null;
                        try {
                            jsonObject = new JSONObject(response.body().string());
                            DS.entry("HOMELIST", JSONParser.parsePosts(jsonObject));
                            list = JSONParser.parsePosts(jsonObject);
                        } catch (JSONException | IOException e) {
                            e.printStackTrace();
                        }
                    }
                    return list;
                })
                .subscribe(posts -> {
                    XLog.e("tampilkanData done ");
                    mHomeView.isRefreshing(false);
                    mHomeView.tampilkanData(posts);
                }, throwable -> {
                    mHomeView.isRefreshing(false);
                    XLog.e("Error", throwable);
                    mHomeView.onErrorView();
                });

        compositeDisposable.add(disp);
    }

    @Override
    public void loadMore(int page) {
        String url = Config.POSTS_URL + "?json=get_posts&page=" + page;
        Log.w("Urlapi" ,url);
        mHomeView.showDialog();

        Disposable disp = RestUtil.withRx().getRecentPosts(url)
                // Run on a background thread
                .subscribeOn(Schedulers.io())
                // Be notified on the main thread
                .observeOn(AndroidSchedulers.mainThread())
                .map((Function<Response<ResponseBody>, List<Post>>) response -> {
                    List<Post> listp = new ArrayList<>();
                    if (response.isSuccessful()) {

                        JSONObject jsonObject = null;
                        try {
                            jsonObject = new JSONObject(response.body().string());
                            DS.entry("HOMELIST", JSONParser.parsePosts(jsonObject));
                            listp =  JSONParser.parsePosts(jsonObject);
                        } catch (JSONException | IOException e) {
                            e.printStackTrace();
                        }
                    }
                    return listp;
                })
                .subscribe(posts -> {
                    mHomeView.hideDialog();
                    mHomeView.onLoadMoreSukses(posts);
                }, throwable -> {
                    mHomeView.hideDialog();
                    XLog.e("Error", throwable);
                    mHomeView.onErrorView();
                });

        compositeDisposable.add(disp);

    }

    @Override
    public void stop(){
        compositeDisposable.clear();
        if(mHomeView !=null) mHomeView = null;
    }

}
