package com.mantapps.reader.mvp.category.model;

/**
 * @author paniclabs.
 * @created on 1/24/17.
 * @email panic.inc.dev@gmail.com
 * @projectName mantapps_mvp
 * @packageName com.mantapps.reader.api.model.
 * @className ${CLASS}.
 */
public class ZipCategoryRespone {

    public CategoryIndexResponse response1,response2,response3;

    public ZipCategoryRespone(CategoryIndexResponse cir1,CategoryIndexResponse cir2,CategoryIndexResponse cir3) {
        this.response1 = cir1;
        this.response2 = cir2;
        this.response3 = cir3;
    }
}
