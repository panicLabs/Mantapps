package com.mantapps.reader.mvp.kategori;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.mantapps.reader.R;
import com.mantapps.reader.app.CategoryListActivity;
import com.mantapps.reader.model.Post;
import com.mantapps.reader.mvp.category.model.CategoriesItem;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * @author ali@pergikuliner
 * @created 6/13/17.
 * @project Mantapps.
 */

public class KategoriAdapter extends RecyclerView.Adapter<KategoriAdapter.ViewHolder> {

    private final Context ctx;
    private ArrayList<CategoriesItem> mList;
    OnLoadImageListener listener;

    void setImageList(ArrayList<Post> posts, int pos, ViewHolder holder) {
        String imgs = posts.get(0).getMediumImageUrl();
        Glide.with(ctx).load(imgs).asBitmap()
                .diskCacheStrategy( DiskCacheStrategy.RESULT )
                .into(holder.imgCategory);

    }

    interface OnLoadImageListener{
        void getImage(int id, int pos, ViewHolder holder);
    }

    KategoriAdapter(Context activity, OnLoadImageListener mListener) {
        ctx = activity;
        listener = mListener;
        mList = new ArrayList<>();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_row, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        CategoriesItem items = mList.get(position);

        if(listener !=null){
            listener.getImage(items.getId(),position,holder);
        }

        holder.textCategory.setText(items.getTitle());

        holder.imgCategory.setOnClickListener(view -> {
            Intent intent = new Intent(ctx, CategoryListActivity.class);
            Bundle args = new Bundle();
            args.putSerializable(CategoryListActivity.CATEGORY, items);
            intent.putExtras(args);
            ctx.startActivity(intent);

        });
    }

    @Override
    public int getItemCount() {
        return mList == null ? 0 : mList.size();
    }

    public void addAll(List<CategoriesItem> categories) {
        mList.clear();
        mList.addAll(categories);
        notifyDataSetChanged();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        @BindView(R.id.id_category_text) TextView textCategory;
        @BindView(R.id.id_category_image) ImageView imgCategory;

        public ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this,view);
        }
    }
}
