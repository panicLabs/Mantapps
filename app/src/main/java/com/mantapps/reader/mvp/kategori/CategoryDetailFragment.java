package com.mantapps.reader.mvp.kategori;

import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;

import com.elvishew.xlog.XLog;
import com.mantapps.reader.R;
import com.mantapps.reader.api.MantappsApi;
import com.mantapps.reader.base.BaseFragment;
import com.mantapps.reader.model.Post;
import com.mantapps.reader.mvp.category.model.CategoriesItem;
import com.mantapps.reader.mvp.category.model.CategoryIndexResponse;
import com.mantapps.reader.util.JSONParser;
import com.mantapps.reader.util.RestUtil;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import butterknife.BindView;
import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.ObservableTransformer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

/**
 * @author ali@pergikuliner
 * @created 6/13/17.
 * @project Mantapps.
 */

public class CategoryDetailFragment extends BaseFragment implements KategoriAdapter.OnLoadImageListener {

    private static final String SAVED_LAYOUT_MANAGER = "";
    @BindView(R.id.recycler_view)RecyclerView recyclerView;
    @BindView(R.id.progress_bar) ProgressBar progressBar;


    private CompositeDisposable mCompositeDisposable;
    private KategoriAdapter mAdapter;
    private Parcelable layoutManagerSavedState;

    @Override
    protected void initView(View view, Bundle savedInstanceState) {
        mCompositeDisposable = new CompositeDisposable();
        int value = getArguments().getInt("by");
        setuprecyclerView();

        loadJSON(value);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        Bundle bundle = new Bundle();
        bundle.putParcelable(SAVED_LAYOUT_MANAGER, recyclerView.getLayoutManager().onSaveInstanceState());
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onViewStateRestored(@Nullable Bundle state) {
        if (state instanceof Bundle) {
            layoutManagerSavedState = ((Bundle) state).getParcelable(SAVED_LAYOUT_MANAGER);
        }
        super.onViewStateRestored(state);
    }

    private void loadJSON(int value) {
        MantappsApi requestInterface = RestUtil.withRx();
        List<CategoriesItem> categoriesItems = new ArrayList<>();
//
//        Disposable api = requestInterface.getCategoryInd(value)
//                .flatMap(new Function<CategoryIndexResponse, ObservableSource<CategoriesItem>>() {
//                    @Override
//                    public ObservableSource<CategoriesItem> apply(@NonNull CategoryIndexResponse response) throws Exception {
//                        categoriesItems.addAll(response.getCategories());
//                        return Observable.fromIterable(response.getCategories());
//                    }
//                })
//                .flatMap(new Function<CategoriesItem, ObservableSource<?>>() {
//                    @Override
//                    public ObservableSource<?> apply(@NonNull CategoriesItem o) throws Exception {
//                        return getCategoryPost(o.getId());
//                    }
//                })
//                .flatMap(new Function<String, ObservableSource<List<CategoriesItem>>>() {
//                    @Override
//                    public ObservableSource<List<CategoriesItem>> apply(@NonNull String s) throws Exception {
//                        return getListss(categoriesItems,s);
//                    }
//                })
//                .observeOn(AndroidSchedulers.mainThread())
//                .subscribeOn(Schedulers.io())
//                .subscribe(new Consumer<List<CategoriesItem>>() {
//                    @Override
//                    public void accept(@NonNull List<CategoriesItem> categoriesItems) throws Exception {
//
//                    }
//                });

//        mCompositeDisposable.add(api);

    }

    private Observable<List<CategoriesItem>> getListss(List<CategoriesItem> list,String string){
        return Observable.fromCallable(() -> {
            CategoriesItem it = new CategoriesItem();
            for (int i = 0; i < list.size(); i++) {

                it.setImage(string);
                list.add(it);
            }

            return list;
        });
    }

    private Observable<String> getCategoryPost(int id){
        return RestUtil.withRx().getKategoriPost(id,"1","1")
                .map(response -> {
                    List<Post> postList = new ArrayList<Post>();
                    if (response.isSuccessful()) {
                        try {
                            JSONObject jsonObject = new JSONObject(response.body().string());
                            postList.addAll(JSONParser.parsePosts(jsonObject));
                        } catch (JSONException | IOException e) {
                            e.printStackTrace();
                        }
                    }
                    return postList;
                })
                .compose(new ObservableTransformer<List<Post>, String>() {
                    @Override
                    public ObservableSource<String> apply(@NonNull Observable<List<Post>> upstream) {
                        return upstream.map(posts -> posts.get(0).getMediumImageUrl());
                    }
                });
    }


    private Observable<String> getImg(List<Post> list){
        return Observable.just(list.get(0).getMediumImageUrl());
    }

    private void setuprecyclerView() {
        recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(getActivity(),2);
        recyclerView.setLayoutManager(layoutManager);
        mAdapter = new KategoriAdapter(getActivity(),this);
        recyclerView.setAdapter(mAdapter);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.category_rvlist;
    }

    @Override
    protected void setPresenter() {

    }

    private void handleError(Throwable throwable) {
        progressBar.setVisibility(View.GONE);
        XLog.e(throwable.getLocalizedMessage());
    }

    private void handleResponse(CategoryIndexResponse response) {
        progressBar.setVisibility(View.GONE);
        mAdapter.addAll(response.getCategories());
        restoreLayoutManagerPosition();
    }

    private void restoreLayoutManagerPosition() {
        if (layoutManagerSavedState != null) {
            recyclerView.getLayoutManager().onRestoreInstanceState(layoutManagerSavedState);
        }
    }

    // Simple representation of getting your ids.
    // Replace the content of this method with yours
    private static Observable<List<Integer>> getIds(int id) {
        return Observable.just(Collections.singletonList(id));
    }

//    // Replace the content of this method with yours
//    private static Observable<Item> getItemObservable(Integer id) {
//        Item item = new Item();
//        item.id = id;
//        return Observable.just(item);
//    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mCompositeDisposable.clear();
    }

    @Override
    public void getImage(int id, int pos, KategoriAdapter.ViewHolder holder) {
        Disposable apiImage = RestUtil.withRx().getKategoriPost(id, "1", "1")
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(response -> {
                    if (response.isSuccessful()) {
                        try {
                            JSONObject jsonObject = new JSONObject(response.body().string());
                            mAdapter.setImageList(JSONParser.parsePosts(jsonObject), pos,holder);
                        } catch (JSONException | IOException e) {
                            e.printStackTrace();
                        }
                    }
                }, this::handleError);

        mCompositeDisposable.add(apiImage);
    }
}
