package com.mantapps.reader.mvp.newhome;

import com.mantapps.reader.base.BasePresenter;
import com.mantapps.reader.base.BaseView;
import com.mantapps.reader.model.Post;

import java.util.List;

/**
 * @author paniclabs.
 * @created on 1/28/17.
 * @email panic.inc.dev@gmail.com
 * @projectName mantapps_mvp
 * @packageName com.mantapps.reader.mvp.home.
 * @className ${CLASS}.
 */
public interface NewHomeContract {
    interface View extends BaseView<Presenter> {
        void tampilkanData(List<Post> posts);

        void onErrorView();

        void hideLoading();
        void showLoading();

        void showDialog();

        void hideDialog();

        void onLoadMoreSukses(List<Post> posts);

        void onNoConnection(String s);

        void isNull();

        void isRefreshing(boolean isRefresfh);
    }

    interface Presenter extends BasePresenter {
        void loadData();
        void loadNewData();
        void loadMore(int page);
        void stop();
    }
}
