package com.mantapps.reader.mvp.newhome;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import com.elvishew.xlog.XLog;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.remoteconfig.FirebaseRemoteConfig;
import com.mantapps.reader.R;
import com.mantapps.reader.base.BaseFragment;
import com.mantapps.reader.model.Post;
import com.mantapps.reader.util.Utils;
import com.mantapps.reader.view.LoadingView;
import com.mtburn.android.sdk.AppDavis;
import com.mtburn.android.sdk.instream.ADVSInstreamAdPlacer;
import com.mtburn.android.sdk.instream.ADVSInstreamAdPlacerListener;
import com.mtburn.android.sdk.model.ADVSInstreamInfoModel;
import com.paniclabs.libs.datastorage.SharedPrefManager;
import com.paniclabs.libs.recyclerflipview.FlipLayoutManager;
import com.paniclabs.libs.recyclerflipview.FlipRefreshListener;
import com.paniclabs.libs.recyclerflipview.MySnap;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;

/**
 * @author paniclabs.
 * @created on 1/28/17.
 * @email panic.inc.dev@gmail.com
 * @projectName mantapps_mvp
 * @packageName com.mantapps.reader.mvp.home.
 * @className ${CLASS}.
 */
public class NewHomeFragment extends BaseFragment implements NewHomeContract.View, FlipRefreshListener.Listener, ADVSInstreamAdPlacerListener {

    private static final String TAG = "HomeFragment";

    NewHomeContract.Presenter presenter;

    @BindView(R.id.colayout)CoordinatorLayout coordinatorLayout;
    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;
    @BindView(R.id.loadingView)
    LoadingView loadingView;
    @BindView(R.id.logo)
    ImageView mLogo;
    @Nullable @BindView(R.id.swipe_refresh_layout)SwipeRefreshLayout swipeRefreshLayout;

    private NewHomeAdapter mAdapter;
    private FlipRefreshListener mFlipListener;
    private boolean mIsLoading;
    private boolean mHasMore = true;
    private int mPage = 1;
    private List<Object> Newslist = new ArrayList<>();
    private ADVSInstreamAdPlacer adPlacer;
    private ProgressDialog pd;
    private OnHomeSelectedListener mListener;
//    private AdView fbAdView;

    public interface OnHomeSelectedListener {
        void onFlipSelected(ImageView id_photo, HashMap<String, String> map);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (NewHomeFragment.OnHomeSelectedListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    protected void initView(View view, Bundle savedInstanceState) {
        setupAdsSDK();

//        setupFbAds();
        pd = new ProgressDialog(getActivity());
        pd.setMessage("Loading...");
        pd.setCancelable(false);

        FlipLayoutManager layoutManager = new FlipLayoutManager(getContext());
        layoutManager.setAutoMeasureEnabled(false);
        layoutManager.setOnErrorListener(getActivity(), this::isNull);

        if (mAdapter == null) {
            mAdapter = new NewHomeAdapter<>(getContext(), Newslist, adPlacer, mListener, this);
        }

        recyclerView.setItemAnimator(null);
        recyclerView.setLayoutManager(layoutManager);
        MySnap snap = new MySnap();
        snap.attachToRecyclerView(recyclerView);

        recyclerView.setAdapter(mAdapter); // setting an AdmobExpressRecyclerAdapterWrapper to a RecyclerView

        if (mFlipListener == null) {
            mFlipListener = new FlipRefreshListener(this);
        }
        recyclerView.addOnScrollListener(mFlipListener);
        if (mAdapter.getDataCount() == 0) {
            if (Newslist == null || Newslist.size() <= 0){
                isNull();
            }
        } else {
            mFlipListener.onScrolled(recyclerView, 0, 0);
        }

    }

//    private void setupFbAds() {
//        // Instantiate an AdView view
//        fbAdView = new com.facebook.ads.AdView(getActivity(), "180164295839771_249770228879177", AdSize.BANNER_HEIGHT_50);
//        fbAdView.setAdListener(new com.facebook.ads.AdListener() {
//            @Override
//            public void onError(Ad ad, AdError adError) {
////                showToast("Banner: onError : " + adError.getErrorMessage());
//            }
//
//            @Override
//            public void onAdLoaded(Ad ad) {
//                lbanner.removeAllViews();
//                lbanner.addView(fbAdView);
////                showToast("Banner: onAdLoaded");
//            }
//
//            @Override
//            public void onAdClicked(Ad ad) {
////                showToast("Banner: onAdClicked");
//            }
//
//            @Override
//            public void onLoggingImpression(Ad ad) {
////                showToast("Banner: onLoggingImpression");
//            }
//        });
//        // Request an ad
//        fbAdView.loadAd();
//    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        presenter = new NewHomePresenter(this);
        presenter.start();


    }

    private void setupAdsSDK() {
        AppDavis.init(getActivity().getApplicationContext(), getString(R.string.media_id));
        adPlacer = AppDavis.createInstreamAdPlacer(getContext().getApplicationContext(), getString(R.string.spot_id));
        adPlacer.setAdListener(this);

    }

    @Override
    protected int getLayoutId() {
        return R.layout.new_home_fragment;
    }

    @Override
    protected void setPresenter() {
        presenter = new NewHomePresenter(this);
    }

    @Override
    public void tampilkanData(List<Post> posts) {
        swipeRefreshLayout.setRefreshing(false);
        recyclerView.setVisibility(View.VISIBLE);
        mLogo.setVisibility(View.GONE);
        Newslist.clear();
        Newslist.addAll(posts);
        mAdapter.notifyDataSetChanged();
        adPlacer.loadAd();

        XLog.w(TAG + Newslist.size());
    }

    @Override
    public void onErrorView() {
        swipeRefreshLayout.setRefreshing(false);
    }

    @Override
    public void hideLoading() {
        loadingView.setLoading(false);
    }

    @Override
    public void showLoading() {
        loadingView.setLoading(true);

    }

    @Override
    public void showDialog() {
        if(pd !=null && !pd.isShowing()) pd.show();
    }

    @Override
    public void hideDialog() {
        if(pd !=null && pd.isShowing()) pd.dismiss();
    }

    @Override
    public void onLoadMoreSukses(List<Post> posts) {
        Newslist.addAll(posts);
        mAdapter.notifyDataSetChanged();
    }

    @Override
    public void onNoConnection(String errorMsg) {
        swipeRefreshLayout.setRefreshing(false);
        Snackbar.make(coordinatorLayout,errorMsg,Snackbar.LENGTH_SHORT).show();
    }

    @Override
    public void isNull() {
        mLogo.setVisibility(View.VISIBLE);
        recyclerView.setVisibility(View.GONE);
    }

    @Override
    public void isRefreshing(boolean isRefresfh) {
        assert swipeRefreshLayout != null;
        swipeRefreshLayout.setRefreshing(isRefresfh);
    }

    @Override
    public void onRefresh() {
        assert swipeRefreshLayout != null;
        swipeRefreshLayout.setOnRefreshListener(() -> {
            if(Utils.isOnline(getActivity())){
                swipeRefreshLayout.setRefreshing(true);
                presenter.loadNewData();
            }else {
                swipeRefreshLayout.setRefreshing(false);
                onNoConnection("tidak ada koneksi internet");
            }

        });

    }

    @Override
    public void onDrag(float percent, boolean shouldRefresh) {

    }

    @Override
    public void onLoadMore() {
        ++mPage;
        if (mHasMore) {
            loadMore();
        }
    }

    private void loadMore() {
        if(Utils.isOnline(getActivity())){
            presenter.loadMore(mPage);
        }else {
            Snackbar.make(coordinatorLayout,getString(R.string.no_internet),Snackbar.LENGTH_SHORT).show();
        }

    }


    @Override
    public void onAdsLoaded(List<? extends ADVSInstreamInfoModel> items) {
        Log.d("", "onAdsLoaded: ");
        mAdapter.insertAds(items);
    }

    @Override
    public void onAdMainImageLoaded(String s) {

    }

    @Override
    public void onAdIconImageLoaded(String s) {

    }

    @Override
    public void onAdsLoadedFail(String s) {

    }

    @Override
    public void onAdImageLoadedFail(String s, String s1) {

    }

    @Override
    public void onAdClicked(String s) {

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        presenter.stop();
    }

    @Override
    public void onResume() {
        super.onResume();
        long cacheExpiration = 0; // 1 hour in seconds.
        if (FirebaseRemoteConfig.getInstance().getInfo().getConfigSettings().isDeveloperModeEnabled()) {
            cacheExpiration = 0;
        }
        FirebaseRemoteConfig.getInstance().fetch(cacheExpiration).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (task.isSuccessful()) {
                    FirebaseRemoteConfig.getInstance().activateFetched();
                    boolean experiment1_variant = FirebaseRemoteConfig.getInstance().getBoolean("useAds");
                    XLog.i("REMOTE : " + experiment1_variant);
                    SharedPrefManager.getInstance().putValue("useAds",experiment1_variant);
                }
            }
        });
    }
}
