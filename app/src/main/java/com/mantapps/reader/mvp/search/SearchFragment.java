//package com.mantapps.reader.mvp.search;
//
//import android.content.Intent;
//import android.os.Bundle;
//import android.view.View;
//import android.widget.EditText;
//
//import com.mantapps.reader.R;
//import com.mantapps.reader.base.BaseFragment;
//
//import butterknife.BindView;
//
///**
//* Created by MVPHelper on 2017/01/26
//*/
//
//public class SearchFragment extends BaseFragment {
//
//    @BindView(R.id.id_search_click)
//    EditText mSearchView;
//
//    @Override
//    protected void initView(View view, Bundle savedInstanceState) {
//        mSearchView.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                mSearchView.setVisibility(View.INVISIBLE);
//
//                startActivity(new Intent(getActivity(), SearchResultActivity.class));
//
//            }
//        });
//    }
//
//    @Override
//    public void onResume() {
//        super.onResume();
//        mSearchView.setVisibility(View.VISIBLE);
//        mSearchView.requestFocus();
//    }
//
//    @Override
//    public void onHiddenChanged(boolean hidden) {
//        super.onHiddenChanged(hidden);
//        if(!hidden){
//            mSearchView.setVisibility(View.VISIBLE);
//            mSearchView.requestFocus();
//        }
//    }
//
//    @Override
//    protected int getLayoutId() {
//        return R.layout.search_fragment;
//    }
//
//    @Override
//    protected void setPresenter() {
//
//    }
//}