package com.mantapps.reader.mvp.category.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class CategoryIndexResponse{

	@SerializedName("count")
	@Expose
	private int count;

	@SerializedName("categories")
	@Expose
	private List<CategoriesItem> categories;

	@SerializedName("status")
	@Expose
	private String status;

	public void setCount(int count){
		this.count = count;
	}

	public int getCount(){
		return count;
	}

	public void setCategories(List<CategoriesItem> categories){
		this.categories = categories;
	}

	public List<CategoriesItem> getCategories(){
		return categories;
	}

	public void setStatus(String status){
		this.status = status;
	}

	public String getStatus(){
		return status;
	}

	@Override
	public String toString() {
		return "CategoryIndexResponse{" +
				"count=" + count +
				", categories=" + categories +
				", status='" + status + '\'' +
				'}';
	}
}