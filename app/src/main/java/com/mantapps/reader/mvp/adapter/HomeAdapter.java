//package com.mantapps.reader.mvp.adapter;
//
//import android.content.Intent;
//import android.graphics.Color;
//import android.os.Bundle;
//import android.support.v4.app.FragmentActivity;
//import android.text.Html;
//import android.util.Log;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//import android.view.animation.AlphaAnimation;
//import android.view.animation.DecelerateInterpolator;
//import android.widget.BaseAdapter;
//import android.widget.ImageView;
//import android.widget.LinearLayout;
//import android.widget.TextView;
//
//import com.bumptech.glide.Glide;
//import com.elvishew.xlog.XLog;
//import com.mantapps.reader.R;
//import com.mantapps.reader.app.ArticleListActivity;
//import com.mantapps.reader.app.CategoryListActivity;
//import com.mantapps.reader.model.Author;
//import com.mantapps.reader.model.Category;
//import com.mantapps.reader.model.Post;
//import com.mtburn.android.sdk.instream.ADVSInstreamAdPlacer;
//import com.mtburn.android.sdk.model.ADVSInstreamInfoModel;
//
//import java.util.HashMap;
//import java.util.List;
//
///**
// * @author paniclabs.
// * @created on 1/26/17.
// * @email panic.inc.dev@gmail.com
// * @projectName mantapps_mvp
// * @packageName com.mantapps.reader.adapter.
// * @className ${CLASS}.
// */
//public class HomeAdapter extends BaseAdapter {
//    private static final String TAG_DEFAULT = "TAG_DEFAULT";
//    private final FragmentActivity context;
//    private final HomeFragment.OnHomeSelectedListener homeListener;
//    //private final List<Post> mList;
//    private HashMap<Integer, Integer> colors = new HashMap<>();
//    private LayoutInflater inflater;
//    public List<Object> dataSourceList;
//    private ADVSInstreamAdPlacer adPlacer;
//
//    public HomeAdapter(FragmentActivity activity, List<Object> news, ADVSInstreamAdPlacer ad, HomeFragment.OnHomeSelectedListener mListener) {
//        inflater = LayoutInflater.from(activity.getApplicationContext());
//        context = activity;
//        adPlacer = ad;
//        dataSourceList = news;
//        homeListener = mListener;
//        //mList = posts;
//        colors.put(1,Color.parseColor("#FF1DD3FA"));
//        colors.put(2,Color.parseColor("#FF2288DD"));
//        colors.put(3,Color.parseColor("#FFA98D09"));
//        colors.put(4,Color.parseColor("#FF7F023D"));
//        colors.put(5,Color.parseColor("#FF01B634"));
//        colors.put(6,Color.parseColor("#FF10E8A4"));
//        colors.put(6857,Color.parseColor("#FF6A05DB"));
//        colors.put(6851, Color.parseColor("#FFE302E1"));
//    }
//
//
//
//    @Override
//    public int getCount() {
//        return dataSourceList.size();
//    }
//
//    @Override
//    public Object getItem(int position) {
//        return dataSourceList.get(position);
//    }
//
//    @Override
//    public long getItemId(int position) {
//        return position;
//    }
//
//    @Override
//    public int getViewTypeCount() {
//        return 2;
//    }
//
//    @Override
//    public int getItemViewType(int position) {
//        Log.w("Isad", "kah =" + isAd(position));
//        return isAd(position) ? 0 : 1;
//    }
//
//    private boolean isAd(int position) {
//        return (getItem(position) instanceof ADVSInstreamInfoModel);
//    }
//
//    @Override
//    public boolean hasStableIds() {
//        return true;
//    }
//
//    @Override
//    public View getView(final int position, View convertView, ViewGroup parent) {
//        Log.w("Isad", "=" + (getItem(position) instanceof ADVSInstreamInfoModel));
//        if (isAd(position)||getItem(position) instanceof ADVSInstreamInfoModel) {
//            Log.w("Isad", "getView: ");
//            try {
//                convertView = adPlacer.placeAd((ADVSInstreamInfoModel) getItem(position), convertView, parent);
//            }catch (Error e){
//                Log.w("Isad", "getView: "+e);
//            }
//        } else {
//            final ViewHolder holder;
//
//            if(convertView == null || !TAG_DEFAULT.equals(convertView.getTag())){
//                holder = new ViewHolder();
//                convertView = inflater.inflate(R.layout.home_item, parent, false);
//
//                holder.mRootView = (LinearLayout) convertView.findViewById(R.id.rootview);
//                holder.photo = (ImageView) convertView.findViewById(R.id.id_photo);
//                holder.id_author = (TextView) convertView.findViewById(R.id.id_author);
//                holder.id_post_title = (TextView) convertView.findViewById(R.id.id_post_title);
//                holder.c1 = (TextView) convertView.findViewById(R.id.c1);
//                holder.c2 = (TextView) convertView.findViewById(R.id.c2);
//                holder.c3 = (TextView) convertView.findViewById(R.id.c3);
//                holder.c4 = (TextView) convertView.findViewById(R.id.c4);
//                holder.c5 = (TextView) convertView.findViewById(R.id.c5);
//                holder.c6 = (TextView) convertView.findViewById(R.id.c6);
//                holder.c7 = (TextView) convertView.findViewById(R.id.c7);
//                holder.c8 = (TextView) convertView.findViewById(R.id.c8);
//                holder.txvDate = (TextView) convertView.findViewById(R.id.txvDate);
//                holder.id_post_description = (TextView) convertView.findViewById(R.id.id_post_description);
//
//                convertView.setTag(holder);
//            }else{
//                holder = (ViewHolder) convertView.getTag();
//            }
//
//            //TODO set a text with the id as well
//
//            try{
//                final Post post = ((Post) dataSourceList.get(position));
//                holder.mRootView.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View view) {
//                        HashMap<String, String> map = new HashMap<>();
//                        XLog.w("id = " + post.getId() +
//                                "\nposition = " + position +
//                                "\ntitle = " + post.getTitle() +
//                                "\nslug = " + post.getSlug() +
//                                "\ndate = " + post.getDate() +
//                                "\nauthor = " + post.getAuthor() +
//                                "\nurl = " + post.getUrl());
//
//                        map.put("id", String.valueOf(post.getId()));
//                        map.put("title", post.getTitle());
//                        map.put("slug", post.getSlug());
//                        map.put("date", post.getDate());
//                        map.put("author", post.getAuthor());
//                        map.put("content", post.getContent());
//                        map.put("url", post.getUrl());
//                        map.put("thumbnailURL", post.getMediumImageUrl());
//                        homeListener.onFlipSelected(holder.photo, map);
//                    }
//                });
//
//                String excerpt = post.getExcerpt();
//                int index = excerpt.indexOf("mantapps.co.id");
//                if (index > -1)
//                    excerpt = excerpt.substring(0, index);
//
//                holder.id_post_description.setText(Html.fromHtml(excerpt));
//                AlphaAnimation fadeImage = new AlphaAnimation(0, 1);
//                fadeImage.setDuration(3000);
//                fadeImage.setInterpolator(new DecelerateInterpolator());
//
//                Glide.with(context).load(post.getMediumImageUrl())
//                        .animate(fadeImage)
//                        .into(holder.photo);
//                // holder.photo.setImageUrl(post.getMediumImageUrl(), imageLoader);
//                holder.txvDate.setText(post.getDate());
//                holder.id_author.setText(post.getAuthor());
//                //id_categoriesDesc.setText("ssssss");
//                holder.id_post_title.setText(post.getTitle());
//
//                final Author a = new Author();
//                a.setFullName(post.getAuthor());
//                a.setAuthorId(post.getAuthorId());
//                holder.id_author.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        Intent intent = new Intent(context, ArticleListActivity.class);
//                        Bundle args = new Bundle();
//                        args.putSerializable(ArticleListActivity.AUTHOR, a);
//                        intent.putExtras(args);
//                        context.startActivity(intent);
//                    }
//                });
//
//                holder.c1.setVisibility(View.GONE);
//                holder.c2.setVisibility(View.GONE);
//                holder.c3.setVisibility(View.GONE);
//                holder.c4.setVisibility(View.GONE);
//                holder.c5.setVisibility(View.GONE);
//                holder.c6.setVisibility(View.GONE);
//                holder.c7.setVisibility(View.GONE);
//                holder.c8.setVisibility(View.GONE);
//                for (int i = 1; i <= post.getCategories().size(); i++) {
//                    TextView txv = null;
//                    switch (i) {
//                        case 1:
//                            txv = holder.c1;
//                            break;
//                        case 2:
//                            txv = holder.c2;
//                            break;
//                        case 3:
//                            txv = holder.c3;
//                            break;
//                        case 4:
//                            txv = holder.c4;
//                            break;
//                        case 5:
//                            txv = holder.c5;
//                            break;
//                        case 6:
//                            txv = holder.c6;
//                            break;
//                        case 7:
//                            txv = holder.c7;
//                            break;
//                        default:
//                            txv = holder.c8;
//                            break;
//                    }
//
//                    final Category c = post.getCategories().get(i - 1);
//                    txv.setText(c.getName());
//                    txv.setVisibility(View.VISIBLE);
//                    if (colors.containsKey(c.getId())) {
//                        txv.setBackgroundColor(colors.get(c.getId()));
//                    } else {
//                        txv.setBackgroundColor(Color.parseColor("#FF777777"));
//                    }
//
//                    txv.setOnClickListener(new View.OnClickListener() {
//                        @Override
//                        public void onClick(View v) {
//                            Category category = c;
//                            Intent intent = new Intent(context, CategoryListActivity.class);
//                            Bundle args = new Bundle();
//                            args.putSerializable(CategoryListActivity.CATEGORY, category);
//                            intent.putExtras(args);
//                            context.startActivity(intent);
//                        }
//                    });
//                }
//            }catch (Exception e){
//                e.printStackTrace();
//            }
//
//
//        }
//
//        return convertView;
//    }
//
//    public void addItems(List<Post> post) {
//        for(int i = 0 ; i<post.size(); i++){
//            dataSourceList.addAll(post);
//        }
//        notifyDataSetChanged();
//    }
//
//    public class ViewHolder{
//        LinearLayout mRootView;
//        TextView id_post_description;
//        TextView txvDate;
//        TextView id_author;
//        TextView id_post_title;
//        TextView c1;
//        TextView c2;
//        TextView c3;
//        TextView c4;
//        TextView c5;
//        TextView c6;
//        TextView c7;
//        TextView c8;
//        public ImageView photo;
//    }
//}
