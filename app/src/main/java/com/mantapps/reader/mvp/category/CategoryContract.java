package com.mantapps.reader.mvp.category;

import com.mantapps.reader.base.BasePresenter;
import com.mantapps.reader.base.BaseView;
import com.mantapps.reader.mvp.category.model.CategoryIndexResponse;

/**
 * @author paniclabs.
 * @created on 1/24/17.
 * @email panic.inc.dev@gmail.com
 * @projectName mantapps_mvp
 * @packageName com.mantapps.reader.mvp.category.
 * @className ${CLASS}.
 */
public class CategoryContract {
    
	interface View extends BaseView<Presenter> {
		void hideLoading();
		void showLoading();

		void tampilkanRespone(CategoryIndexResponse response1);

		void tampilkanError(int code, String msg);

		void noConnection();

        void saveToDB(Boolean aBoolean);


	}

	interface Presenter extends BasePresenter {
		void loadData(long id);
		void loadDataApi(int id, CategoryPresenter.OnCompletedListener listener);
		void setImgUrl(int id, CategoryPresenter.OnImageCategoryListener listener);
		void stop();
	}

}