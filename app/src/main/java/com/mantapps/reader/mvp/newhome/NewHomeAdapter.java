package com.mantapps.reader.mvp.newhome;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.DecelerateInterpolator;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.crashlytics.android.answers.Answers;
import com.crashlytics.android.answers.ContentViewEvent;
import com.elvishew.xlog.XLog;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;
import com.mantapps.reader.R;
import com.mantapps.reader.app.ArticleListActivity;
import com.mantapps.reader.app.CategoryListActivity;
import com.mantapps.reader.model.Author;
import com.mantapps.reader.model.Category;
import com.mantapps.reader.model.Post;
import com.mantapps.reader.util.Utils;
import com.mtburn.android.sdk.instream.ADVSInstreamAdPlacer;
import com.mtburn.android.sdk.model.ADVSInstreamInfoModel;
import com.paniclabs.libs.datastorage.SharedPrefManager;
import com.paniclabs.libs.recyclerflipview.FlipRefreshListener;
import com.paniclabs.libs.utils.LetterImageView;

import java.util.HashMap;
import java.util.List;
import java.util.Random;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * @author paniclabs.
 * @created on 1/30/17.
 * @email panic.inc.dev@gmail.com
 * @projectName mtps-n
 * @packageName com.mantapps.reader.mvp.newhome.
 * @className ${CLASS}.
 */
public class NewHomeAdapter<T> extends RecyclerView.Adapter {
    private final Context mContext;
    private final FlipRefreshListener.Listener mListener;
    private final List<T> mList;
    private final ADVSInstreamAdPlacer mAdplacer;
    private boolean mHasMore= true;
    private HashMap<Integer, Integer> colors = new HashMap<>();


    private static final int ITEM_VIEWTYPE_AD      = 0;
    private static final int ITEM_VIEWTYPE_CONTENT = 1;

    private NewHomeFragment.OnHomeSelectedListener homeListener;

    public NewHomeAdapter(Context context, List<T> list, ADVSInstreamAdPlacer adPlacer, NewHomeFragment.OnHomeSelectedListener mListener, FlipRefreshListener.Listener listener) {
        mContext = context;
        mList = list;
        mAdplacer = adPlacer;
        this.homeListener = mListener;
        this.mListener = listener;
        colors.put(1, Color.parseColor("#FF1DD3FA"));
        colors.put(2,Color.parseColor("#FF2288DD"));
        colors.put(3,Color.parseColor("#FFA98D09"));
        colors.put(4,Color.parseColor("#FF7F023D"));
        colors.put(5,Color.parseColor("#FF01B634"));
        colors.put(6,Color.parseColor("#FF10E8A4"));
        colors.put(6857,Color.parseColor("#FF6A05DB"));
        colors.put(6851, Color.parseColor("#FFE302E1"));
    }

    public void setHasMore(boolean hasMore) {
        if (mHasMore == hasMore) {
            return;
        }
        mHasMore = hasMore;
        notifyItemChanged(getItemCount() - 1);
    }

    public void insertData(List<T> post){
        mList.addAll(post);
        notifyDataSetChanged();
    }

    public void insertAds(List<? extends ADVSInstreamInfoModel> items){
        int a = 1;
        for (ADVSInstreamInfoModel adData : items) {

            try{
                mList.add(a, (T) adData);
            }catch (Exception e){
                if(e instanceof IndexOutOfBoundsException){
                    e.printStackTrace();
                }
            }
            //Newslist.add(i,adData);
            a += 2;
        }
        notifyDataSetChanged();
//
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    private boolean isAd(int position) {
        return (mList.get(position) instanceof ADVSInstreamInfoModel) ? true : false;
    }

    @Override
    public int getItemViewType(int position) {
        return isAd(position) ? ITEM_VIEWTYPE_AD : ITEM_VIEWTYPE_CONTENT;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        switch (viewType) {
            case ITEM_VIEWTYPE_AD:
                View mtBurnAdsLayout = LayoutInflater.from(parent.getContext()).inflate(R.layout.custom_instream_ad_view, parent, false);
                return new AdViewHolder(mtBurnAdsLayout, mAdplacer);

            case ITEM_VIEWTYPE_CONTENT:
                // fall through
            default:
                View menuItemLayoutView = LayoutInflater.from(parent.getContext()).inflate(
                        R.layout.home_item_new, parent, false);
                return new FlipPageVh(menuItemLayoutView);
        }
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {
        int viewType = getItemViewType(position);
        switch (viewType) {
            case ITEM_VIEWTYPE_AD:
                ADVSInstreamInfoModel adInfo = (( ADVSInstreamInfoModel) mList.get(position));
                AdViewHolder avh = (AdViewHolder)holder;
                avh.setData(adInfo);
                mAdplacer.measureImp(adInfo);
                break;

            case ITEM_VIEWTYPE_CONTENT:
                //fall through
            default:
                final FlipPageVh flipVh = (FlipPageVh) holder;
                final Post post = ((Post) mList.get(position));
                String excerpt = post.getExcerpt();
                int index = excerpt.indexOf("mantapps.co.id");
                if (index > -1)
                    excerpt = excerpt.substring(0, index);

                flipVh.id_post_description.setText(Html.fromHtml(excerpt));
                AlphaAnimation fadeImage = new AlphaAnimation(0, 1);
                fadeImage.setDuration(3000);
                fadeImage.setInterpolator(new DecelerateInterpolator());

                Glide.with(mContext).load(post.getMediumImageUrl())
                        .animate(fadeImage)
                        .into(flipVh.photo);

                flipVh.txvDate.setText(post.getDate());
                flipVh.id_author.setText(post.getAuthor());
                flipVh.imgAuthor.setOval(true);
                flipVh.imgAuthor.setLetter(post.getAuthor().charAt(0));
                //id_categoriesDesc.setText("ssssss");
                flipVh.id_post_title.setText(post.getTitle());

                flipVh.btnShare.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String body = String.format(mContext.getResources().getString(R.string.share_body), post.getTitle(), post.getUrl());
                        Utils.shareData(mContext, mContext.getResources().getString(R.string.share_title), body);
                    }
                });


                flipVh.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        HashMap<String, String> map = new HashMap<>();
                        XLog.w("id = " + post.getId() +
                                "\nposition = " + position +
                                "\ntitle = " + post.getTitle() +
                                "\nslug = " + post.getSlug() +
                                "\ndate = " + post.getDate() +
                                "\nauthor = " + post.getAuthor() +
                                "\nurl = " + post.getUrl());

                        map.put("id", String.valueOf(post.getId()));
                        map.put("title", post.getTitle());
                        map.put("slug", post.getSlug());
                        map.put("date", post.getDate());
                        map.put("author", post.getAuthor());
                        map.put("content", post.getContent());
                        map.put("url", post.getUrl());
                        map.put("thumbnailURL", post.getMediumImageUrl());
                        homeListener.onFlipSelected(flipVh.photo, map);
                    }
                });


                final Author a = new Author();
                a.setFullName(post.getAuthor());
                a.setAuthorId(post.getAuthorId());
                flipVh.id_author.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(mContext, ArticleListActivity.class);
                        Bundle args = new Bundle();
                        args.putSerializable(ArticleListActivity.AUTHOR, a);
                        intent.putExtras(args);
                        mContext.startActivity(intent);
                    }
                });

                flipVh.c1.setVisibility(View.GONE);
                flipVh.c2.setVisibility(View.GONE);
                flipVh.c3.setVisibility(View.GONE);
                flipVh.c4.setVisibility(View.GONE);
                flipVh.c5.setVisibility(View.GONE);
                flipVh.c6.setVisibility(View.GONE);
                flipVh.c7.setVisibility(View.GONE);
                flipVh.c8.setVisibility(View.GONE);
                for (int i = 1; i <= post.getCategories().size(); i++) {
                    TextView txv = null;
                    switch (i) {
                        case 1:
                            txv = flipVh.c1;
                            break;
                        case 2:
                            txv = flipVh.c2;
                            break;
                        case 3:
                            txv = flipVh.c3;
                            break;
                        case 4:
                            txv = flipVh.c4;
                            break;
                        case 5:
                            txv = flipVh.c5;
                            break;
                        case 6:
                            txv = flipVh.c6;
                            break;
                        case 7:
                            txv = flipVh.c7;
                            break;
                        default:
                            txv = flipVh.c8;
                            break;
                    }

                    final Category c = post.getCategories().get(i - 1);
                    txv.setText(c.getName());
                    txv.setVisibility(View.VISIBLE);
                    if (colors.containsKey(c.getId())) {
                        txv.setBackgroundColor(colors.get(c.getId()));
                    } else {
                        txv.setBackgroundColor(Color.parseColor("#FF777777"));
                    }

                    txv.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Intent intent = new Intent(mContext, CategoryListActivity.class);
                            Bundle args = new Bundle();
                            args.putSerializable(CategoryListActivity.CATEGORY, c);
                            intent.putExtras(args);
                            mContext.startActivity(intent);
                        }
                    });

                    String[] id = {"ca-app-pub-5738233908582263/5107265035","ca-app-pub-5738233908582263/9750505089"};
                    String ids = randomId(id);
                    // Initialize the Mobile Ads SDK.
                    MobileAds.initialize(mContext, ids);
                    // Create an ad request. Check your logcat output for the hashed device ID to
                    // get test ads on a physical device. e.g.
                    // "Use AdRequest.Builder.addTestDevice("ABCDEF012345") to get test ads on this device."
                    AdRequest adRequest = new AdRequest.Builder()
                            .build();


                    boolean experiment1_variant = SharedPrefManager.getInstance().getValue("useAds",Boolean.class,false);
                    XLog.i("REMOTE : " + experiment1_variant);
                    if(experiment1_variant){
                        flipVh.adView.setVisibility(View.VISIBLE);
                        // Start loading the ad in the background.
                        flipVh.adView.loadAd(adRequest);
                    }else flipVh.adView.setVisibility(View.GONE);


                    // TODO: Use your own attributes to track content views in your app
                    Answers.getInstance().logContentView(new ContentViewEvent()
                            .putContentName(post.getTitle())
                            .putContentType("Post")
                            .putContentId(String.valueOf(post.getId()))
                            .putCustomAttribute("Category ", c.getName()));

                }
                break;
        }

    }

    private String randomId(String[] id) {
        Random generator = new Random();
        int randomIndex = generator.nextInt(id.length);
        return id[randomIndex];
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    int getDataCount() {
        return mList.size();
    }

    class FlipPageVh extends RecyclerView.ViewHolder {
        @BindView(R.id.id_post_description) TextView id_post_description;
        @BindView(R.id.txvDate) TextView txvDate;
        @BindView(R.id.id_author) TextView id_author;
        @BindView(R.id.id_post_title) TextView id_post_title;
        @BindView(R.id.img_author) LetterImageView imgAuthor;
        @BindView(R.id.c1) TextView c1;
        @BindView(R.id.c2) TextView c2;
        @BindView(R.id.c3) TextView c3;
        @BindView(R.id.c4) TextView c4;
        @BindView(R.id.c5) TextView c5;
        @BindView(R.id.c6) TextView c6;
        @BindView(R.id.c7) TextView c7;
        @BindView(R.id.c8) TextView c8;
        @BindView(R.id.id_photo)
        ImageView photo;
        @BindView(R.id.btnshare)
        ImageButton btnShare;
        @BindView(R.id.ad_view)AdView adView;

        FlipPageVh(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }

    static class AdViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        ADVSInstreamAdPlacer adPlacer;
        ADVSInstreamInfoModel adData;

        @BindView(R.id.custom_instream_advertiser_name) TextView advertiserName;
        @BindView(R.id.custom_instream_ad_text) TextView adText;
        @BindView(R.id.custom_instream_ad_image) ImageView adImage;


        public AdViewHolder(View itemView, ADVSInstreamAdPlacer adPlacer) {
            super(itemView);
            ButterKnife.bind(this,itemView);
            this.adPlacer = adPlacer;

            itemView.setOnClickListener(this);

            advertiserName = (TextView) itemView.findViewById(R.id.custom_instream_advertiser_name);
            adText = (TextView) itemView.findViewById(R.id.custom_instream_ad_text);
            adImage = (ImageView) itemView.findViewById(R.id.custom_instream_ad_image);
        }

        void setData(ADVSInstreamInfoModel adData) {
            this.adData = adData;

            advertiserName.setText(adData.title());
            adText.setText(adData.content());

            Glide.with(adImage.getContext()).load(adData.creative_url())
                    .thumbnail(0.1f)
                    .into(adImage);
        }

        @Override
        public void onClick(View view) {
            adPlacer.sendClickEvent(adData);
        }
    }
}
