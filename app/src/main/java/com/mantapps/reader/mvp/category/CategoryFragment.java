package com.mantapps.reader.mvp.category;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.elvishew.xlog.XLog;
import com.mantapps.reader.R;
import com.mantapps.reader.app.CategoryListActivity;
import com.mantapps.reader.base.BaseFragment;
import com.mantapps.reader.model.Category;
import com.mantapps.reader.mvp.category.model.CategoryIndexResponse;
import com.mantapps.reader.util.Config;
import com.mantapps.reader.view.LoadingView;
import com.paniclabs.libs.categoryview.CategoryItem;
import com.paniclabs.libs.categoryview.OnCategoryItemClickListener;
import com.paniclabs.libs.categoryview.VRCategoryView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

/**
 * on 2017/01/24
 */

public class CategoryFragment extends BaseFragment implements CategoryContract.View {

    @SuppressLint("StaticFieldLeak")
    public static VRCategoryView mVrCategoryView;
    @BindView(R.id.loadingView)
    LoadingView loadingView;
    @BindView(R.id.colayout)
    CoordinatorLayout coordinatorLayout;

    private CategoryContract.Presenter mPresenter;
    private String[] stringId = {"Brand","News","Tech"};
    private List<CategoryItem> categoryItemList = new ArrayList<>();
    private List<Category> subItemList = new ArrayList<>();

    @Override
    protected void initView(View view, Bundle savedInstanceState) {
        mVrCategoryView = (VRCategoryView) view.findViewById(R.id.vrCategoryView);
        for (int i = 0; i < stringId.length; i++) {
            final int finalI = i;
            CategoryItem categoryItem = new CategoryItem() {
                @Override
                public String getName() {
                    return stringId[finalI];
                }

                @Override
                public Drawable getImage() {
                    return null;
                }

                @Override
                public List<Category> getSubCategoryItems() {
                    return subItemList;
                }
            };
            categoryItemList.add(categoryItem);
        }

        mVrCategoryView.setSubCategoryListBackgroundColor(Color.WHITE);
        final TestAdapter adapter = new TestAdapter(getActivity());
        mVrCategoryView.initialize(categoryItemList, adapter, new OnCategoryItemClickListener() {

            @Override
            public void onCategoryItemSelected(String name, int position, boolean expanded) {
                if(!expanded){
                    switch (position){
                        case 0:
                            mPresenter.loadDataApi(Config.BRAND_ID, new CategoryPresenter.OnCompletedListener() {
                                @Override
                                public void onResult(List<Category> result) {
                                    adapter.setSub(result);
                                    adapter.notifyDataSetChanged();
                                }

                                @Override
                                public void onError() {
                                    XLog.e("ONERROR");
                                }
                            });
                            break;
                        case 1:
                            mPresenter.loadDataApi(Config.NEWS_ID, new CategoryPresenter.OnCompletedListener() {
                                @Override
                                public void onResult(List<Category> result) {
                                    XLog.w(result);
                                    adapter.setSub(result);
                                    adapter.notifyDataSetChanged();
                                }

                                @Override
                                public void onError() {
                                    XLog.e("ONERROR");
                                }
                            });
                            break;
                        case 2:
                            mPresenter.loadDataApi(Config.TECH_ID, new CategoryPresenter.OnCompletedListener() {
                                @Override
                                public void onResult(List<Category> result) {
                                    XLog.w( result.size() + result.toString());
                                    adapter.setSub(result);
                                    adapter.notifyDataSetChanged();
                                }

                                @Override
                                public void onError() {
                                    XLog.e("ONERROR");
                                }
                            });
                            break;
                    }
                }else {
                    adapter.itemsLists.clear();
                }
            }
        });

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mPresenter = new CategoryPresenter(this);
        mPresenter.start();

    }

    @Override
    public void hideLoading() {
        loadingView.setLoading(false);
    }

    @Override
    public void showLoading() {
        loadingView.setLoading(true);
    }

    @Override
    public void tampilkanRespone(CategoryIndexResponse response1) {
        for (int i = 0; i < response1.getCategories().size(); i++) {
            Category subItem = new Category();
            subItem.setName(response1.getCategories().get(i).getTitle());
            subItemList.add(subItem);
        }

    }

    @Override
    public void tampilkanError(int code, String msg) {
        hideLoading();
    }

    @Override
    public void noConnection() {
        Snackbar.make(coordinatorLayout,getString(R.string.no_internet),Snackbar.LENGTH_SHORT).show();
    }

    @Override
    public void saveToDB(Boolean aBoolean) {
        if(aBoolean){

        }
    }

    @Override
    protected int getLayoutId() {
        return R.layout.category_fragment;
    }

    @Override
    protected void setPresenter() {
        mPresenter = new CategoryPresenter(this);
    }

    private class TestAdapter extends VRCategoryView.VRSubCategoryAdapter<ViewHolder> {
        private final Context mContext;
        private List<Category> itemsLists = new ArrayList<>();

        List<CategoryItem> categoryItems;

        TestAdapter(Context context) {
            this.mContext = context;
        }

        void setSub(List<Category> sub){
            itemsLists.clear();
            itemsLists.addAll(sub);
            XLog.e(sub.size() + "\n" + itemsLists.size());
            notifyDataSetChanged();
        }

        @Override
        public void setData(List<CategoryItem> categoryItems) {
            this.categoryItems = categoryItems;
            notifyDataSetChanged();
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View v  = LayoutInflater.from(parent.getContext()).inflate(R.layout.category_item, parent, false);
            return new ViewHolder(v);
        }

        @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR1)
        @Override
        public void onBindViewHolder(final ViewHolder holder, final int position) {
            holder.textCategory.setText(itemsLists.get(position).getName());
//            if(mContext != null && !getActivity().isDestroyed()){
//                mPresenter.setImgUrl(itemsLists.get(position).getId(), url -> {
//                    XLog.w("IMGURLS: " + url);
//                    if(isAdded()){
//                        Glide.with(mContext)
//                                .load(url)
//                                .asBitmap()
//                                .centerCrop()
//                                .diskCacheStrategy(DiskCacheStrategy.RESULT)
//                                .into(holder.imgCategory);
//                    }
//
//                });
//            }
            Category item = itemsLists.get(position);
            if(item.getImgUrl() !=null){
                Glide.with(mContext)
                    .load(item.getImgUrl())
                    .asBitmap()
                    .centerCrop()
                    .diskCacheStrategy(DiskCacheStrategy.RESULT)
                    .into(holder.imgCategory);
            }

            holder.imgCategory.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(getActivity(), CategoryListActivity.class);
                    Bundle args = new Bundle();
                    args.putSerializable(CategoryListActivity.CATEGORY, itemsLists.get(position));
                    intent.putExtras(args);
                    getActivity().startActivity(intent);
                }
            });
        }

        @Override
        public int getItemCount() {
            return itemsLists == null ? 0 : itemsLists.size();
        }

    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        private final TextView textCategory;
        private final ImageView imgCategory;

        public ViewHolder(View itemView) {
            super(itemView);
            textCategory = (TextView) itemView.findViewById(R.id.id_category_text);
            imgCategory = (ImageView) itemView.findViewById(R.id.id_category_image);

        }
    }

    @Override
    public void onDestroyView() {
        mPresenter.stop();
        super.onDestroyView();
    }
}