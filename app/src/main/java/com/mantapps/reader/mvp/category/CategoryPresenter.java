package com.mantapps.reader.mvp.category;

import android.os.AsyncTask;
import android.util.Log;

import com.elvishew.xlog.XLog;
import com.mantapps.reader.api.MantappsApi;
import com.mantapps.reader.model.Category;
import com.mantapps.reader.mvp.category.model.CategoriesItem;
import com.mantapps.reader.mvp.category.model.CategoryIndexResponse;
import com.mantapps.reader.util.Config;
import com.mantapps.reader.util.JSONParser;
import com.mantapps.reader.util.RestUtil;
import com.paniclabs.libs.datastorage.DS;

import org.json.JSONException;
import org.json.JSONObject;
import org.reactivestreams.Publisher;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import io.reactivex.Flowable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;
import okhttp3.ResponseBody;
import retrofit2.Call;

/**
* Created by panicLabs on 2017/01/24
*/

public class CategoryPresenter implements CategoryContract.Presenter {

    private CategoryContract.View mCategoryView;
    CompositeDisposable compositeDisposable;

    public CategoryPresenter(CategoryContract.View categoryView) {
        mCategoryView = categoryView;
        compositeDisposable = new CompositeDisposable();
    }
    @Override
    public void start() {
        loadDatas(Config.categoryIndex);
    }

    private void loadDatas(Integer[] categoryIndex) {
        Flowable<Integer> indCat = Flowable.fromArray(categoryIndex);
        Disposable sss = indCat
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .flatMap(new Function<Integer, Publisher<Boolean>>() {
                    @Override
                    public Publisher<Boolean> apply(@NonNull Integer integer) throws Exception {
                        return indexResponseFlowable(integer);
                    }
                }).subscribe(aBoolean -> {
                    if(aBoolean){
                        Log.e("LOADED", "True");
                        savetodb();
                        mCategoryView.saveToDB(true);
                    }
                });

        compositeDisposable.add(sss);
    }

    private void savetodb() {
        List<CategoriesItem> brandList = new ArrayList<>();
        List<CategoriesItem> newsList = new ArrayList<>();
        List<CategoriesItem> techList = new ArrayList<>();
        if(DS.grab("Brand") !=null){
            brandList.addAll(DS.grab("Brand"));
        }
        if(DS.grab("News") !=null){
            newsList.addAll(DS.grab("News"));
        }
        if(DS.grab("Tech") !=null){
            techList.addAll(DS.grab("Tech"));
        }


    }

    private Flowable<Boolean> indexResponseFlowable(int id){
        return RestUtil.withRx().getCategoryIndexs(id)
                .map(response -> {
                    switch (id){
                        case Config.BRAND_ID:
                            DS.entry("Brand", response.getCategories());
                            break;
                        case Config.NEWS_ID:
                            DS.entry("News", response.getCategories());
                            break;
                        case Config.TECH_ID:
                            DS.entry("Tech", response.getCategories());
                            break;
                    }
                    return response.getStatus().equals("ok");
                });
    }

    private Flowable<Boolean> saveToDbFLow(int id){
        return RestUtil.withRx().getCategoryIndexs(id)
                .map(new Function<CategoryIndexResponse, Boolean>() {
                    @Override
                    public Boolean apply(@NonNull CategoryIndexResponse response) throws Exception {
                        return null;
                    }
                });
    }

    @Override
    public void loadData(final long id) {


//        if(Utils.isOnline(AppController.getInstance())) {
//            mCategoryView.showLoading();
//            RestUtil.withRx().getCategoryIndexs(id).subscribe(response -> {
//                mCategoryView.hideLoading();
//                switch (id) {
//                    case Config.BRAND_ID:
//                        DS.entry("Brand", response.getCategories());
//                        break;
//                    case Config.NEWS_ID:
//                        DS.entry("News", response.getCategories());
//                        break;
//                    case Config.TECH_ID:
//                        DS.entry("Tech", response.getCategories());
//                        break;
//                }
//            });
//        }


//            RestUtil.getInstance().create(MantappsApi.class).getCategoryIndex(id).enqueue(new Callback<CategoryIndexResponse>() {
//                @Override
//                public void onResponse(Call<CategoryIndexResponse> call, Response<CategoryIndexResponse> response) {
//                    mCategoryView.hideLoading();
//                    if(response.isSuccessful()){
//                        switch (id){
//                            case Config.BRAND_ID:
//                                DS.entry("Brand", response.body().getCategories());
//                                break;
//                            case Config.NEWS_ID:
//                                DS.entry("News", response.body().getCategories());
//                                break;
//                            case Config.TECH_ID:
//                                DS.entry("Tech", response.body().getCategories());
//                                break;
//                        }
//                        mCategoryView.tampilkanRespone(response.body());
//                    }else {
//                        mCategoryView.tampilkanError(0,"error");
//                    }
//                }
//
//                @Override
//                public void onFailure(Call<CategoryIndexResponse> call, Throwable t) {
//                    mCategoryView.hideLoading();
//                    if (t instanceof HttpEx) {
//                        switch (((HttpEx) t).response().code()) {
//                            case 404:
//                                mCategoryView.tampilkanError(404,"tidak ada jaringan");
//                                Toast.makeText(AppController.getInstance(), "Tidak ada jaringan", Toast.LENGTH_SHORT).show();
//                                //mTextView.setText("Thingy not found");
//                                break;
//                            case 500:
//                                mCategoryView.tampilkanError(500,"server Bermasalah");
//                                Toast.makeText(AppController.getInstance(), "Server sedang bermasalah", Toast.LENGTH_SHORT).show();
//                                break;
//                        }
//                    } else {
//                        mCategoryView.tampilkanError(666,"null");
//                        Toast.makeText(AppController.getInstance(), "Tidak ada jaringan", Toast.LENGTH_SHORT).show();
//                    }
//                }
//            });
//        }else {
//            mCategoryView.noConnection();
//        }

    }

    public interface OnCompletedListener {
        void onResult(List<Category> result);

        void onError();
    }

    @Override
    public void loadDataApi(int id, OnCompletedListener listener) {
        List<CategoriesItem> resultCache = new ArrayList<>();

        switch (id){
            case Config.BRAND_ID:
                if(DS.grab("Brand") != null){
                    resultCache = DS.grab("Brand");
                }else resultCache = null;
                break;
            case Config.NEWS_ID:
                if(DS.grab("News") != null){
                    resultCache = DS.grab("News");
                }else resultCache = null;
                break;
            case Config.TECH_ID:
                if(DS.grab("Tech") != null){
                    resultCache = DS.grab("Tech");
                }else resultCache = null;
                break;
        }

        if(resultCache != null){

            XLog.w("from cache");
            List<CategoriesItem> finalResultCache = resultCache;
            Disposable dd = Flowable.fromIterable(resultCache)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                                .flatMap(new Function<CategoriesItem, Flowable<String>>() {
                                    @Override
                                    public Flowable<String> apply(@NonNull CategoriesItem categoriesItem) throws Exception {
                                        return stringObservable(categoriesItem.getId());
                                    }
                                }).flatMap(s -> Flowable.fromCallable(() -> {
                                    XLog.e("CATS2 :" + finalResultCache.size());
                                    List<Category> results = new ArrayList<>();
                                    Category ix = new Category();
                                    for (int i = 0; i < finalResultCache.size(); i++) {
                                        ix.setName(finalResultCache.get(i).getTitle());
                                        ix.setId(finalResultCache.get(i).getId());
                                        ix.setParent(finalResultCache.get(i).getParent());
                                        ix.setImageUrl(s);
                                        results.add(ix);
                                    }
                                    XLog.e("CATS :" + results.size());
                                    return results;
                                }))
                                .subscribe(new Consumer<List<Category>>() {
                                    @Override
                                    public void accept(@NonNull List<Category> result) throws Exception {
                                        XLog.e("CATS SIZE = " + result.size());
                                        listener.onResult(result);
                                    }
                                });

            compositeDisposable.add(dd);
        }
    }

    @Override
    public void setImgUrl(int id, OnImageCategoryListener listener) {
//        Disposable ss = stringObservable(id)
//                .subscribe(listener::onImageLoaded);
//
//        compositeDisposable.add(ss);
    }

    @Override
    public void stop() {
        mCategoryView = null;
        compositeDisposable.clear();
    }

    private Flowable<String> stringObservable(int id){
        return RestUtil.withRx().getCatePost(id,"1","1")
                .concatMap(response -> Flowable.fromCallable(() -> {
                    String result = "";
                    if (response.isSuccessful()) {
                        JSONObject jsonObject = new JSONObject(response.body().string());
                        result = JSONParser.parsePosts(jsonObject).get(0).getMediumImageUrl();
                        XLog.w("RESULTIMG : " + result);
                    }
                    return result;
                }));
    }



    private class GetImageUrlTask extends AsyncTask<Integer, Integer,  String> {

        private final OnImageCategoryListener imgListener;

        GetImageUrlTask(OnImageCategoryListener listener) {
            imgListener = listener;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            XLog.w("IMGURL = " + result);
            if(imgListener != null){
                try{
                    imgListener.onImageLoaded(result);
                }catch (Exception e){
                    e.printStackTrace();
                }

            }

        }

        @Override
        protected String doInBackground(Integer... params) {
            String url = Config.CATEGORY_PORTS + params[0];
            String result = "";
            Call<ResponseBody> callApiImage = RestUtil.getInstance().create(MantappsApi.class).getCategoryPost(url);

            try {
                ResponseBody body = callApiImage.execute().body();
                JSONObject jsonObject = new JSONObject(body.string());
                result = JSONParser.parsePosts(jsonObject).get(0).getMediumImageUrl();
            } catch (IOException | JSONException e) {
                e.printStackTrace();
            }

            return result;
        }

    }

    interface OnImageCategoryListener {
        void onImageLoaded(String url);
    }

}