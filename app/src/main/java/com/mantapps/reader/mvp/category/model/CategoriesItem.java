package com.mantapps.reader.mvp.category.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class CategoriesItem implements Serializable{

	@SerializedName("parent")
	@Expose
	private int parent;

	@SerializedName("description")
	@Expose
	private String description;

	@SerializedName("id")
	@Expose
	private int id;

	@SerializedName("post_count")
	@Expose
	private int postCount;

	@SerializedName("title")
	@Expose
	private String title;

	@SerializedName("slug")
	@Expose
	private String slug;
	private String images;

	public void setParent(int parent){
		this.parent = parent;
	}

	public int getParent(){
		return parent;
	}

	public void setDescription(String description){
		this.description = description;
	}

	public String getDescription(){
		return description;
	}

	public void setId(int id){
		this.id = id;
	}

	public int getId(){
		return id;
	}

	public void setPostCount(int postCount){
		this.postCount = postCount;
	}

	public int getPostCount(){
		return postCount;
	}

	public void setTitle(String title){
		this.title = title;
	}

	public String getTitle(){
		return title;
	}

	public void setSlug(String slug){
		this.slug = slug;
	}

	public String getSlug(){
		return slug;
	}

	@Override
	public String toString() {
		return "CategoriesItem{" +
				"parent=" + parent +
				", description='" + description + '\'' +
				", id=" + id +
				", postCount=" + postCount +
				", title='" + title + '\'' +
				", slug='" + slug + '\'' +
				'}';
	}

	public void setImage(String image) {
		this.images = image;
	}

	public String getImages() {
		return images;
	}
}