//package com.mantapps.reader.mvp.home;
//
//
//import com.mantapps.reader.base.BaseView;
//import com.mantapps.reader.base.BasePresenter;
//import com.mantapps.reader.model.Post;
//
//import java.util.ArrayList;
//import java.util.List;
//
///**
// * @author paniclabs.
// * @created on 1/26/17.
// * @email panic.inc.dev@gmail.com
// * @projectName mantapps_mvp
// * @packageName com.mantapps.reader.mvp.home.
// * @className ${CLASS}.
// */
//public class HomeContract {
//
//	interface View extends BaseView<Presenter> {
//        void tampilkanData(List<Post> posts);
//
//        void onErrorView();
//
//        void hideLoading();
//        void showLoading();
//
//        void showDialog();
//
//        void hideDialog();
//
//        void onLoadMoreSukses(ArrayList<Post> posts);
//    }
//
//	interface Presenter extends BasePresenter {
//        void loadMore(int page);
//	}
//
//}