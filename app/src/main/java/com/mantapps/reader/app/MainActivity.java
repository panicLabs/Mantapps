package com.mantapps.reader.app;

import android.app.ActivityOptions;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.transition.ChangeImageTransform;
import android.widget.ImageView;

import com.elvishew.xlog.XLog;
import com.mantapps.reader.R;
import com.mantapps.reader.adaptor.PagerAdapterBottomBar;
import com.mantapps.reader.fragment.CategoryFragment;
import com.mantapps.reader.fragment.SearchFragment;
import com.mantapps.reader.mvp.newhome.NewHomeFragment;
import com.mantapps.reader.service.FeedService;
import com.mantapps.reader.util.FirebaseAnalyticsHelper;
import com.mantapps.reader.util.FirebaseAnalyticsParams;
import com.mantapps.reader.util.Utils;

import java.util.HashMap;
import java.util.UUID;

import butterknife.ButterKnife;


public class MainActivity extends AppCompatActivity implements //HomeFragment.OnNewsSelectedListener {
        NewHomeFragment.OnHomeSelectedListener {
    private static final String TAG = MainActivity.class.getSimpleName();


    private ViewPager viewPager;
    private NewHomeFragment homeFragment;
    private SearchFragment searchFragment;
    private CategoryFragment categoryFragment;
    private TabLayout tabLayout;

    private int[] tabIcons = {
            R.drawable.home_btn_selector,
            R.drawable.category_btn_selector,
            R.drawable.search_btn_selector
    };
//    private InterstitialAd fbInterstitialAd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        if (android.os.Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }

        logEvent();

        viewPager = (ViewPager) findViewById(R.id.pager);
        viewPager.setOffscreenPageLimit(3);
        setupViewPager(viewPager);

        tabLayout = (TabLayout) findViewById(R.id.tabLayout);
        tabLayout.setupWithViewPager(viewPager);
        setupTabIcons();

//        fbInterstitialAd = new com.facebook.ads.InterstitialAd(this, "180164295839771_249770228879177");
//        // Set listeners for the Interstitial Ad
//        fbInterstitialAd.setAdListener(new InterstitialAdListener() {
//            @Override
//            public void onInterstitialDisplayed(Ad ad) {
////                showToast("Interstitial: Displayed");
//            }
//
//            @Override
//            public void onInterstitialDismissed(Ad ad) {
//                fbInterstitialAd.loadAd();
////                showToast("Interstitial: Dismissed");
//            }
//
//            @Override
//            public void onError(Ad ad, AdError adError) {
////                showToast("Interstitial: onError");
//            }
//
//            @Override
//            public void onAdLoaded(Ad ad) {
////                showToast("Interstitial: onAdLoaded");
//            }
//
//            @Override
//            public void onAdClicked(Ad ad) {
////                showToast("Interstitial: onAdClicked");
//            }
//
//            @Override
//            public void onLoggingImpression(Ad ad) {
////                showToast("Interstitial: onLoggingImpression");
//            }
//        });
//        fbInterstitialAd.loadAd();

    }

//    @OnClick(R.id.homes)
//    void onLogoClicked(View v){
//        Intent intentDynamicLink = new Intent();
//        intentDynamicLink.setClass(this, DynamicLinkActivity.class);
//        startActivity(intentDynamicLink);
//    }

    private void logEvent() {
        XLog.w(TAG, "Generating event's variable values");

        String uuid = "" + UUID.randomUUID().toString().replace("-", "").substring(0, 30);

        String id = "id-" + uuid;
        String name = "name-" + uuid;

        FirebaseAnalyticsParams params = new FirebaseAnalyticsParams
                .Builder().param1(id).param2(100).param3(name).build();
        FirebaseAnalyticsHelper.getInstance(this).logEvent("MainActivity", params);
    }

    private void setupTabIcons() {
        tabLayout.getTabAt(0).setIcon(tabIcons[0]);
        tabLayout.getTabAt(1).setIcon(tabIcons[1]);
        tabLayout.getTabAt(2).setIcon(tabIcons[2]);
    }

    private void setupViewPager(ViewPager viewPager) {
        PagerAdapterBottomBar adapter = new PagerAdapterBottomBar(getSupportFragmentManager());
        homeFragment = new NewHomeFragment();
        categoryFragment = new CategoryFragment();
        searchFragment = new SearchFragment();
        adapter.addFragment(homeFragment);
        adapter.addFragment(categoryFragment);
        adapter.addFragment(searchFragment);
        viewPager.setAdapter(adapter);
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                switch (position) {
                    case 0:
                        break;
                    case 1:
                        break;
                    case 2:
                        break;
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    @Override
    public void onNewIntent(Intent intent) {
        Bundle extras = intent.getExtras();
        if (extras != null) {
            if (extras.containsKey(FeedService.ULR_FROM_NOTIFICATION_EXTRA)) {
                Intent i = new Intent(MainActivity.this, WebViewActivity.class);
                i.setData(Uri.parse(extras.getString(FeedService.ULR_FROM_NOTIFICATION_EXTRA)));
                startActivity(i);
                // mWebView.loadUrl(extras.getString(FeedService.ULR_FROM_NOTIFICATION_EXTRA));
            }
        }
    }

    /**
     * Invoked when an article in a list is selected
     *
     * @param id_photo
     * @param map      HashMap that stores properties of a Post object
     */
    @Override
    public void onFlipSelected(ImageView id_photo, HashMap<String, String> map) {
        // Set necessary arguments
        Bundle args = new Bundle();
        args.putInt("id", Integer.valueOf(map.get("id")));
        args.putString("title", map.get("title"));
        args.putString("slug", map.get("slug"));
        args.putString("date", map.get("date"));
        args.putString("author", map.get("author"));
        args.putString("content", map.get("content"));
        args.putString("url", map.get("url"));
        args.putString("thumbnailUrl", map.get("thumbnailURL"));

        Intent intent = new Intent(MainActivity.this, DetailActivity.class);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            String shareElementName = "sharedImageView";
            ActivityOptions activityOptions = ActivityOptions.makeSceneTransitionAnimation(MainActivity.this, id_photo, shareElementName);
            getWindow().setSharedElementEnterTransition(new ChangeImageTransform(MainActivity.this, null));
            intent.putExtra(DetailActivity.SHARED_ELEMENT_KEY, shareElementName);
            intent.putExtras(args);
            intent.putExtra(DetailActivity.IMAGE_RES_ID, map.get("thumbnailURL"));
            startActivity(intent, activityOptions.toBundle());
        } else {
            intent.putExtras(args);
            intent.putExtra(DetailActivity.IMAGE_RES_ID, map.get("thumbnailURL"));
            startActivity(intent);
        }

    }

    public void showNoticeDialog(final String downUrl, final boolean force) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(R.string.softwareTitle);
        builder.setMessage(R.string.softwareMessage);
        builder.setPositiveButton(R.string.softwarePositiveButton,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        Utils.initDownManager(MainActivity.this, downUrl);
                    }
                });
        if (force) {
            builder.setCancelable(false);
        }
        builder.setNegativeButton(R.string.softwareNegativeButton,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        if (force) {
                            finish();
                        }
                    }
                });
        Dialog noticeDialog = builder.create();
        noticeDialog.show();
    }

    @Override
    protected void onPause() {
//        if (fbInterstitialAd != null && fbInterstitialAd.isAdLoaded()) {
//            fbInterstitialAd.show();
//        }
        super.onPause();

    }

    @Override
    public void onBackPressed() {
        // startAppAd.onBackPressed();
        super.onBackPressed();

    }
}
