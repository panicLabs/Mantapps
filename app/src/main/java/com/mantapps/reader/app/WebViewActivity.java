package com.mantapps.reader.app;

import android.net.Uri;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.mantapps.reader.util.Utils;
import com.mantapps.reader.view.ClickableWebView;
import com.mantapps.reader.view.OnWebViewClicked;
import com.mantapps.reader.view.ProgressWebView;
import com.mantapps.reader.R;
import com.mantapps.reader.view.StateView;

/**
 * Created by paniclabs on 11/16/16.
 */

public class WebViewActivity extends SwipeBaseActivity{
    ProgressWebView mWebView;
    private StateView mStateView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_webview);
        mStateView = StateView.inject(this,true);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);


        Uri uri = getIntent().getData();
        Log.w("uri", uri.toString());

        mWebView = (ProgressWebView) findViewById(R.id.webview);
        ClickableWebView wv = mWebView.getWebView();

        wv.setOnWebViewClickListener(new OnWebViewClicked() {
            @Override
            public void onClick(View view, String url) {
                Toast.makeText(WebViewActivity.this, url, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onOpenWeb(String extra) {

            }
        });

        wv.setWebViewClient(new WebViewClient() {
            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                mStateView.showContent();
               // setTitle(view.getTitle());
            }
        });

        if (uri != null && Utils.isOnline(getApplicationContext())) {
            mStateView.showLoading();
            mWebView.loadUrl(uri.toString());
        }else {
            mStateView.showRetry();
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onResume() {
        super.onResume();
    }
}
