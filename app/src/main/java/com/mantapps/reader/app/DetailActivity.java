package com.mantapps.reader.app;


import android.annotation.SuppressLint;
import android.app.ActivityOptions;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.Toolbar;
import android.transition.ChangeImageTransform;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.elvishew.xlog.XLog;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;
import com.mantapps.reader.R;
import com.mantapps.reader.api.MantappsApi;
import com.mantapps.reader.model.Category;
import com.mantapps.reader.model.slug.CategoriesItem;
import com.mantapps.reader.model.slug.SlugResponse;
import com.mantapps.reader.util.Config;
import com.mantapps.reader.util.FirebaseAnalyticsHelper;
import com.mantapps.reader.util.FirebaseAnalyticsParams;
import com.mantapps.reader.util.RestUtil;
import com.mantapps.reader.util.Utils;
import com.mantapps.reader.view.ClickableWebView;
import com.mantapps.reader.view.LoadingView;
import com.mantapps.reader.view.OnWebViewClicked;
import com.paniclabs.libs.datastorage.SharedPrefManager;

import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * Created by paniclabs on 11/8/16.
 */

public class DetailActivity extends SwipeBaseActivity {


    public static final String SHARED_ELEMENT_KEY = "SHARED_ELEMENT_KEY";
    public static final String IMAGE_RES_ID = "imageResId";

    ClickableWebView webView;
    ImageView imageView;
    AppBarLayout toolbarLayout;
    LoadingView loadingView;
    TextView c1, c2, c3, c4,c5, c6, c7, c8;
    private HashMap<Integer, Integer> colors;

    private int id;
    private String title;
    private String content;
    private String url;
    private AdView mAdView;

    @SuppressLint("SetJavaScriptEnabled")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        colors = new HashMap<>();
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);

        colors.put(1, Color.parseColor("#FF1DD3FA"));
        colors.put(2,Color.parseColor("#FF2288DD"));
        colors.put(3,Color.parseColor("#FFA98D09"));
        colors.put(4,Color.parseColor("#FF7F023D"));
        colors.put(5,Color.parseColor("#FF01B634"));
        colors.put(6,Color.parseColor("#FF10E8A4"));
        colors.put(6857,Color.parseColor("#FF6A05DB"));
        colors.put(6851,Color.parseColor("#FFE302E1"));

        toolbarLayout = (AppBarLayout) findViewById(R.id.app_bar);
        loadingView = (LoadingView) findViewById(R.id.loadingView);
        webView = (ClickableWebView) findViewById(R.id.web);
        imageView = (ImageView) findViewById(R.id.image);
        loadingView.setLoading(true);

        // Get data
        Bundle args = getIntent().getExtras();
        id = args.getInt("id");
        title = args.getString("title");

        initImageEnterTransition();

        c1 = (TextView) findViewById(R.id.c1);
        c2 = (TextView) findViewById(R.id.c2);
        c3 = (TextView) findViewById(R.id.c3);
        c4 = (TextView) findViewById(R.id.c4);
        c5 = (TextView) findViewById(R.id.c5);
        c6 = (TextView) findViewById(R.id.c6);
        c7 = (TextView) findViewById(R.id.c7);
        c8 = (TextView) findViewById(R.id.c8);


        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        setTitle(title);

        String slug = args.getString("slug");
        if(slug != null ) loadApi(slug);


        Log.w("SLUG = ", slug);
        String date = args.getString("date");
        String author = args.getString("author");
        content = args.getString("content");

        FirebaseAnalyticsParams params = new FirebaseAnalyticsParams
                .Builder().param1(title).param2(id).param3(slug).build();
        FirebaseAnalyticsHelper.getInstance(this).logEvent("DETAIL_POST", params);

        AppController.getInstance().getFirebaseAnalytics().setUserProperty("Title",title);

        // Initialize the Mobile Ads SDK.
        MobileAds.initialize(this, "ca-app-pub-5738233908582263/9750505089");

        mAdView = (AdView)findViewById(R.id.ad_view);

        AdRequest request = new AdRequest.Builder().build();

        boolean experiment1_variant = SharedPrefManager.getInstance().getValue("useAds",Boolean.class,false);
        XLog.i("REMOTE : " + experiment1_variant);
        if(experiment1_variant){
            mAdView.setVisibility(View.VISIBLE);
            // Start loading the ad in the background.
            mAdView.loadAd(request);
        }else mAdView.setVisibility(View.GONE);

        String replaceStr = content.substring(content.indexOf( "<div class=\"sharedaddy" ), content.length()) ;
        Log.e("content" , content);
        Log.e("replaceStr " , replaceStr);
        String newContent = content.replace(replaceStr,"\n");
        Log.e("newContent", newContent);
        url = args.getString("url");
        //thumbnailUrl = args.getString("thumbnailUrl");


        // Construct HTML content
        // First, some CSS
        String html = "<head><LINK href=\"http://www.mantapps.co.id/wp-content/themes/hueman/style.css\" type=\"text/css\" rel=\"stylesheet\"/>" +
                "<LINK href=\"http://www.mantapps.co.id/wp-content/themes/hueman/fonts/font-awesome.min.css\" type=\"text/css\" rel=\"stylesheet\"/>" +
                "<LINK href=\"http://www.mantapps.co.id/wp-content/cache/autoptimize/css/autoptimize_5ebc92e22bb225ccd5143307459a5362.css\" type=\"text/css\" rel=\"stylesheet\"/></head>";
//        html += "<style>img{max-width:100%;height:auto;} " +
//                ".titleImg{width:100%;}" +
//                "iframe{width:100%;height:56%;}</style> ";
//
//        html += "<a href=\""+ thumbnailUrl + "\" ><img src=\"" + thumbnailUrl + "\" class=\"titleImg\" /></a>";
//        // Article Title
        // Date & author
        html += "<h4 style=\"color:#999999;font-size:0.8em;\">" + date + " by " + author + "</h4>";


        html += "<div style=\"padding:10px;padding-top:20px;\">";
        html += "<h1 style=\"font-size:1.4em;\">" + title + "</h1><br /> ";
        // The actual content
        html += newContent;
        html += "</div>";
        html += "<div style=\"text-align:center; background:#cdcdcd; padding:22px 0px;margin:20px 0; width:100%; font-size:2em; \"---------------<br />";
        html += "<a href=\""+ url + "\" >MORE DETAIL</a><br />";
        html += "</div><br />";

        // Enable JavaScript in order to be able to Play Youtube videos
        webView.getSettings().setJavaScriptEnabled(true);
        webView.setWebViewClient(new myWebClient());
        webView.setWebChromeClient(new WebChromeClient());
        // Load and display HTML content
        // Use "charset=UTF-8" to support non-English language
        webView.loadData(html, "text/html; charset=UTF-8", null);

        webView.setOnWebViewClickListener(new OnWebViewClicked() {
            @Override
            public void onClick(View view, String url) {
                Log.w(DetailActivity.class.getSimpleName(),"clicked");
                Intent intent = new Intent(DetailActivity.this, PhotoActivity.class);
                intent.setData(Uri.parse(url));
                intent.putExtra(PhotoActivity.IMAGE_RES_ID, url);
                startActivity(intent);
            }

            @Override
            public void onOpenWeb(String extraUrl) {
                Intent intent = new Intent(DetailActivity.this, WebViewActivity.class);
                intent.setData(Uri.parse(extraUrl));
                startActivity(intent);
            }
        });

    }

    private void initImageEnterTransition() {
        String imageTransitionName = getIntent().getStringExtra(SHARED_ELEMENT_KEY);
        ViewCompat.setTransitionName(imageView, imageTransitionName);
        String resId = getIntent().getExtras().getString(IMAGE_RES_ID);
        Glide.with(getApplicationContext())
                .load(resId)
                .thumbnail(0.2f)
                .into(imageView);
    }

    private void loadApi(String slug) {

        RestUtil.getInstance().create(MantappsApi.class).getSlug(slug).enqueue(new Callback<SlugResponse>() {
            @Override
            public void onResponse(Call<SlugResponse> call, Response<SlugResponse> response) {
                Log.w("respone", "sukses ? " + response.isSuccessful());
                loadingView.setLoading(false);
                if(response.isSuccessful()){
                    initView(response);
                }
            }

            @Override
            public void onFailure(Call<SlugResponse> call, Throwable t) {
                loadingView.setLoading(false);
                t.printStackTrace();
            }
        });
    }

    private void initView(Response<SlugResponse> response) {

        final String imageUrl;
        if(response.body().getPost().getThumbnail_images().getFull().getUrl() != null)
            imageUrl = response.body().getPost().getThumbnail_images().getFull().getUrl();
        else imageUrl = Config.DEFAULT_THUMBNAIL_URL;

        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(DetailActivity.this, PhotoActivity.class);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                String shareElementName = "sharedImageView";
                ActivityOptions activityOptions = ActivityOptions.makeSceneTransitionAnimation(DetailActivity.this, imageView, shareElementName);
                    getWindow().setSharedElementEnterTransition(new ChangeImageTransform(DetailActivity.this, null));
                intent.putExtra(PhotoActivity.SHARED_ELEMENT_KEY, shareElementName);
                intent.setData(Uri.parse(imageUrl));
                intent.putExtra(PhotoActivity.IMAGE_RES_ID, imageUrl);
                startActivity(intent, activityOptions.toBundle());
                }else {
                    intent.setData(Uri.parse(imageUrl));
                    intent.putExtra(PhotoActivity.IMAGE_RES_ID, imageUrl);
                    startActivity(intent);
                }

            }
        });

        c1.setVisibility(View.GONE);
        c2.setVisibility(View.GONE);
        c3.setVisibility(View.GONE);
        c4.setVisibility(View.GONE);
        c5.setVisibility(View.GONE);
        c6.setVisibility(View.GONE);
        c7.setVisibility(View.GONE);
        c8.setVisibility(View.GONE);
        for (int i = 1;i <=response.body().getPost().getCategories().size();i++ ){
            TextView txv = null;
            switch (i) {
                case 1:
                    txv = c1;
                    break;
                case 2:
                    txv = c2;
                    break;
                case 3:
                    txv = c3;
                    break;
                case 4:
                    txv = c4;
                    break;
                case 5:
                    txv = c5;
                    break;
                case 6:
                    txv = c6;
                    break;
                case 7:
                    txv = c7;
                    break;
                default:
                    txv = c8;
                    break;
            }

            final CategoriesItem c = response.body().getPost().getCategories().get(i - 1);
            txv.setText(c.getTitle());
            txv.setVisibility(View.VISIBLE);
            if(colors.containsKey(c.getId())){
                txv.setBackgroundColor(colors.get(c.getId()));
            }else{
                txv.setBackgroundColor(Color.parseColor("#FF777777"));
            }

            txv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Category category = new Category();
                    category.setId(c.getId());
                    category.setName(c.getTitle());
                    category.setParent(c.getParent());

                    Intent intent = new Intent(DetailActivity.this, CategoryListActivity.class);
                    Bundle args = new Bundle();
                    args.putSerializable(CategoryListActivity.CATEGORY, category);
                    intent.putExtras(args);
                    startActivity(intent);
                }
            });
        }


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_detail, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == android.R.id.home) {
            onBackPressed();
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                finishAfterTransition();
            }
        } else if (id == R.id.action_share) {
            String body = String.format(getResources().getString(R.string.share_body), title, url);
            Utils.shareData(DetailActivity.this, getResources().getString(R.string.share_title), body);
        }
        return super.onOptionsItemSelected(item);
    }

    public String htmlifyText(String txt) {

        String html = "";
        html = "<!DOCTYPE html>\n" +
                "<html>\n" +
                "\n" +
                "<head>\n" +
                "<meta http-equiv=\"content-type\" content=\"text/html; charset=utf-8\"/>\n" +
                "<style> \n" +
                "img{display: inline; max-width:100%; width:auto; height:auto;}" +
                "table{border-collapse: collapse !important;width:auto !important; }" +
                "table, th, td {\n" +
                "    border: 1px solid black; width:100% !important; \n" +
                "}" +
                "body{text-align:justify;color:#212121 !important;}" +
                "</style>\n" +
                "</head>\n" +
                "<body style=\"\">"
                + txt + "</body></html>";
        return html;

    }

    @Override
    public void onPause() {
        if (mAdView != null) {
            mAdView.pause();
        }
        super.onPause();
        if (webView != null)
            webView.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (mAdView != null) {
            mAdView.resume();
        }
        if (webView != null)
            webView.onResume();
    }


    @Override
    protected void onDestroy() {
        if (mAdView != null) {
            mAdView.destroy();
        }
        super.onDestroy();
    }

    private class myWebClient extends WebViewClient {
        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            // TODO Auto-generated method stub
            super.onPageStarted(view, url, favicon);
        }

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, final String url) {
            // TODO Auto-generated method stub
//            Intent intent = new Intent(DetailActivity.this, WebViewActivity.class);
//            intent.setData(Uri.parse(url));
//            startActivity(intent);
            return true;

        }
    }
}
