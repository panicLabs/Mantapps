package com.mantapps.reader.app;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.multidex.MultiDex;
import android.support.multidex.MultiDexApplication;
import android.support.v7.app.AppCompatDelegate;

import com.crashlytics.android.Crashlytics;
import com.elvishew.xlog.LogConfiguration;
import com.elvishew.xlog.LogLevel;
import com.elvishew.xlog.XLog;
import com.elvishew.xlog.formatter.border.DefaultBorderFormatter;
import com.elvishew.xlog.formatter.message.json.DefaultJsonFormatter;
import com.elvishew.xlog.formatter.message.throwable.DefaultThrowableFormatter;
import com.elvishew.xlog.formatter.message.xml.DefaultXmlFormatter;
import com.elvishew.xlog.formatter.stacktrace.DefaultStackTraceFormatter;
import com.elvishew.xlog.formatter.thread.DefaultThreadFormatter;
import com.elvishew.xlog.printer.AndroidPrinter;
import com.elvishew.xlog.printer.Printer;
import com.elvishew.xlog.printer.SystemPrinter;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.remoteconfig.FirebaseRemoteConfig;
import com.google.firebase.remoteconfig.FirebaseRemoteConfigSettings;
import com.mantapps.reader.BuildConfig;
import com.mantapps.reader.util.MiscUtil;
import com.mantapps.reader.util.OkHttpImageDownloader;
import com.nostra13.universalimageloader.cache.disc.impl.UnlimitedDiskCache;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.utils.StorageUtils;
import com.paniclabs.libs.cache.EnhancedCacheInterceptor;
import com.paniclabs.libs.datastorage.DS;
import com.paniclabs.libs.datastorage.SharedPrefManager;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

import io.fabric.sdk.android.Fabric;
import okhttp3.Cache;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;

import static okhttp3.logging.HttpLoggingInterceptor.Level.BODY;
import static okhttp3.logging.HttpLoggingInterceptor.Level.NONE;

public class AppController extends MultiDexApplication {
	/**
     * Log or request TAG
     */
    public static final String TAG = AppController.class.getSimpleName();

    static
    {
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
    }

    /**
     * A singleton instance of the application class for easy access in other places
     */
    private static AppController mInstance;
    private ImageLoaderConfiguration imgConfig;
    private ImageLoader imageLoader;
    private OkHttpClient okClient;
    private FirebaseAnalytics mFirebaseAnalytics;
    private DisplayImageOptions defaultOptions;
    private static Context context;

    public static Context getContext() {
        return context;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Fabric.with(this, new Crashlytics());

        // initialize the singleton
        mInstance = this;
        context = getApplicationContext();

        // init analystics
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
        MiscUtil.init(this);
        DS.initialize(this.getApplicationContext());

        okClient = new OkHttpClient.Builder()
                .readTimeout(15, TimeUnit.SECONDS)
                .connectTimeout(15, TimeUnit.SECONDS)
                .addInterceptor(LoggingInterceptor)
                .addInterceptor( provideHttpLoggingInterceptor() )
                .addInterceptor( new EnhancedCacheInterceptor() )
                .cache(provideCache())
                .build();

        defaultOptions = new DisplayImageOptions.Builder()
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .imageScaleType(ImageScaleType.NONE)
                .bitmapConfig(Bitmap.Config.ARGB_8888)
                .build();
        File cacheDir = StorageUtils.getCacheDirectory(this);

        imgConfig = new ImageLoaderConfiguration.Builder(this)
                .defaultDisplayImageOptions(defaultOptions)
                .threadPoolSize(1)
                .diskCache(new UnlimitedDiskCache(cacheDir))
                .diskCacheExtraOptions(480, 320, null)
                .imageDownloader(new OkHttpImageDownloader(this,getOkClient()))
                .writeDebugLogs()
                .build();

        imageLoader = ImageLoader.getInstance();
        imageLoader.init(imgConfig);

        LogConfiguration config = new LogConfiguration.Builder()
                .tag("MY_TAG")                                         // Specify TAG, default: "X-LOG"
                .t()                                                   // Enable thread info, disabled by default
                .st(2)                                                 // Enable stack trace info with depth 2, disabled by default
                .b()                                                   // Enable border, disabled by default
                .jsonFormatter(new DefaultJsonFormatter())                  // Default: DefaultJsonFormatter
                .xmlFormatter(new DefaultXmlFormatter())                    // Default: DefaultXmlFormatter
                .throwableFormatter(new DefaultThrowableFormatter())        // Default: DefaultThrowableFormatter
                .threadFormatter(new DefaultThreadFormatter())              // Default: DefaultThreadFormatter
                .stackTraceFormatter(new DefaultStackTraceFormatter())      // Default: DefaultStackTraceFormatter
                .borderFormatter(new DefaultBorderFormatter())               // Default: DefaultBorderFormatter
                .build();

        Printer androidPrinter = new AndroidPrinter();             // Printer that print the log using android.util.Log
        Printer systemPrinter = new SystemPrinter();               // Printer that print the log using System.out.println

        XLog.init(LogLevel.ALL,                                    // Specify the log level, logs below this level won't be printed
                config,                                                // Specify the log configuration, if not specified, will use new LogConfiguration.Builder().build()
                androidPrinter,                                        // Specify printers, if no printer is specified, AndroidPrinter will be used by default
                systemPrinter);

        SharedPrefManager.init(this,true,"");

        FirebaseRemoteConfigSettings configSettings = new FirebaseRemoteConfigSettings.Builder()
                .setDeveloperModeEnabled(BuildConfig.DEBUG)
                .build();
        FirebaseRemoteConfig.getInstance().setConfigSettings(configSettings);
    }

    /**
     * @return AppController singleton instance
     */
    public static synchronized AppController getInstance() {
        return mInstance;
    }


    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

    public ImageLoader getImageLoader() {
        return imageLoader;
    }

    public OkHttpClient getOkClient() {
        return okClient;
    }

    private static final Interceptor LoggingInterceptor = new Interceptor() {
        @Override
        public Response intercept(Interceptor.Chain chain) throws IOException {
            Request request = chain.request();
            long t1 = System.nanoTime();
            XLog.i(String.format("Sending request %s on %s%n%s", request.url(), chain.connection(), request.headers()));
            Response response = chain.proceed(request);
            long t2 = System.nanoTime();
            XLog.i(String.format("Received response for %s in %.1fms%n%s", response.request().url(), (t2 - t1) / 1e6d, response.headers()));
            return response;
        }
    };

    private static Cache provideCache ()
    {
        Cache cache = null;
        try
        {
            cache = new Cache( new File( AppController.getInstance().getCacheDir(), "http-cache" ),
                    10 * 1024 * 1024 ); // 10 MB
        }
        catch (Exception e)
        {
            XLog.e(  "Could not create Cache!" );
        }
        return cache;
    }

    private static HttpLoggingInterceptor provideHttpLoggingInterceptor ()
    {
        HttpLoggingInterceptor httpLoggingInterceptor =
                new HttpLoggingInterceptor( new HttpLoggingInterceptor.Logger()
                {
                    @Override
                    public void log (String message)
                    {
                        XLog.e( message );
                    }
                } );
        httpLoggingInterceptor.setLevel( BuildConfig.DEBUG ? BODY : NONE );
        return httpLoggingInterceptor;
    }

    public FirebaseAnalytics getFirebaseAnalytics() {
        return mFirebaseAnalytics;
    }

    public DisplayImageOptions getDefaultOptions() {
        return defaultOptions;
    }
}
