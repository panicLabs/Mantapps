package com.mantapps.reader.app;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.ShareActionProvider;

import com.mantapps.reader.fragment.PostFragment;
import com.mantapps.reader.R;

public class PostActivity extends ActionBarActivity implements PostFragment.OnPostSelectedListener {
    private ShareActionProvider mShareActionProvider;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

    }

//
//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        getMenuInflater().inflate(R.menu.menu_comment, menu);
//        MenuItem menuItem = menu.findItem(R.id.menu_item_share);
//        mShareActionProvider = (ShareActionProvider) MenuItemCompat.getActionProvider(menuItem);
//        mShareActionProvider.setShareIntent(getDefaultIntent());
//        return true;
//    }
//
//    // Somewhere in the application.
//    public void doShare(Intent shareIntent) {
//        // When you want to share set the share intent.
//        mShareActionProvider.setShareIntent(shareIntent);
//    }
//
//    private Intent getDefaultIntent() {
//        Intent intent = new Intent(Intent.ACTION_SEND);
//        intent.setType("text/plain");
//        intent.putExtra(Intent.EXTRA_TEXT, "http://www.baidu.com");
//        return intent;
//    }
//
//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        // Handle action bar item clicks here. The action bar will
//        // automatically handle clicks on the Home/Up button, so long
//        // as you specify a parent activity in AndroidManifest.xml.
//        int id = item.getItemId();
//
//
//        return super.onOptionsItemSelected(item);
//    }

    @Override
    public void onPostSelected(int id) {

    }
}
