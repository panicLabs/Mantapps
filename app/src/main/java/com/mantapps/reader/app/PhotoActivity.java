package com.mantapps.reader.app;


import android.annotation.TargetApi;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v4.view.ViewCompat;
import android.support.v7.app.AppCompatActivity;
import android.transition.ChangeBounds;
import android.transition.ChangeClipBounds;
import android.transition.ChangeImageTransform;
import android.transition.TransitionSet;
import android.util.Log;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.Window;
import android.view.animation.DecelerateInterpolator;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.mantapps.reader.R;
import com.mantapps.reader.glide.GlideUtils;
import com.paniclabs.libs.touchimageview.ImageViewTouch;

/**
 * Created by paniclabs on 11/16/16.
 */

public class PhotoActivity extends AppCompatActivity {

    public static final String SHARED_ELEMENT_KEY = "SHARED_ELEMENT_KEY";
    public static final String IMAGE_RES_ID = "imageResId";
    private ImageViewTouch photoView;
    private ProgressBar progressBar;

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_photo);
        photoView = (ImageViewTouch) findViewById(R.id.iv_photo);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        final TextView txtPhoto = (TextView) findViewById(R.id.txt_photo);

        initImageEnterTransition();
        Uri uri = getIntent().getData();
        ImageView mClose = (ImageView) findViewById(R.id.close);

        mClose.setOnClickListener(new View.OnClickListener() {
            @TargetApi(21)
            @Override
            public void onClick(View view) {
                onBackPressed();
                finishAfterTransition();
            }
        });

        String imageName = uri.toString().substring(uri.toString().lastIndexOf( "/" )+1, uri.toString().length()) ;
        txtPhoto.setText(imageName);

    }

    private void initImageEnterTransition() {
        String imageTransitionName = getIntent().getStringExtra(SHARED_ELEMENT_KEY);
        ViewCompat.setTransitionName(photoView, imageTransitionName);

        String resId = getIntent().getExtras().getString(IMAGE_RES_ID);
        GlideUtils imageLoader = new GlideUtils(this);
        imageLoader.setImageUrl(resId);
        imageLoader.setImageView(photoView);
        imageLoader.setSize(Target.SIZE_ORIGINAL, Target.SIZE_ORIGINAL);
        imageLoader.setProgressBar(progressBar);
        imageLoader.load();
    }

}
