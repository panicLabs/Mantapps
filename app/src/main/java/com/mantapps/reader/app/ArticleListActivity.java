package com.mantapps.reader.app;

import android.app.ActivityOptions;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.transition.ChangeImageTransform;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.elvishew.xlog.XLog;
import com.mantapps.reader.R;
import com.mantapps.reader.adaptor.PostAdaptor;
import com.mantapps.reader.api.MantappsApi;
import com.mantapps.reader.model.Author;
import com.mantapps.reader.model.Post;
import com.mantapps.reader.util.Config;
import com.mantapps.reader.util.JSONParser;
import com.mantapps.reader.util.RestUtil;
import com.mantapps.reader.view.LoadingView;
import com.mantapps.reader.view.RefreshLayout;
import com.mtburn.android.sdk.AppDavis;
import com.mtburn.android.sdk.instream.ADVSInstreamAdPlacer;
import com.mtburn.android.sdk.instream.ADVSInstreamAdPlacerListener;
import com.mtburn.android.sdk.instream.InstreamAdViewBinderImpl;
import com.mtburn.android.sdk.model.ADVSInstreamInfoModel;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ArticleListActivity extends SwipeBaseActivity implements AdapterView.OnItemClickListener,
        RefreshLayout.OnRefreshListener, RefreshLayout.OnLoadListener, ADVSInstreamAdPlacerListener {

    private static final String TAG = ArticleListActivity.class.getName();


    Author author = null;

    private OnFragmentInteractionListener mInteractionListener;
    public static final String AUTHOR = "author";

    private RefreshLayout post_list_swipe_refresh_layout;
    private ListView listView;
    private LoadingView loadingView;
    private PostAdaptor postAdaptor;

    // Page number
    private int mPage = 1;
    // List of all posts in the ListView
    private ArrayList<Post> postList = new ArrayList<>();
    private ArrayList<Object> mList = new ArrayList<>();
    private ADVSInstreamAdPlacer adPlacer;

    public ArticleListActivity() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.category_list);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        initUI();
        initData();
        setupSdk();
        postAdaptor = new PostAdaptor(this, mList, adPlacer);

        listView.setAdapter(postAdaptor);
        listView.setOnItemClickListener(this);

        // Pull to refresh listener
        post_list_swipe_refresh_layout.setOnRefreshListener(this);
        post_list_swipe_refresh_layout.setOnLoadListener(this);
    }

    private void setupSdk() {
        // (3) Initialize AppDavis SDK
        AppDavis.init(this, getString(R.string.media_id));
        // (4) Generate ADVSInstreamAdPlacer
        adPlacer = AppDavis.createInstreamAdPlacer(this,
                getString(R.string.spot_id));
        // (5) Allocate the advertising project information to your chosen
        // View (refer to the following parameter items used to display
        // custom in-feed advertisements)

        InstreamAdViewBinderImpl adViewBinder;
        adViewBinder= new InstreamAdViewBinderImpl(this){
            @Override
            public View createView(ViewGroup parent, int layoutId)
            {
                View view = LayoutInflater.from(getApplicationContext())
                        .inflate(R.layout.custom_instream_ad, parent, false);
                PostAdaptor.AdViewHolder holder = new PostAdaptor.AdViewHolder(view);
                view.setTag(holder);
                return view;
            }
            @Override
            public void bindAdData(View v, ADVSInstreamInfoModel adData) {
                PostAdaptor.AdViewHolder holder = (PostAdaptor.AdViewHolder)v.getTag();
                holder.setData(adData);
                loadAdImage(adData, holder.adImage, null);
                //loadAdIconImage(adData, holder.iconImage, null);
            }
        };

        adPlacer.registerAdViewBinder(adViewBinder);

        adPlacer.setAdListener(this);

        // (6) Start loading the in-feed advertisement
    }

    private void initData(){
        try {
            author = (Author) getIntent().getExtras().getSerializable(AUTHOR);
        }catch (Exception e) {
            Log.e("CATEGORY", "category null");
        }
        if(author != null){
            getSupportActionBar().setTitle(author.getFullName());
            onRefresh();
        }
    }

    private void initUI(){

        post_list_swipe_refresh_layout = (RefreshLayout) findViewById(R.id.post_list_swipe_refresh_layout);
        //swipeRefreshLayout.setColorSchemeColors(android.R.color.holo_blue_dark, android.R.color.holo_blue_light, android.R.color.holo_green_light, android.R.color.holo_green_light);
        post_list_swipe_refresh_layout.setColorScheme(new int[]{android.R.color.holo_blue_bright, android.R.color.holo_green_light, android.R.color.holo_orange_light, android.R.color.holo_red_light});
        listView = (ListView) findViewById(R.id.list);
        loadingView = (LoadingView) findViewById(R.id.loginLoadingView);
        loadingView.setLoading(true);
        initIkl();
//        loadPosts();
    }

    private void initIkl() {
//        EasyAds.forBanner(ArticleListActivity.this)
//                .with((AdView) findViewById(R.id.adView))
//                .testDevice("F8BB3D3AA0809185FF7B45D54C4132FB")
//                .show();
    }

    @Override
    public void onStart() {
        super.onStart();
    }


    @Override
    public void onResume() {
        super.onResume();
    }


    /**
     * Load posts from a specific page number
     *
     * @param page Page number
     * @param showProgressDialog flag to determine whether to show a ProgressDialog
     */
    private void loadPosts(final int page, final boolean showProgressDialog) {
        Log.d(TAG, "----------------- Loading All Search News, page:" + String.valueOf(page));

        if(author == null){
            return;
        }

        // Construct the proper API Url
        String url = Config.AUTHOR_POST + author.getAuthorId() + "&page=" + String.valueOf(page);
        Log.d(TAG, url);
        // Request post JSON
        RestUtil.getInstance().create(MantappsApi.class).getAuthotPost(url).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if(response.isSuccessful()){
                    try {
                        JSONObject jsonObject = new JSONObject(response.body().string());

                        loadingView.setLoading(false);
                        post_list_swipe_refresh_layout.setLoading(false);    // Stop when done
                        post_list_swipe_refresh_layout.setRefreshing(false); // Stop when done
                        if (page == 1) {
                            mList.clear();
                            adPlacer.loadAd();
                        }
                        // Parse JSON data
                        mList.addAll(JSONParser.parsePosts(jsonObject));

                        postAdaptor.setCapacity(-1);
                        postAdaptor.notifyDataSetChanged(); // Display the list

                        // Set ListView position
                        if (ArticleListActivity.this.mPage != 1) {
                            // Move the article list up by one row
                            listView.setSelection(listView.getFirstVisiblePosition() + 1);
                        }
                        ArticleListActivity.this.mPage++;
                    } catch (JSONException | IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                loadingView.setLoading(false);
                post_list_swipe_refresh_layout.setRefreshing(false);
                t.printStackTrace();
                Toast.makeText(ArticleListActivity.this, "Network error. Please try again later...",
                        Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Post p = ((Post) mList.get(position));

        Bundle args = new Bundle();
        args.putInt("id", p.getId());
        args.putString("title",p.getTitle());
        args.putString("slug", p.getSlug());
        args.putString("date", p.getDate());
        args.putString("author", p.getAuthor());
        args.putString("content", p.getContent());
        args.putString("url", p.getUrl());
        args.putString("thumbnailUrl", p.getMediumImageUrl());
        Intent intent = new Intent(this, DetailActivity.class);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            String shareElementName = "sharedImageView";
            ActivityOptions activityOptions = ActivityOptions.makeSceneTransitionAnimation(this, view, shareElementName);
            getWindow().setSharedElementEnterTransition(new ChangeImageTransform(this, null));
            intent.putExtra(DetailActivity.SHARED_ELEMENT_KEY, shareElementName);
            intent.putExtras(args);
            intent.putExtra(DetailActivity.IMAGE_RES_ID, p.getMediumImageUrl());
            startActivity(intent, activityOptions.toBundle());
        }else {
            intent.putExtras(args);
            intent.putExtra(DetailActivity.IMAGE_RES_ID, p.getMediumImageUrl());
            startActivity(intent);
        }
    }

    @Override
    public void onRefresh() {
        mPage = 1; // Refresh only the first page
        // Clear the list
        loadPosts(mPage, false);
    }

    @Override
    public void onLoad() {
        loadPosts(mPage, false);
    }

    String MTBURN="MTBURN";
    @Override
    public void onAdsLoaded(List<? extends ADVSInstreamInfoModel>items) {
        Log.d(MTBURN, "onAdsLoaded: ");

        int i = 1;
        for (ADVSInstreamInfoModel adData : items) {
            XLog.w("adsData content = " + adData.content() +
                    "\n adsData url = " + adData.creative_url() +
                    "\n adsData Position = " + adData.position() +
                    "\n adsData size = " + items.size());
            try{
                if(items.size() > 0){
                    postAdaptor.dataSourceList.add(i, adData);
                }
                //postAdaptor.dataSourceList.add(i, adData);
                i += 3;
            }catch (Exception e){
                e.printStackTrace();
            }
        }
        postAdaptor.notifyDataSetChanged();
    }
    @Override
    public void onAdMainImageLoaded(String imageUrl) {
        Log.d(MTBURN,"onAdMainImageLoaded:imageUrl=" + imageUrl);
    }
    @Override
    public void onAdIconImageLoaded(String imageUrl) {
        Log.d(MTBURN, "onAdIconImageLoaded:imageUrl=" + imageUrl);
    }
    @Override
    public void onAdsLoadedFail(String errorString) {
        Log.d(MTBURN, "onAdsLoadedFail:error=" + errorString);
    }
    @Override
    public void onAdImageLoadedFail(String imageUrl, String errorString) {
        Log.d(MTBURN, "onAdImageLoadedFail:error" + errorString);
    }
    @Override
    public void onAdClicked(String redirectURL) {
        Log.d(MTBURN, "onAdClicked:redirectURL" + redirectURL);
    }


    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        public void onFragmentInteraction(Uri uri);
    }

}
