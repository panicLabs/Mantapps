package com.mantapps.reader.app;

import android.app.Activity;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputType;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.crashlytics.android.answers.Answers;
import com.crashlytics.android.answers.ContentViewEvent;
import com.crashlytics.android.answers.SearchEvent;
import com.elvishew.xlog.XLog;
import com.google.android.gms.analytics.Tracker;
import com.mantapps.reader.R;
import com.mantapps.reader.adaptor.PostAdaptor;
import com.mantapps.reader.api.MantappsApi;
import com.mantapps.reader.model.Post;
import com.mantapps.reader.util.Config;
import com.mantapps.reader.util.InputTools;
import com.mantapps.reader.util.JSONParser;
import com.mantapps.reader.util.RestUtil;
import com.mantapps.reader.util.StringUtils;
import com.mantapps.reader.view.LoadingView;
import com.mantapps.reader.view.RefreshLayout;
import com.mtburn.android.sdk.AppDavis;
import com.mtburn.android.sdk.instream.ADVSInstreamAdPlacer;
import com.mtburn.android.sdk.instream.ADVSInstreamAdPlacerListener;
import com.mtburn.android.sdk.instream.InstreamAdViewBinderImpl;
import com.mtburn.android.sdk.model.ADVSInstreamInfoModel;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SearchResultActivity extends Activity implements AdapterView.OnItemClickListener,
        RefreshLayout.OnRefreshListener, RefreshLayout.OnLoadListener, ADVSInstreamAdPlacerListener {

    private static final String TAG = SearchResultActivity.class.getName();

    EditText id_search_edit;
    ImageView id_iv_back;
    LinearLayout id_ll_top;

    private Drawable mIconClean;
    private InputMethodManager imm;

    private OnFragmentInteractionListener mInteractionListener;
    private String searchText = "";

    private RefreshLayout swipeRefreshLayout;
    private ListView listView;
    private LoadingView loadingView;
    private PostAdaptor postAdaptor;

    // Page number
    private int mPage = 1;
    // List of all posts in the ListView
    private ArrayList<Post> postList = new ArrayList<>();
    private ArrayList<Object> mList = new ArrayList<>();
    private ADVSInstreamAdPlacer adPlacer;
    private Tracker mTracker;

    public SearchResultActivity() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.search_result);

        initUI();
        initSdk();
        initEvent();

    }

    @Override
    protected void onResume() {
        super.onResume();

    }

    private void initSdk() {
        // (3) Initialize AppDavis SDK
        AppDavis.init(this, getString(R.string.media_id));
        // (4) Generate ADVSInstreamAdPlacer
        adPlacer = AppDavis.createInstreamAdPlacer(this,
                getString(R.string.spot_id));
        // (5) Allocate the advertising project information to your chosen
        // View (refer to the following parameter items used to display
        // custom in-feed advertisements)

        InstreamAdViewBinderImpl adViewBinder;
        adViewBinder= new InstreamAdViewBinderImpl(this){
            @Override
            public View createView(ViewGroup parent, int layoutId)
            {
                View view = LayoutInflater.from(getApplicationContext())
                        .inflate(R.layout.custom_instream_ad, parent, false);
                PostAdaptor.AdViewHolder holder = new PostAdaptor.AdViewHolder(view);
                view.setTag(holder);
                return view;
            }
            @Override
            public void bindAdData(View v, ADVSInstreamInfoModel adData) {
                PostAdaptor.AdViewHolder holder = (PostAdaptor.AdViewHolder)v.getTag();
                holder.setData(adData);
                loadAdImage(adData, holder.adImage, null);
               // loadAdIconImage(adData, holder.iconImage, null);
            }
        };

        adPlacer.registerAdViewBinder(adViewBinder);

        adPlacer.setAdListener(this);
    }

    private void initUI(){

        imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
        mIconClean = getResources().getDrawable(R.drawable.selector_clean);
        id_search_edit = (EditText) findViewById(R.id.id_search_edit);
        id_iv_back = (ImageView) findViewById(R.id.id_iv_back);
        id_ll_top = (LinearLayout) findViewById(R.id.id_ll_top);

        swipeRefreshLayout = (RefreshLayout) findViewById(R.id.swipe_refresh_layout1);
        //swipeRefreshLayout.setColorSchemeColors(android.R.color.holo_blue_dark, android.R.color.holo_blue_light, android.R.color.holo_green_light, android.R.color.holo_green_light);
        swipeRefreshLayout.setColorScheme(new int[]{android.R.color.holo_blue_bright, android.R.color.holo_green_light, android.R.color.holo_orange_light, android.R.color.holo_red_light});
        listView = (ListView) findViewById(R.id.list);
        loadingView = (LoadingView) findViewById(R.id.loginLoadingView);
        loadingView.setLoading(true);

    }

    private void initEvent(){

        id_search_edit.clearFocus();
        id_search_edit.setFocusable(true);
        id_search_edit.requestFocus();
        InputTools.ShowKeyboard(id_search_edit);

        id_search_edit.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    searchText = v.getText().toString();
                    mPage = 1;
                    onLoad();
                    return true;
                }
                return false;
            }
        });
        id_iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

//        id_search_edit.addTextChangedListener(new TextChangeListener(id_search_edit));
        id_search_edit.setOnTouchListener(textTouchLisener);

    }

    @Override
    public void onStart() {
        super.onStart();

        // Custom list adaptor for Post object
        postAdaptor = new PostAdaptor(this, mList, adPlacer);

        listView.setAdapter(postAdaptor);
        listView.setOnItemClickListener(this);

        // Pull to refresh listener
        swipeRefreshLayout.setOnRefreshListener(this);
        swipeRefreshLayout.setOnLoadListener(this);
    }

    /**
     * Load posts
     *
     */
    public void loadPosts(String searchText){
        Answers.getInstance().logSearch(new SearchEvent()
                .putQuery(searchText));
        loadPosts(mPage, true, searchText);
    }

    /**
     * Load posts from a specific page number
     *
     * @param page Page number
     * @param showProgressDialog flag to determine whether to show a ProgressDialog
     */
    private void loadPosts(int page, final boolean showProgressDialog, String searchText) {
        Log.d(TAG, "----------------- Loading All Search News, page:" + String.valueOf(page));

        if(page == 1){
            mList.clear();
            postAdaptor.notifyDataSetChanged();
            loadingView.setLoading(true);
            swipeRefreshLayout.setRefreshing(true);
            swipeRefreshLayout.setLoading(true);
            adPlacer.loadAd();
        }

        // Construct the proper API Url
        String url = Config.SEARCH + searchText +"&page=" + String.valueOf(page);

        if(StringUtils.isEmpty(searchText)) {
            //postAdaptor.setDatas(new ArrayList<Post>());
            loadingView.setVisibility(View.GONE);
            return;
        }else {
            loadingView.setVisibility(View.VISIBLE);
        }

        Log.d(TAG, url);
        // Request post JSON
        RestUtil.getInstance().create(MantappsApi.class).getSearch(url).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if(response.isSuccessful()){
                    try {
                        JSONObject jsonObject = new JSONObject(response.body().string());
                        XLog.w("hasil " + jsonObject.toString());

                        loadingView.setLoading(false);
                        swipeRefreshLayout.setRefreshing(false); // Stop when done
                        swipeRefreshLayout.setLoading(false);
                        // Parse JSON data
                        mList.addAll(JSONParser.parsePosts(jsonObject));
                        postAdaptor.notifyDataSetChanged(); // Display the list
                        // (6) Start loading the in-feed advertisement


                        // Set ListView position
                        if (SearchResultActivity.this.mPage != 1) {
                            // Move the article list up by one row
                            listView.setSelection(listView.getFirstVisiblePosition() + 1);
                        }
                        // Prepare for the next page
                        SearchResultActivity.this.mPage++;
                    } catch (JSONException | IOException e) {
                        e.printStackTrace();
                    }

                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                loadingView.setLoading(false);
                swipeRefreshLayout.setRefreshing(false);
                swipeRefreshLayout.setLoading(false);
                t.printStackTrace();
                Toast.makeText(SearchResultActivity.this, "Network error. Please try again later...",
                        Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Post p = (Post) mList.get(position);

        Bundle args = new Bundle();
        args.putInt("id", p.getId());
        args.putString("title",p.getTitle());
        args.putString("slug", p.getSlug());
        args.putString("date", p.getDate());
        args.putString("author", p.getAuthor());
        args.putString("content", p.getContent());
        args.putString("url", p.getUrl());
        args.putString("thumbnailUrl", p.getMediumImageUrl());
        //pf.setArguments(args);
        Intent intent = new Intent(this, DetailActivity.class);
        intent.putExtras(args);
        startActivity(intent);

    }

    @Override
    public void onRefresh() {
        mPage = 0; // Refresh only the first page
        // Clear the list
        mList.clear();
        postAdaptor.notifyDataSetChanged();
        loadPosts(mPage, false, searchText);
    }

    //@Override
    public void onScrollStateChanged(AbsListView view, int scrollState) {

    }

    //@Override
    public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
        // Automatically load new posts if end of the list is reached
        if (visibleItemCount != 0 && totalItemCount > visibleItemCount && (firstVisibleItem + visibleItemCount) == totalItemCount) {
            //loading = true;
            loadPosts(searchText);
        }
    }

    @Override
    public void onLoad() {
        loadPosts(searchText);
    }

    String MTBURN="MTBURN";
    @Override
    public void onAdsLoaded(List<? extends ADVSInstreamInfoModel>items) {
        Log.d(MTBURN, "onAdsLoaded: ");

        int i = 1;
        for (ADVSInstreamInfoModel adData : items) {
            XLog.w("adsData content = " + adData.content() +
                    "\n adsData url = " + adData.creative_url() +
                    "\n adsData Position = " + adData.position() +
                    "\n adsData size = " + items.size());
            try{
                if(items.size() > 0){
                    postAdaptor.dataSourceList.add(i, adData);
                }
                //postAdaptor.dataSourceList.add(i, adData);
                i += 3;
            }catch (Exception e){
                e.printStackTrace();
            }
        }
        postAdaptor.notifyDataSetChanged();
    }
    @Override
    public void onAdMainImageLoaded(String imageUrl) {
        Log.d(MTBURN,"onAdMainImageLoaded:imageUrl=" + imageUrl);
    }
    @Override
    public void onAdIconImageLoaded(String imageUrl) {
        Log.d(MTBURN, "onAdIconImageLoaded:imageUrl=" + imageUrl);
    }
    @Override
    public void onAdsLoadedFail(String errorString) {
        Log.d(MTBURN, "onAdsLoadedFail:error=" + errorString);
    }
    @Override
    public void onAdImageLoadedFail(String imageUrl, String errorString) {
        Log.d(MTBURN, "onAdImageLoadedFail:error" + errorString);
    }
    @Override
    public void onAdClicked(String redirectURL) {
        Log.d(MTBURN, "onAdClicked:redirectURL" + redirectURL);
    }

    // Interface used to exchange data with MainActivity
    public static interface OnNewsSelectedListener {
        void onNewsSelected(HashMap<String, String> map);
    }

    public void onFragmentInteraction(Uri uri){

    }

    private View.OnTouchListener textTouchLisener = new View.OnTouchListener() {
        @Override
        public boolean onTouch(View v, MotionEvent event) {
            switch (event.getAction()) {
                case MotionEvent.ACTION_UP:
                    EditText text = ((EditText)v);
                    int curX = (int) event.getX();
                    // curX为触屏时的x位置，v.getWidth()为textview的宽度，通过这两个值锁定有单击事件的触屏区域
                    // TextUtils.isEmpty(tv.getText())判断textview的值是否为空
                    Drawable deleteDrawable = text.getCompoundDrawables()[2];
                    int paddingRight = text.getCompoundPaddingRight();
                    int drawableWidth = deleteDrawable == null ? 0 : deleteDrawable.getIntrinsicWidth();
                    if (curX > v.getWidth() - drawableWidth - paddingRight
                            && !TextUtils.isEmpty(text.getText())) {
                        text.setText("");
                        // 下面这四句不设置的话，会弹出“选择输入法”的下拉列表
                        int cacheInputType = text.getInputType();
                        text.setInputType(InputType.TYPE_NULL);
                        text.onTouchEvent(event);
                        text.setInputType(cacheInputType);
                        return true;
                    }
                    break;
            }
            return false;
        }
    };

    private class TextChangeListener implements TextWatcher {
        private EditText text;
        private boolean isNull = true;
        public TextChangeListener(EditText text) {
            this.text = text;
        }

        @Override
        public void onTextChanged(CharSequence arg0, int arg1, int arg2,
                                  int arg3) {
        }

        @Override
        public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
                                      int arg3) {
        }

        @Override
        public void afterTextChanged(Editable s) {
            if (!TextUtils.isEmpty(s)) {
                if (isNull) {
                    text.setCompoundDrawablesWithIntrinsicBounds(null,
                            null, mIconClean, null);
                    isNull = false;
                }
            }else{
                text.setCompoundDrawablesWithIntrinsicBounds(null,
                        null, null, null);
                isNull = true;
            }
            searchText = s.toString();
            mPage = 1;
			loadingView.setLoading(true);
            onLoad();
        }
    };

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        public void onFragmentInteraction(Uri uri);
    }

}
