package com.mantapps.reader.model.recentpost;

import java.util.List;
import com.google.gson.annotations.Expose;
import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("com.robohorse.robopojogenerator")
public class CustomFields{

	@SerializedName("mashsb_timestamp")
	@Expose
	private List<String> mashsbTimestamp;

	@SerializedName("mashsb_jsonshares")
	@Expose
	private List<String> mashsbJsonshares;

	@SerializedName("mashsb_shares")
	@Expose
	private List<String> mashsbShares;

	@SerializedName("mashsb_twitter_handle")
	@Expose
	private List<String> mashsbTwitterHandle;

	public void setMashsbTimestamp(List<String> mashsbTimestamp){
		this.mashsbTimestamp = mashsbTimestamp;
	}

	public List<String> getMashsbTimestamp(){
		return mashsbTimestamp;
	}

	public void setMashsbJsonshares(List<String> mashsbJsonshares){
		this.mashsbJsonshares = mashsbJsonshares;
	}

	public List<String> getMashsbJsonshares(){
		return mashsbJsonshares;
	}

	public void setMashsbShares(List<String> mashsbShares){
		this.mashsbShares = mashsbShares;
	}

	public List<String> getMashsbShares(){
		return mashsbShares;
	}

	public void setMashsbTwitterHandle(List<String> mashsbTwitterHandle){
		this.mashsbTwitterHandle = mashsbTwitterHandle;
	}

	public List<String> getMashsbTwitterHandle(){
		return mashsbTwitterHandle;
	}
}