package com.mantapps.reader.model.recentpost;

import com.google.gson.annotations.Expose;
import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("com.robohorse.robopojogenerator")
public class TagsItem{

	@SerializedName("description")
	@Expose
	private String description;

	@SerializedName("id")
	@Expose
	private int id;

	@SerializedName("post_count")
	@Expose
	private int postCount;

	@SerializedName("title")
	@Expose
	private String title;

	@SerializedName("slug")
	@Expose
	private String slug;

	public void setDescription(String description){
		this.description = description;
	}

	public String getDescription(){
		return description;
	}

	public void setId(int id){
		this.id = id;
	}

	public int getId(){
		return id;
	}

	public void setPostCount(int postCount){
		this.postCount = postCount;
	}

	public int getPostCount(){
		return postCount;
	}

	public void setTitle(String title){
		this.title = title;
	}

	public String getTitle(){
		return title;
	}

	public void setSlug(String slug){
		this.slug = slug;
	}

	public String getSlug(){
		return slug;
	}
}