package com.mantapps.reader.model.slug;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import javax.annotation.Generated;

@Generated("com.robohorse.robopojogenerator")
public class AttachmentsItem{
	@SerializedName("parent")
	@Expose
	private int parent;
	@SerializedName("images")
	@Expose
	private Images images;
	@SerializedName("mime_type")
	@Expose
	private String mime_type;
	@SerializedName("description")
	@Expose
	private String description;
	@SerializedName("caption")
	@Expose
	private String caption;
	@SerializedName("id")
	@Expose
	private int id;
	@SerializedName("title")
	@Expose
	private String title;
	@SerializedName("url")
	@Expose
	private String url;
	@SerializedName("slug")
	@Expose
	private String slug;

	public void setParent(int parent){
		this.parent = parent;
	}

	public int getParent(){
		return parent;
	}

	public void setImages(Images images){
		this.images = images;
	}

	public Images getImages(){
		return images;
	}

	public void setMime_type(String mime_type){
		this.mime_type = mime_type;
	}

	public String getMime_type(){
		return mime_type;
	}

	public void setDescription(String description){
		this.description = description;
	}

	public String getDescription(){
		return description;
	}

	public void setCaption(String caption){
		this.caption = caption;
	}

	public String getCaption(){
		return caption;
	}

	public void setId(int id){
		this.id = id;
	}

	public int getId(){
		return id;
	}

	public void setTitle(String title){
		this.title = title;
	}

	public String getTitle(){
		return title;
	}

	public void setUrl(String url){
		this.url = url;
	}

	public String getUrl(){
		return url;
	}

	public void setSlug(String slug){
		this.slug = slug;
	}

	public String getSlug(){
		return slug;
	}

}