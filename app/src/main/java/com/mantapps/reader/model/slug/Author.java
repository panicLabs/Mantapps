package com.mantapps.reader.model.slug;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import javax.annotation.Generated;

@Generated("com.robohorse.robopojogenerator")
public class Author{
	@SerializedName("name")
	@Expose
	private String name;
	@SerializedName("nickname")
	@Expose
	private String nickname;
	@SerializedName("last_name")
	@Expose
	private String last_name;
	@SerializedName("description")
	@Expose
	private String description;
	@SerializedName("id")
	@Expose
	private int id;
	@SerializedName("first_name")
	@Expose
	private String first_name;
	@SerializedName("slug")
	@Expose
	private String slug;
	@SerializedName("url")
	@Expose
	private String url;

	public void setName(String name){
		this.name = name;
	}

	public String getName(){
		return name;
	}

	public void setNickname(String nickname){
		this.nickname = nickname;
	}

	public String getNickname(){
		return nickname;
	}

	public void setLast_name(String last_name){
		this.last_name = last_name;
	}

	public String getLast_name(){
		return last_name;
	}

	public void setDescription(String description){
		this.description = description;
	}

	public String getDescription(){
		return description;
	}

	public void setId(int id){
		this.id = id;
	}

	public int getId(){
		return id;
	}

	public void setFirst_name(String first_name){
		this.first_name = first_name;
	}

	public String getFirst_name(){
		return first_name;
	}

	public void setSlug(String slug){
		this.slug = slug;
	}

	public String getSlug(){
		return slug;
	}

	public void setUrl(String url){
		this.url = url;
	}

	public String getUrl(){
		return url;
	}

}