package com.mantapps.reader.model.slug;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import javax.annotation.Generated;

@Generated("com.robohorse.robopojogenerator")
public class SlugResponse{
	@SerializedName("post")
	@Expose
	private Post post;
	@SerializedName("previous_url")
	@Expose
	private String previous_url;
	@SerializedName("status")
	@Expose
	private String status;

	public void setPost(Post post){
		this.post = post;
	}

	public Post getPost(){
		return post;
	}

	public void setPrevious_url(String previous_url){
		this.previous_url = previous_url;
	}

	public String getPrevious_url(){
		return previous_url;
	}

	public void setStatus(String status){
		this.status = status;
	}

	public String getStatus(){
		return status;
	}

}