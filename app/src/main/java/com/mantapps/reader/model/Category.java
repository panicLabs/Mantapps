package com.mantapps.reader.model;

import java.io.Serializable;

/**
 * Created by Declan on 7/07/15.
 */
public class Category implements Serializable {

    private static final long serialVersionUID = 1L;

    private String name;
    private int id;
    private int parent;
    private String imgUrl;

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }

    public int getParent() {
        return parent;
    }
    public void setParent(int parent) {
        this.parent = parent;
    }

    public void setImageUrl(String url) {
        this.imgUrl = url;
    }


    public String getImgUrl() {
        return imgUrl;
    }

    @Override
    public String toString() {
        return "Category{" +
                "name='" + name + '\'' +
                ", id=" + id +
                ", parent=" + parent +
                ", imgUrl='" + imgUrl + '\'' +
                '}';
    }
}
