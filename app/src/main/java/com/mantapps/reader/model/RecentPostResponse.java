package com.mantapps.reader.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import javax.annotation.Generated;

@Generated("com.robohorse.robopojogenerator")
public class RecentPostResponse {

	@SerializedName("count_total")
	@Expose
	private int countTotal;

	@SerializedName("pages")
	@Expose
	private int pages;


	@SerializedName("count")
	@Expose
	private int count;

	@SerializedName("posts")
	@Expose
	private List<Post> posts;

	@SerializedName("status")
	@Expose
	private String status;

	public void setCountTotal(int countTotal){
		this.countTotal = countTotal;
	}

	public int getCountTotal(){
		return countTotal;
	}

	public void setPages(int pages){
		this.pages = pages;
	}

	public int getPages(){
		return pages;
	}

	public void setCount(int count){
		this.count = count;
	}

	public int getCount(){
		return count;
	}

	public void setPosts(List<Post> posts){
		this.posts = posts;
	}

	public List<Post> getPosts(){
		return posts;
	}

	public void setStatus(String status){
		this.status = status;
	}

	public String getStatus(){
		return status;
	}
}