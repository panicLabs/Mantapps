package com.mantapps.reader.model;

import com.google.gson.annotations.SerializedName;

import com.mantapps.reader.util.JsonUtils;

import java.io.Serializable;

/**
 * Created by shaolin on 12/08/15.
 */
public class SearchResult implements Serializable {

    private static final long serialVersionUID = 1L;

    @SerializedName("status")
    private String	status;

    @SerializedName("count")
    private int count;

    @SerializedName("count_total")
    private int count_total;

    @SerializedName("pages")
    private int pages;

    @SerializedName("posts")
    private String	posts;

    public String getStatus() {
        return status;
    }
    public void setStatus(String status) {
        this.status = status;
    }

    public int getCount() {
        return count;
    }
    public void setCount(int count) {
        this.count = count;
    }

    public int getCount_total() {
        return count_total;
    }
    public void setCount_total(int count_total) {
        this.count_total = count_total;
    }

    public int getPages() {
        return pages;
    }
    public void setPages(int pages) {
        this.pages = pages;
    }

    public String getPosts() {
        return posts;
    }
    public void setPosts(String posts) {
        this.posts = posts;
    }

    public String toString() {
        return JsonUtils.toJson(this);
    }

}
