package com.mantapps.reader.model.slug;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

import javax.annotation.Generated;

@Generated("com.robohorse.robopojogenerator")
public class CategoriesItem implements Serializable {
	@SerializedName("parent")
	@Expose
	private int parent;
	@SerializedName("description")
	@Expose
	private String description;
	@SerializedName("id")
	@Expose
	private int id;
	@SerializedName("post_count")
	@Expose
	private int post_count;
	@SerializedName("title")
	@Expose
	private String title;
	@SerializedName("slug")
	@Expose
	private String slug;

	public void setParent(int parent){
		this.parent = parent;
	}

	public int getParent(){
		return parent;
	}

	public void setDescription(String description){
		this.description = description;
	}

	public String getDescription(){
		return description;
	}

	public void setId(int id){
		this.id = id;
	}

	public int getId(){
		return id;
	}

	public void setPost_count(int post_count){
		this.post_count = post_count;
	}

	public int getPost_count(){
		return post_count;
	}

	public void setTitle(String title){
		this.title = title;
	}

	public String getTitle(){
		return title;
	}

	public void setSlug(String slug){
		this.slug = slug;
	}

	public String getSlug(){
		return slug;
	}

}