package com.mantapps.reader.model.recentpost;

import com.google.gson.annotations.Expose;
import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("com.robohorse.robopojogenerator")
public class Query{

	@SerializedName("page")
	@Expose
	private String page;

	@SerializedName("ignore_sticky_posts")
	@Expose
	private boolean ignoreStickyPosts;

	public void setPage(String page){
		this.page = page;
	}

	public String getPage(){
		return page;
	}

	public void setIgnoreStickyPosts(boolean ignoreStickyPosts){
		this.ignoreStickyPosts = ignoreStickyPosts;
	}

	public boolean isIgnoreStickyPosts(){
		return ignoreStickyPosts;
	}
}