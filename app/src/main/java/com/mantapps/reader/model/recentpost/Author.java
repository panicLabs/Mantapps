package com.mantapps.reader.model.recentpost;

import com.google.gson.annotations.Expose;
import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("com.robohorse.robopojogenerator")
public class Author{

	@SerializedName("name")
	@Expose
	private String name;

	@SerializedName("nickname")
	@Expose
	private String nickname;

	@SerializedName("last_name")
	@Expose
	private String lastName;

	@SerializedName("description")
	@Expose
	private String description;

	@SerializedName("id")
	@Expose
	private int id;

	@SerializedName("first_name")
	@Expose
	private String firstName;

	@SerializedName("slug")
	@Expose
	private String slug;

	@SerializedName("url")
	@Expose
	private String url;

	public void setName(String name){
		this.name = name;
	}

	public String getName(){
		return name;
	}

	public void setNickname(String nickname){
		this.nickname = nickname;
	}

	public String getNickname(){
		return nickname;
	}

	public void setLastName(String lastName){
		this.lastName = lastName;
	}

	public String getLastName(){
		return lastName;
	}

	public void setDescription(String description){
		this.description = description;
	}

	public String getDescription(){
		return description;
	}

	public void setId(int id){
		this.id = id;
	}

	public int getId(){
		return id;
	}

	public void setFirstName(String firstName){
		this.firstName = firstName;
	}

	public String getFirstName(){
		return firstName;
	}

	public void setSlug(String slug){
		this.slug = slug;
	}

	public String getSlug(){
		return slug;
	}

	public void setUrl(String url){
		this.url = url;
	}

	public String getUrl(){
		return url;
	}
}