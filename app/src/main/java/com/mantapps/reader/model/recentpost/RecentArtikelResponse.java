package com.mantapps.reader.model.recentpost;

import java.util.List;
import com.google.gson.annotations.Expose;
import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("com.robohorse.robopojogenerator")
public class RecentArtikelResponse{

	@SerializedName("count_total")
	@Expose
	private int countTotal;

	@SerializedName("pages")
	@Expose
	private int pages;

	@SerializedName("query")
	@Expose
	private Query query;

	@SerializedName("count")
	@Expose
	private int count;

	@SerializedName("posts")
	@Expose
	private List<PostsItem> posts;

	@SerializedName("status")
	@Expose
	private String status;

	public void setCountTotal(int countTotal){
		this.countTotal = countTotal;
	}

	public int getCountTotal(){
		return countTotal;
	}

	public void setPages(int pages){
		this.pages = pages;
	}

	public int getPages(){
		return pages;
	}

	public void setQuery(Query query){
		this.query = query;
	}

	public Query getQuery(){
		return query;
	}

	public void setCount(int count){
		this.count = count;
	}

	public int getCount(){
		return count;
	}

	public void setPosts(List<PostsItem> posts){
		this.posts = posts;
	}

	public List<PostsItem> getPosts(){
		return posts;
	}

	public void setStatus(String status){
		this.status = status;
	}

	public String getStatus(){
		return status;
	}
}