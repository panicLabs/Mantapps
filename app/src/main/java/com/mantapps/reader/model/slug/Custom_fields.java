package com.mantapps.reader.model.slug;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import javax.annotation.Generated;

@Generated("com.robohorse.robopojogenerator")
public class Custom_fields{
	@SerializedName("mashsb_timestamp")
	@Expose
	private List<String> mashsb_timestamp;
	@SerializedName("mashsb_jsonshares")
	@Expose
	private List<String> mashsb_jsonshares;
	@SerializedName("mashsb_shares")
	@Expose
	private List<String> mashsb_shares;
	@SerializedName("mashsb_twitter_handle")
	@Expose
	private List<String> mashsb_twitter_handle;

	public void setMashsb_timestamp(List<String> mashsb_timestamp){
		this.mashsb_timestamp = mashsb_timestamp;
	}

	public List<String> getMashsb_timestamp(){
		return mashsb_timestamp;
	}

	public void setMashsb_jsonshares(List<String> mashsb_jsonshares){
		this.mashsb_jsonshares = mashsb_jsonshares;
	}

	public List<String> getMashsb_jsonshares(){
		return mashsb_jsonshares;
	}

	public void setMashsb_shares(List<String> mashsb_shares){
		this.mashsb_shares = mashsb_shares;
	}

	public List<String> getMashsb_shares(){
		return mashsb_shares;
	}

	public void setMashsb_twitter_handle(List<String> mashsb_twitter_handle){
		this.mashsb_twitter_handle = mashsb_twitter_handle;
	}

	public List<String> getMashsb_twitter_handle(){
		return mashsb_twitter_handle;
	}

}