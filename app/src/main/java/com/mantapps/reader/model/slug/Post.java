package com.mantapps.reader.model.slug;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import javax.annotation.Generated;

@Generated("com.robohorse.robopojogenerator")
public class Post{
	@SerializedName("date")
	@Expose
	private String date;
	@SerializedName("comment_count")
	@Expose
	private int comment_count;
	@SerializedName("thumbnail")
	@Expose
	private String thumbnail;
	@SerializedName("comments")
	@Expose
	private List<Object> comments;
	@SerializedName("attachments")
	@Expose
	private List<AttachmentsItem> attachments;
	@SerializedName("author")
	@Expose
	private Author author;
	@SerializedName("custom_fields")
	@Expose
	private Custom_fields custom_fields;
	@SerializedName("type")
	@Expose
	private String type;
	@SerializedName("title")
	@Expose
	private String title;
	@SerializedName("comment_status")
	@Expose
	private String comment_status;
	@SerializedName("url")
	@Expose
	private String url;
	@SerializedName("content")
	@Expose
	private String content;
	@SerializedName("tags")
	@Expose
	private List<TagsItem> tags;
	@SerializedName("title_plain")
	@Expose
	private String title_plain;
	@SerializedName("modified")
	@Expose
	private String modified;
	@SerializedName("id")
	@Expose
	private int id;
	@SerializedName("categories")
	@Expose
	private List<CategoriesItem> categories;
	@SerializedName("excerpt")
	@Expose
	private String excerpt;
	@SerializedName("thumbnail_images")
	@Expose
	private Thumbnail_images thumbnail_images;
	@SerializedName("slug")
	@Expose
	private String slug;
	@SerializedName("status")
	@Expose
	private String status;
	@SerializedName("thumbnail_size")
	@Expose
	private String thumbnail_size;

	public void setDate(String date){
		this.date = date;
	}

	public String getDate(){
		return date;
	}

	public void setComment_count(int comment_count){
		this.comment_count = comment_count;
	}

	public int getComment_count(){
		return comment_count;
	}

	public void setThumbnail(String thumbnail){
		this.thumbnail = thumbnail;
	}

	public String getThumbnail(){
		return thumbnail;
	}

	public void setComments(List<Object> comments){
		this.comments = comments;
	}

	public List<Object> getComments(){
		return comments;
	}

	public void setAttachments(List<AttachmentsItem> attachments){
		this.attachments = attachments;
	}

	public List<AttachmentsItem> getAttachments(){
		return attachments;
	}

	public void setAuthor(Author author){
		this.author = author;
	}

	public Author getAuthor(){
		return author;
	}

	public void setCustom_fields(Custom_fields custom_fields){
		this.custom_fields = custom_fields;
	}

	public Custom_fields getCustom_fields(){
		return custom_fields;
	}

	public void setType(String type){
		this.type = type;
	}

	public String getType(){
		return type;
	}

	public void setTitle(String title){
		this.title = title;
	}

	public String getTitle(){
		return title;
	}

	public void setComment_status(String comment_status){
		this.comment_status = comment_status;
	}

	public String getComment_status(){
		return comment_status;
	}

	public void setUrl(String url){
		this.url = url;
	}

	public String getUrl(){
		return url;
	}

	public void setContent(String content){
		this.content = content;
	}

	public String getContent(){
		return content;
	}

	public void setTags(List<TagsItem> tags){
		this.tags = tags;
	}

	public List<TagsItem> getTags(){
		return tags;
	}

	public void setTitle_plain(String title_plain){
		this.title_plain = title_plain;
	}

	public String getTitle_plain(){
		return title_plain;
	}

	public void setModified(String modified){
		this.modified = modified;
	}

	public String getModified(){
		return modified;
	}

	public void setId(int id){
		this.id = id;
	}

	public int getId(){
		return id;
	}

	public void setCategories(List<CategoriesItem> categories){
		this.categories = categories;
	}

	public List<CategoriesItem> getCategories(){
		return categories;
	}

	public void setExcerpt(String excerpt){
		this.excerpt = excerpt;
	}

	public String getExcerpt(){
		return excerpt;
	}

	public void setThumbnail_images(Thumbnail_images thumbnail_images){
		this.thumbnail_images = thumbnail_images;
	}

	public Thumbnail_images getThumbnail_images(){
		return thumbnail_images;
	}

	public void setSlug(String slug){
		this.slug = slug;
	}

	public String getSlug(){
		return slug;
	}

	public void setStatus(String status){
		this.status = status;
	}

	public String getStatus(){
		return status;
	}

	public void setThumbnail_size(String thumbnail_size){
		this.thumbnail_size = thumbnail_size;
	}

	public String getThumbnail_size(){
		return thumbnail_size;
	}

}