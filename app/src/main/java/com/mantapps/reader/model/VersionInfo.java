package com.mantapps.reader.model;

import com.google.gson.annotations.SerializedName;

import com.mantapps.reader.util.JsonUtils;

import java.io.Serializable;

/**
 * Created by shaolin on 2015/8/26.
 */
public class VersionInfo implements Serializable {

    @SerializedName("version")
    private int vesionNumber;
    @SerializedName("downurl")
    private String downUrl;
    @SerializedName("forcus")
    private boolean force;

    public int getVesionNumber() {
        return vesionNumber;
    }
    public void setVesionNumber(int vesionNumber) {
        this.vesionNumber = vesionNumber;
    }

    public String getDownUrl() {
        return downUrl;
    }
    public void setDownUrl(String downUrl) {
        this.downUrl = downUrl;
    }

    public boolean isForce() {
        return force;
    }
    public void setForce(boolean force) {
        this.force = force;
    }

    public String toString() {
        return JsonUtils.toJson(this);
    }

}
