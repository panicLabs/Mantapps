package com.mantapps.reader.model.recentpost;

import com.google.gson.annotations.Expose;
import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("com.robohorse.robopojogenerator")
public class ThumbSmedium{

	@SerializedName("width")
	@Expose
	private int width;

	@SerializedName("url")
	@Expose
	private String url;

	@SerializedName("height")
	@Expose
	private int height;

	public void setWidth(int width){
		this.width = width;
	}

	public int getWidth(){
		return width;
	}

	public void setUrl(String url){
		this.url = url;
	}

	public String getUrl(){
		return url;
	}

	public void setHeight(int height){
		this.height = height;
	}

	public int getHeight(){
		return height;
	}
}