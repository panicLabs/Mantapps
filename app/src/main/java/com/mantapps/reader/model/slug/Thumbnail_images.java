package com.mantapps.reader.model.slug;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import javax.annotation.Generated;

@Generated("com.robohorse.robopojogenerator")
public class Thumbnail_images{
	@SerializedName("thumbnail")
	@Expose
	private Thumbnail thumbnail;
	@SerializedName("thumb-small")
	@Expose
	private Thumbsmall thumbsmall;
	@SerializedName("thumb-smedium")
	@Expose
	private Thumbsmedium thumbsmedium;
	@SerializedName("thumb-standard")
	@Expose
	private Thumbstandard thumbstandard;
	@SerializedName("thumb-medium")
	@Expose
	private Thumbmedium thumbmedium;
	@SerializedName("medium")
	@Expose
	private Medium medium;
	@SerializedName("thumb-large")
	@Expose
	private Thumblarge thumblarge;
	@SerializedName("full")
	@Expose
	private Full full;

	public void setThumbnail(Thumbnail thumbnail){
		this.thumbnail = thumbnail;
	}

	public Thumbnail getThumbnail(){
		return thumbnail;
	}

	public void setThumbsmall(Thumbsmall thumbsmall){
		this.thumbsmall = thumbsmall;
	}

	public Thumbsmall getThumbsmall(){
		return thumbsmall;
	}

	public void setThumbsmedium(Thumbsmedium thumbsmedium){
		this.thumbsmedium = thumbsmedium;
	}

	public Thumbsmedium getThumbsmedium(){
		return thumbsmedium;
	}

	public void setThumbstandard(Thumbstandard thumbstandard){
		this.thumbstandard = thumbstandard;
	}

	public Thumbstandard getThumbstandard(){
		return thumbstandard;
	}

	public void setThumbmedium(Thumbmedium thumbmedium){
		this.thumbmedium = thumbmedium;
	}

	public Thumbmedium getThumbmedium(){
		return thumbmedium;
	}

	public void setMedium(Medium medium){
		this.medium = medium;
	}

	public Medium getMedium(){
		return medium;
	}

	public void setThumblarge(Thumblarge thumblarge){
		this.thumblarge = thumblarge;
	}

	public Thumblarge getThumblarge(){
		return thumblarge;
	}

	public void setFull(Full full){
		this.full = full;
	}

	public Full getFull(){
		return full;
	}

}