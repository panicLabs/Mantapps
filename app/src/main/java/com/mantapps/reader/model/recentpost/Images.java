package com.mantapps.reader.model.recentpost;

import com.google.gson.annotations.Expose;
import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("com.robohorse.robopojogenerator")
public class Images{

	@SerializedName("thumbnail")
	@Expose
	private Thumbnail thumbnail;

	@SerializedName("thumb-small")
	@Expose
	private ThumbSmall thumbSmall;

	@SerializedName("large")
	@Expose
	private Large large;

	@SerializedName("thumb-smedium")
	@Expose
	private ThumbSmedium thumbSmedium;

	@SerializedName("thumb-standard")
	@Expose
	private ThumbStandard thumbStandard;

	@SerializedName("thumb-medium")
	@Expose
	private ThumbMedium thumbMedium;

	@SerializedName("medium")
	@Expose
	private Medium medium;

	@SerializedName("medium_large")
	@Expose
	private MediumLarge mediumLarge;

	@SerializedName("thumb-large")
	@Expose
	private ThumbLarge thumbLarge;

	@SerializedName("full")
	@Expose
	private Full full;

	public void setThumbnail(Thumbnail thumbnail){
		this.thumbnail = thumbnail;
	}

	public Thumbnail getThumbnail(){
		return thumbnail;
	}

	public void setThumbSmall(ThumbSmall thumbSmall){
		this.thumbSmall = thumbSmall;
	}

	public ThumbSmall getThumbSmall(){
		return thumbSmall;
	}

	public void setLarge(Large large){
		this.large = large;
	}

	public Large getLarge(){
		return large;
	}

	public void setThumbSmedium(ThumbSmedium thumbSmedium){
		this.thumbSmedium = thumbSmedium;
	}

	public ThumbSmedium getThumbSmedium(){
		return thumbSmedium;
	}

	public void setThumbStandard(ThumbStandard thumbStandard){
		this.thumbStandard = thumbStandard;
	}

	public ThumbStandard getThumbStandard(){
		return thumbStandard;
	}

	public void setThumbMedium(ThumbMedium thumbMedium){
		this.thumbMedium = thumbMedium;
	}

	public ThumbMedium getThumbMedium(){
		return thumbMedium;
	}

	public void setMedium(Medium medium){
		this.medium = medium;
	}

	public Medium getMedium(){
		return medium;
	}

	public void setMediumLarge(MediumLarge mediumLarge){
		this.mediumLarge = mediumLarge;
	}

	public MediumLarge getMediumLarge(){
		return mediumLarge;
	}

	public void setThumbLarge(ThumbLarge thumbLarge){
		this.thumbLarge = thumbLarge;
	}

	public ThumbLarge getThumbLarge(){
		return thumbLarge;
	}

	public void setFull(Full full){
		this.full = full;
	}

	public Full getFull(){
		return full;
	}
}