package com.mantapps.reader.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Dibuat oleh Ali pada tanggal 1/4/17.
 * Nama Project : Movies-IAK2.
 * Email        : panic.inc.dev@gmail.com
 */
public class Kategori {
    public String brandName;
    public List<Category> mobiles = new ArrayList<Category>();

    public Kategori(String brandName, List<Category> mobiles) {
        this.brandName = brandName;
        this.mobiles = mobiles;
    }
}
