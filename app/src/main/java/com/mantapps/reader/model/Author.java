package com.mantapps.reader.model;

import java.io.Serializable;

/**
 * Created by Administrator on 2016/6/1.
 */
public class Author implements Serializable {
    private static final long serialVersionUID = 1L;

    private String fullName;

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public int getAuthorId() {
        return authorId;
    }

    public void setAuthorId(int authorId) {
        this.authorId = authorId;
    }

    private int authorId;
}
