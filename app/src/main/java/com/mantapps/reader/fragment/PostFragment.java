package com.mantapps.reader.fragment;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.widget.ShareActionProvider;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.TextView;

import com.mantapps.reader.app.CategoryListActivity;
import com.mantapps.reader.app.PostActivity;
import com.mantapps.reader.model.slug.CategoriesItem;
import com.mantapps.reader.model.slug.SlugResponse;
import com.mantapps.reader.api.MantappsApi;
import com.mantapps.reader.util.RestUtil;
import com.mantapps.reader.view.LoadingView;
import com.bumptech.glide.Glide;
import com.mantapps.reader.R;

import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A placeholder fragment containing a simple view.
 */
public class PostFragment extends Fragment {
    private final String TAG = PostFragment.class.getName();
    private int id;
    private String title;
    private String content;
    private String url;
    private OnPostSelectedListener mListener;
    private WebView webView;
    private LoadingView loadingView;
    private ImageView imageView;
    TextView c1, c2, c3, c4,c5, c6, c7, c8;

    private HashMap<Integer, Integer> colors;

    public PostFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Needed to show Options Menu
        setHasOptionsMenu(true);
        colors = new HashMap<>();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_post, container, false);

        colors.put(1, Color.parseColor("#FF1DD3FA"));
        colors.put(2,Color.parseColor("#FF2288DD"));
        colors.put(3,Color.parseColor("#FFA98D09"));
        colors.put(4,Color.parseColor("#FF7F023D"));
        colors.put(5,Color.parseColor("#FF01B634"));
        colors.put(6,Color.parseColor("#FF10E8A4"));
        colors.put(6857,Color.parseColor("#FF6A05DB"));
        colors.put(6851,Color.parseColor("#FFE302E1"));

        loadingView = (LoadingView) v.findViewById(R.id.loadingView);
        imageView = (ImageView) v.findViewById(R.id.image_);


        // Get data
        Bundle args = getActivity().getIntent().getExtras();
        id = args.getInt("id");
        title = args.getString("title");

        String slug = args.getString("slug");
        if(slug != null || !slug.isEmpty())
            loadApi(slug);


        c1 = (TextView) v.findViewById(R.id.c1);
        c2 = (TextView) v.findViewById(R.id.c2);
        c3 = (TextView) v.findViewById(R.id.c3);
        c4 = (TextView) v.findViewById(R.id.c4);
        c5 = (TextView) v.findViewById(R.id.c5);
        c6 = (TextView) v.findViewById(R.id.c6);
        c7 = (TextView) v.findViewById(R.id.c7);
        c8 = (TextView) v.findViewById(R.id.c8);

        ((PostActivity) getActivity()).getSupportActionBar().setTitle(title);
        String date = args.getString("date");
        String author = args.getString("author");
        content = args.getString("content");
        url = args.getString("url");
        //thumbnailUrl = args.getString("thumbnailUrl");


        // Construct HTML content
        // First, some CSS
        String html = "<head><LINK href=\"http://www.mantapps.co.id/wp-content/themes/hueman/style.css\" type=\"text/css\" rel=\"stylesheet\"/>" +
                "<LINK href=\"http://www.mantapps.co.id/wp-content/themes/hueman/fonts/font-awesome.min.css\" type=\"text/css\" rel=\"stylesheet\"/>" +
                "<LINK href=\"http://www.mantapps.co.id/wp-content/cache/autoptimize/css/autoptimize_5ebc92e22bb225ccd5143307459a5362.css\" type=\"text/css\" rel=\"stylesheet\"/></head>";
//        html += "<style>img{max-width:100%;height:auto;} " +
//                ".titleImg{width:100%;}" +
//                "iframe{width:100%;height:56%;}</style> ";
//
//        html += "<a href=\""+ thumbnailUrl + "\" ><img src=\"" + thumbnailUrl + "\" class=\"titleImg\" /></a>";
//        // Article Title
        html += "<div style=\"padding:10px;padding-top:20px;\">";
        html += "<h1 style=\"font-size:1.4em;\">" + title + "</h1><br /> ";
        // Date & author
        html += "<h4 style=\"color:#999999;font-size:0.8em;\">" + date + "<br />by " + author + "</h4><br />";
        // The actual content
        html += content;
        html += "</div>";
        html += "<div style=\"text-align:center; background:#cdcdcd; padding:22px 0px;margin:20px 0; width:100%; font-size:2em; \"---------------<br />";
        html += "<a href=\""+ url + "\" >MORE DETAIL</a><br />";
        html += "</div><br />";

        // Create the WebView
        webView = (WebView) v.findViewById(R.id.webview_comment);
        // Enable JavaScript in order to be able to Play Youtube videos
        webView.getSettings().setJavaScriptEnabled(true);
        webView.setWebChromeClient(new WebChromeClient());
        // Load and display HTML content
        // Use "charset=UTF-8" to support non-English language
        webView.loadData(html, "text/html; charset=UTF-8", null);

        Log.d(TAG, "Showing post, ID: " + id);
        return v;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        loadingView.setLoading(true);
    }

    private void loadApi(String slug) {
        RestUtil.getInstance().create(MantappsApi.class).getSlug(slug).enqueue(new Callback<SlugResponse>() {
            @Override
            public void onResponse(Call<SlugResponse> call, Response<SlugResponse> response) {
                loadingView.setLoading(false);
                if(response.isSuccessful()){
                    initView(response);
                }
            }

            @Override
            public void onFailure(Call<SlugResponse> call, Throwable t) {
                t.printStackTrace();
                loadingView.setLoading(false);
            }
        });
    }

    private void initView(Response<SlugResponse> response) {
        Glide.with(getActivity())
                .load(response.body().getPost().getThumbnail_images().getFull().getUrl())
                .into(imageView);

        c1.setVisibility(View.GONE);
            c2.setVisibility(View.GONE);
            c3.setVisibility(View.GONE);
            c4.setVisibility(View.GONE);
            c5.setVisibility(View.GONE);
            c6.setVisibility(View.GONE);
            c7.setVisibility(View.GONE);
            c8.setVisibility(View.GONE);
            for (int i = 1;i <=response.body().getPost().getCategories().size();i++ ){
                TextView txv = null;
                switch (i) {
                    case 1:
                        txv = c1;
                        break;
                    case 2:
                        txv = c2;
                        break;
                    case 3:
                        txv = c3;
                        break;
                    case 4:
                        txv = c4;
                        break;
                    case 5:
                        txv = c5;
                        break;
                    case 6:
                        txv = c6;
                        break;
                    case 7:
                        txv = c7;
                        break;
                    default:
                        txv = c8;
                        break;
                }

                final CategoriesItem c = response.body().getPost().getCategories().get(i - 1);
                txv.setText(c.getTitle());
                txv.setVisibility(View.VISIBLE);
                if(colors.containsKey(c.getId())){
                    txv.setBackgroundColor(colors.get(c.getId()));
                }else{
                    txv.setBackgroundColor(Color.parseColor("#FF777777"));
                }

                txv.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        CategoriesItem category = c;
                        Intent intent = new Intent(getActivity(), CategoryListActivity.class);
                        Bundle args = new Bundle();
                        args.putSerializable(CategoryListActivity.CATEGORY, category);
                        intent.putExtras(args);
                        startActivity(intent);
                    }
                });
            }


    }

    @Override
    public void onPause() {
        super.onPause();
        if (webView != null)
            webView.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (webView != null)
            webView.onResume();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_post, menu);

        // Get share menu item
        MenuItem item = menu.findItem(R.id.action_share);
        // Initialise ShareActionProvider
        // Use MenuItemCompat.getActionProvider(item) since we are using AppCompat support library
        ShareActionProvider shareActionProvider = (ShareActionProvider) MenuItemCompat.getActionProvider(item);
        // Share the article URL
        Intent i = new Intent(Intent.ACTION_SEND);
        i.setType("text/plain");
        i.putExtra(Intent.EXTRA_TEXT,  "www.mantapps.co.id \n" + title + "\n" + url);
        shareActionProvider.setShareIntent(i);

        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_share:
                return true;
            case android.R.id.home:
                getActivity().finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnPostSelectedListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnCommentSelectedListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnPostSelectedListener {
        void onPostSelected(int id);
    }
}
