package com.mantapps.reader.fragment;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatDelegate;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.mantapps.reader.R;
import com.mantapps.reader.app.CategoryListActivity;
import com.mantapps.reader.app.SearchResultActivity;
import com.mantapps.reader.model.Category;
import com.mantapps.reader.model.Post;
import com.mantapps.reader.presenter.CategoryPresenter;
import com.mantapps.reader.view.CategoryView;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link SearchFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link SearchFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class SearchFragment extends Fragment {

    static
    {
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
    }

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    EditText id_search_click;

    private OnFragmentInteractionListener mListener;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment SearchFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static SearchFragment newInstance(String param1, String param2) {
        SearchFragment fragment = new SearchFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    public SearchFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_search, container, false);
        initUI(view);
        initEvent();
        return view;
    }

    private void initUI(View view){
        id_search_click = (EditText)view.findViewById(R.id.id_search_click);
    }

    private void initEvent(){
        id_search_click.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                id_search_click.setVisibility(View.INVISIBLE);

                startActivity(new Intent(getActivity(), SearchResultActivity.class));

                // TODO 测试代码，稍后删除：
//                test();

            }
        });
    }

    private void test() {
        CategoryPresenter cp = new CategoryPresenter(new CategoryView() {
            @Override
            public void showProgress() {
                // TODO
            }
            @Override
            public void hideProgress() {
                // TODO
            }
            @Override
            public void onSuccessCategories(ArrayList<Category> firstLevel, ArrayList<Category> secondLevel) {
                Log.d("list", firstLevel.size() + "list.size()" + secondLevel.size());
                Intent intent = new Intent(getActivity(), CategoryListActivity.class);
                intent.putExtra(CategoryListActivity.CATEGORY, firstLevel.get(2));
                startActivity(intent);
            }
            @Override
            public void onSuccessPosts(ArrayList<Post> list) {
                Log.d("list", "list.size()" + list.size());
            }

            @Override
            public void onFailure(String errMsg) {
                // TODO
            }
        });
        cp.getAllCategories();
//        cp.getPosts(453);
//        cp.getPosts(8, "2");
//        cp.getPosts(450, "1", "5");
//        cp.getPostsCount(18, "2");
//        cp.getCategoryTempPic(450);
    }

    @Override
    public void onResume() {
        super.onResume();
        id_search_click.setVisibility(View.VISIBLE);
        id_search_click.requestFocus();
    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        if(!hidden){
            id_search_click.setVisibility(View.VISIBLE);
            id_search_click.requestFocus();
        }
    }

    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
//        try {
//            mListener = (OnFragmentInteractionListener) activity;
//        } catch (ClassCastException e) {
//            throw new ClassCastException(activity.toString()
//                    + " must implement OnFragmentInteractionListener");
//        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        public void onFragmentInteraction(Uri uri);
    }

}
