package com.mantapps.reader.fragment;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.mantapps.reader.R;
import com.mantapps.reader.adaptor.CategoryAdapter;
import com.mantapps.reader.model.Category;
import com.mantapps.reader.model.Post;
import com.mantapps.reader.presenter.CategoryPresenter;
import com.mantapps.reader.view.CategoryView;
import com.mantapps.reader.view.slideexpandable.ActionSlideExpandableListView;

import java.util.ArrayList;


public class CategoryFragment extends Fragment {
    private ActionSlideExpandableListView list;
    private ArrayList<Category> categories = new ArrayList<Category>();
    private ArrayList<Category> mSecondLevel = new ArrayList<Category>();
    private CategoryPresenter categoryPresenter;
    private CategoryAdapter categoryAdaptor;
    private SwipeRefreshLayout category_swipe_refresh_layout;


    public CategoryFragment() {
        // Required empty public constructor
        categoryPresenter = new CategoryPresenter(new CategoryView() {
            @Override
            public void showProgress() {

            }

            @Override
            public void hideProgress() {

            }

            @Override
            public void onSuccessCategories(ArrayList<Category> firstLevel, ArrayList<Category> secondLevel) {
                categories.clear();
                mSecondLevel.clear();
                categories.addAll(firstLevel);
                mSecondLevel.addAll(secondLevel);
                category_swipe_refresh_layout.setRefreshing(false);
                categoryAdaptor.notifyDataSetChanged();
            }


            @Override
            public void onSuccessPosts(ArrayList<Post> list) {



            }

            @Override
            public void onFailure(String errMsg) {

            }
        });

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_category, container, false);
        list = (ActionSlideExpandableListView) v.findViewById(R.id.list);
        category_swipe_refresh_layout= (SwipeRefreshLayout) v.findViewById(R.id.category_swipe_refresh_layout);
        category_swipe_refresh_layout.setColorScheme(android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);
        category_swipe_refresh_layout.setOnRefreshListener(() -> categoryPresenter.getCategory());
        categoryAdaptor = new CategoryAdapter(getActivity(), categories, mSecondLevel);

        // 1.Set categoryAdaptor for the grid view
        list.setAdapter(categoryAdaptor);
        categoryPresenter.getAllCategories();
        return v;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        categoryPresenter.clearDispose();
    }
}
