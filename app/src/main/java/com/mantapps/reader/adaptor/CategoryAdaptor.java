package com.mantapps.reader.adaptor;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.mantapps.reader.R;
import com.mantapps.reader.app.CategoryListActivity;
import com.mantapps.reader.model.Category;
import com.mantapps.reader.presenter.CategoryPresenter;
import com.mantapps.reader.view.CategoryImageView;
import com.mantapps.reader.view.slideexpandable.ActionSlideExpandableListView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Declan on 16/01/15.
 */
public class CategoryAdaptor extends BaseAdapter {
    private Activity activity;
    private LayoutInflater layoutInflater;
    private ActionSlideExpandableListView listView;
    // A list of posts
    private List<Category> categories;
    private List<Category> secondLevel;

    // Constructor
    public CategoryAdaptor(Activity activity, List<Category> categories, List<Category> secondLevel) {
        this.activity = activity;
        this.categories = categories;
        this.secondLevel = secondLevel;
        //this.listView = listView;
    }


    @Override
    public int getCount() {
        return (categories.size() / 2) + (categories.size() % 2);
    }

    @Override
    public Object getItem(int position) {
        return categories.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final ViewHolder viewHolder;

        if (layoutInflater == null) {
            layoutInflater = (LayoutInflater)
                    activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        if (convertView == null) {
            // Inflate list_row layout for each row in the list
            convertView = layoutInflater.inflate(R.layout.expandable_list_item, null);

            // Create the ViewHolder
            viewHolder = new ViewHolder();
            viewHolder.item1 = (RelativeLayout) convertView.findViewById(R.id.item1);
            viewHolder.networkImageView1 =
                    (ImageView) convertView.findViewById(R.id.id_category_image1);
            viewHolder.title1 = (TextView) convertView.findViewById(R.id.id_category_text1);

            viewHolder.item2 = (RelativeLayout) convertView.findViewById(R.id.item2);
            viewHolder.networkImageView2 =
                    (ImageView) convertView.findViewById(R.id.id_category_image2);
            viewHolder.title2 = (TextView) convertView.findViewById(R.id.id_category_text2);
            viewHolder.expandable = (LinearLayout) convertView.findViewById(R.id.expandable);
            //viewHolder.viewCount = (TextView) convertView.findViewById(R.id.view_count);

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        // Get a post and its properties
        int index = position * 2;

        //first item
        final Category category1 = categories.get(index);
        //下载图片
        CategoryPresenter categoryPresenter = new CategoryPresenter(new CategoryImageView() {
            @Override
            public void onSuccessCategoryTempPicUrl(String url) {
                Glide.with(activity)
                        .load(url)
                        .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                        .into(viewHolder.networkImageView1);
            }
        });
        categoryPresenter.getCategoryTempPic(category1.getId());
        viewHolder.title1.setText(category1.getName());
        final View toggleButton = viewHolder.item2.findViewById(R.id.expandable_toggle_button);
        //viewHolder.viewCount.setText(post.getViewCount() + " View(s)\t\t");
        viewHolder.item1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addExpandView(viewHolder.expandable, category1);
                toggleButton.performClick();
            }
        });

        //second item
        index++;
        int size = categories.size();
        if (index < size) {
            final Category category2 = categories.get(index);
            new CategoryPresenter(new CategoryImageView() {
                @Override
                public void onSuccessCategoryTempPicUrl(String url) {
                    Glide.with(activity)
                            .load(url)
                            .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                            .placeholder(R.drawable.defaultpic)
                            .into(viewHolder.networkImageView2);
                }
            }).getCategoryTempPic(category2.getId());
            viewHolder.title2.setText(category2.getName());
            viewHolder.item2.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    addExpandView(viewHolder.expandable, category2);
                    toggleButton.performClick();
                }
            });
        } else {
            viewHolder.title2.setVisibility(View.GONE);
        }

        return convertView;
    }


    private void addExpandView(LinearLayout expandable, Category category) {
        expandable.removeAllViews();
        ArrayList<Category> expandCategory = new ArrayList<Category>();
        expandCategory.add(category);
        expandCategory.add(null);
        expandCategory.addAll(CategoryPresenter.getSelfSecondLevelList(category, secondLevel));
        int size = expandCategory.size();
        if (size == 2) {
            Intent intent = new Intent(activity, CategoryListActivity.class);
            Bundle args = new Bundle();
            args.putSerializable(CategoryListActivity.CATEGORY, category);
            intent.putExtras(args);
            activity.startActivity(intent);
            return;
        }
        //新增自己


        for (int i = 0; i < size ; i++) {
            LinearLayout root = new LinearLayout(activity);
            root.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
            root.setOrientation(LinearLayout.HORIZONTAL);
            View v = LayoutInflater.from(activity).inflate(R.layout.grid_item, root);
            //root.addView(v);

            //1 item
            final ImageView networkImageView1 =
                    (ImageView) v.findViewById(R.id.id_category_image1);

            TextView title1 = (TextView) v.findViewById(R.id.id_category_text1);
            final Category c1 = expandCategory.get(i);
            networkImageView1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(activity, CategoryListActivity.class);
                    Bundle args = new Bundle();
                    args.putSerializable(CategoryListActivity.CATEGORY, c1);
                    intent.putExtras(args);
                    activity.startActivity(intent);
                }
            });
            new CategoryPresenter(new CategoryImageView() {
                @Override
                public void onSuccessCategoryTempPicUrl(String url) {
                    Glide.with(activity)
                            .load(url)
                            .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                            .into(networkImageView1);
                }
            }).getCategoryTempPic(c1.getId());
            title1.setText(c1.getName());


            //2 item
            i++;
            if (i < size) {
                final Category c2 = expandCategory.get(i);
                if (c2 != null) {
                    final ImageView networkImageView2 =
                            (ImageView) v.findViewById(R.id.id_category_image2);
                    TextView title2 = (TextView) v.findViewById(R.id.id_category_text2);
                    title2.setText(c2.getName());
                    networkImageView2.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Intent intent = new Intent(activity, CategoryListActivity.class);
                            Bundle args = new Bundle();
                            args.putSerializable(CategoryListActivity.CATEGORY, c2);
                            intent.putExtras(args);
                            activity.startActivity(intent);
                        }
                    });

                    new CategoryPresenter(new CategoryImageView() {
                        @Override
                        public void onSuccessCategoryTempPicUrl(String url) {
                            Glide.with(activity)
                                    .load(url)
                                    .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                                    .placeholder(R.drawable.defaultpic)
                                    .into(networkImageView2);
                        }
                    }).getCategoryTempPic(c2.getId());
                } else {
                    TextView title2 = (TextView) v.findViewById(R.id.id_category_text2);
                    title2.setVisibility(View.GONE);
                }
            } else {
                TextView title2 = (TextView) v.findViewById(R.id.id_category_text2);
                title2.setVisibility(View.GONE);
            }

            //add item
            expandable.addView(root);
        }
    }

    // View holder pattern
    private static class ViewHolder {
        RelativeLayout item1;
        RelativeLayout item2;
        ImageView networkImageView1;
        TextView title1;
        ImageView networkImageView2;
        TextView title2;

        LinearLayout expandable;
    }
}
