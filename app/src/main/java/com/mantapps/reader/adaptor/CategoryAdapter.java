package com.mantapps.reader.adaptor;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.elvishew.xlog.XLog;
import com.mantapps.reader.R;
import com.mantapps.reader.app.CategoryListActivity;
import com.mantapps.reader.model.Category;
import com.mantapps.reader.presenter.CategoryPresenter;
import com.mantapps.reader.view.slideexpandable.ActionSlideExpandableListView;

import java.util.ArrayList;
import java.util.List;

/**
 * @author ali@pergikuliner
 * @created 7/28/17.
 * @project Mantapps.
 */

public class CategoryAdapter extends BaseAdapter {
    private ArrayList<Integer> bgColors = new ArrayList<Integer>();
    private Activity activity;
    private LayoutInflater layoutInflater;
    private ActionSlideExpandableListView listView;
    // A list of posts
    private List<Category> categories;
    private List<Category> secondLevel;

    // Constructor
    public CategoryAdapter(Activity activity, List<Category> categories, List<Category> secondLevel) {
        this.activity = activity;
        this.categories = categories;
        this.secondLevel = secondLevel;

        bgColors.add(0xFF72B100);
        bgColors.add(0xFFC10708);
        bgColors.add(0xFF01580D);
        bgColors.add(0xFF008080);
        bgColors.add(0xFF400080);
        bgColors.add(0xFF800000);
        bgColors.add(0xFF808040);
        bgColors.add(0xFF8080C0);
        bgColors.add(0xFFFFFF00);
        bgColors.add(0xFFDDFF00);
    }


    @Override
    public int getCount() {
        return (categories.size() / 2) + (categories.size() % 2);
    }

    @Override
    public Object getItem(int position) {
        return categories.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final ViewHolder viewHolder;

        if (layoutInflater == null) {
            layoutInflater = (LayoutInflater)
                    activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        if (convertView == null) {

            int itemIndex = position * 2;

            // Inflate list_row layout for each row in the list
            convertView = layoutInflater.inflate(R.layout.expandable_list_item, null);

            // Create the ViewHolder
            viewHolder = new ViewHolder();
            viewHolder.item1 = (RelativeLayout) convertView.findViewById(R.id.item1);
            viewHolder.item1.setBackgroundColor(bgColors.get(itemIndex));
            final RelativeLayout finalItem1 = viewHolder.item1;
            finalItem1.getViewTreeObserver().addOnPreDrawListener(() -> {
                LinearLayout.LayoutParams linearParams =
                        (LinearLayout.LayoutParams) finalItem1.getLayoutParams();
                linearParams.height = finalItem1.getMeasuredWidth();
                finalItem1.setLayoutParams(linearParams);
                return true;
            });

            viewHolder.networkImageView1 =
                    (ImageView) convertView.findViewById(R.id.id_category_image1);
            viewHolder.networkImageView1.setVisibility(View.GONE);
            viewHolder.title1 = (TextView) convertView.findViewById(R.id.id_category_text1);

            itemIndex++;
            viewHolder.item2 = (RelativeLayout) convertView.findViewById(R.id.item2);
            viewHolder.item2.setBackgroundColor(bgColors.get(itemIndex));
            final RelativeLayout finalItem2 = viewHolder.item2;
            finalItem2.getViewTreeObserver().addOnPreDrawListener(() -> {
                LinearLayout.LayoutParams linearParams =
                        (LinearLayout.LayoutParams) finalItem2.getLayoutParams();
                linearParams.height = finalItem2.getMeasuredWidth();
                finalItem2.setLayoutParams(linearParams);
                return true;
            });

            viewHolder.networkImageView2 =
                    (ImageView) convertView.findViewById(R.id.id_category_image2);
            viewHolder.networkImageView2.setVisibility(View.GONE);
            viewHolder.title2 = (TextView) convertView.findViewById(R.id.id_category_text2);
            viewHolder.expandable = (LinearLayout) convertView.findViewById(R.id.expandable);

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        // Get a post and its properties
        int index = position * 2;

        //first item
        final Category category1 = categories.get(index);

        viewHolder.title1.setText("By " +category1.getName());
        final View toggleButton = viewHolder.item2.findViewById(R.id.expandable_toggle_button);
        viewHolder.item1.setOnClickListener(v -> {
            addExpandView(viewHolder.expandable, category1);
            toggleButton.performClick();
        });

        //second item
        index++;
        int size = categories.size();
        if (index < size) {
            final Category category2 = categories.get(index);
            viewHolder.title2.setText("By " + category2.getName());
            viewHolder.item2.setOnClickListener(v -> {
                addExpandView(viewHolder.expandable, category2);
                toggleButton.performClick();
            });
        } else {
            viewHolder.item2.setVisibility(View.GONE);
            viewHolder.title2.setVisibility(View.GONE);
        }

        return convertView;
    }


    private void addExpandView(LinearLayout expandable, Category category) {
        expandable.removeAllViews();
        ArrayList<Category> expandCategory = new ArrayList<>();
        expandCategory.add(category);
        expandCategory.add(null);
        expandCategory.addAll(CategoryPresenter.getSelfSecondLevelList(category, secondLevel));
        int size = expandCategory.size();

        if (size == 2) {
            Intent intent = new Intent(activity, CategoryListActivity.class);
            Bundle args = new Bundle();
            args.putSerializable(CategoryListActivity.CATEGORY, category);
            intent.putExtras(args);
            activity.startActivity(intent);
            return;
        }


        for (int i = 0; i < size ; i++) {
            LinearLayout root = new LinearLayout(activity);
            root.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
            root.setOrientation(LinearLayout.HORIZONTAL);
            View v = LayoutInflater.from(activity).inflate(R.layout.grid_item, root);

            //1 item
            RelativeLayout layout1 = (RelativeLayout) v.findViewById(R.id.item1);
            final ImageView networkImageView1 =
                    (ImageView) v.findViewById(R.id.id_category_image1);
            TextView title1 = (TextView) v.findViewById(R.id.id_category_text1);
            final Category c1 = expandCategory.get(i);
            networkImageView1.setOnClickListener(v1 -> {
                Intent intent = new Intent(activity, CategoryListActivity.class);
                Bundle args = new Bundle();
                args.putSerializable(CategoryListActivity.CATEGORY, c1);
                intent.putExtras(args);
                activity.startActivity(intent);
            });
            new CategoryPresenter(url -> {
                c1.setImageUrl(url);
                XLog.w("IMAGEURLS1 : " + c1.getImgUrl());
                Glide.with(activity)
                        .load(c1.getImgUrl())
                        .into(networkImageView1);
            }).getCategoryTempPic(c1.getId());

            String cname = c1.getName();
            if(i == 0) {
                layout1.setVisibility(View.GONE);
                cname = "ALL " + cname;
            }else layout1.setVisibility(View.VISIBLE);

            title1.setText(cname);

            RelativeLayout layout2 = (RelativeLayout) v.findViewById(R.id.item2);

            //2 item
            i++;
            if (i < size) {
                final Category c2 = expandCategory.get(i);
                if (c2 != null) {
                    layout2.setVisibility(View.VISIBLE);
                    final ImageView networkImageView2 = (ImageView) v.findViewById(R.id.id_category_image2);
                    TextView title2 = (TextView) v.findViewById(R.id.id_category_text2);
                    title2.setText(c2.getName());
                    networkImageView2.setOnClickListener(v12 -> {
                        Intent intent = new Intent(activity, CategoryListActivity.class);
                        Bundle args = new Bundle();
                        args.putSerializable(CategoryListActivity.CATEGORY, c2);
                        intent.putExtras(args);
                        activity.startActivity(intent);
                    });

                    new CategoryPresenter(url -> {
                        XLog.w("IMAGEURLS2 : " + url);
                        try{
                            Glide.with(activity)
                                    .load(url)
                                    .into(networkImageView2);
                        }catch (Exception e){
                            e.printStackTrace();
                        }

                    }).getCategoryTempPic(c2.getId());


                } else {
                    layout2.setVisibility(View.GONE);
                }
            } else {
                layout2.setVisibility(View.GONE);
            }

            //add item
            expandable.addView(root);
        }
    }

    // View holder pattern
    private static class ViewHolder {
        RelativeLayout item1;
        RelativeLayout item2;
//        RelativeLayout item3;

        ImageView networkImageView1;
        ImageView networkImageView2;
//        ImageView networkImageView3;

        TextView title1;
        TextView title2;
//        TextView title3;

        LinearLayout expandable;
    }

}
