package com.mantapps.reader.adaptor;

/**
 * @author alwiens
 * @created on 30/12/16.
 */

public interface OnLoadMoreListener {
    void onLoadMore();
}