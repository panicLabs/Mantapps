//package com.mantapps.reader.adaptor;
//
//import android.content.Context;
//import android.content.Intent;
//import android.graphics.Color;
//import android.net.Uri;
//import android.os.Bundle;
//import android.support.v7.widget.RecyclerView;
//import android.text.Html;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//import android.view.animation.AlphaAnimation;
//import android.view.animation.DecelerateInterpolator;
//import android.widget.ImageView;
//import android.widget.TextView;
//
//import com.bumptech.glide.Glide;
//import com.mantapps.reader.R;
//import com.mantapps.reader.app.ArticleListActivity;
//import com.mantapps.reader.app.CategoryListActivity;
//import com.mantapps.reader.app.WebViewActivity;
//import com.mantapps.reader.fragment.HomeFragment;
//import com.mantapps.reader.model.Author;
//import com.mantapps.reader.model.Category;
//import com.mantapps.reader.model.Post;
//import com.mtburn.android.sdk.instream.ADVSInstreamAdPlacer;
//import com.mtburn.android.sdk.model.ADVSInstreamInfoModel;
//
//import java.util.HashMap;
//import java.util.List;
//
///**
// * @author alwiens
// * @created on 30/12/16.
// */
//public class HomeAdapter extends RecyclerView.Adapter {
//
//    private static final int ITEM_VIEWTYPE_AD      = 0;
//    private static final int ITEM_VIEWTYPE_CONTENT = 1;
//
//    private final List<Object> mItems;
//    private final ADVSInstreamAdPlacer adPlacer;
////    private final ImageLoader imageLoader;
//    private Context context;
//    private RecyclerView rv;
//
//    private HashMap<Integer, Integer> colors = new HashMap<>();
//    private HomeFragment.OnNewsSelectedListener mListener;
//
//
//    public HomeAdapter(Context c, List<Object> dataList, ADVSInstreamAdPlacer adPlacer, RecyclerView mRecyclerView, HomeFragment.OnNewsSelectedListener mListener) {
//        this.context = c;
//        this.mItems = dataList;
//        this.adPlacer = adPlacer;
//        this.rv = mRecyclerView;
//        this.mListener = mListener;
////        imageLoader = AppController.getInstance().getImageLoader();
//        colors.put(1,Color.parseColor("#FF1DD3FA"));
//        colors.put(2,Color.parseColor("#FF2288DD"));
//        colors.put(3,Color.parseColor("#FFA98D09"));
//        colors.put(4,Color.parseColor("#FF7F023D"));
//        colors.put(5,Color.parseColor("#FF01B634"));
//        colors.put(6,Color.parseColor("#FF10E8A4"));
//        colors.put(6857,Color.parseColor("#FF6A05DB"));
//        colors.put(6851,Color.parseColor("#FFE302E1"));
//
//    }
//
//    @Override
//    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
//        View v;
//        if (viewType == ITEM_VIEWTYPE_AD) {
//            v = LayoutInflater.from(parent.getContext()).inflate(R.layout.custom_instream_ad_view, parent, false);
//            return new AdViewHolder(v, adPlacer);
//        } else {
//            v = LayoutInflater.from(parent.getContext()).inflate(R.layout.home_item, parent, false);
//            return new HomeViewHolder(v);
//        }
//    }
//
//    @Override
//    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
//        if (holder.getItemViewType() == ITEM_VIEWTYPE_AD) {
//            ADVSInstreamInfoModel adInfo = (ADVSInstreamInfoModel) mItems.get(position);
//            AdViewHolder avh = (AdViewHolder)holder;
//            avh.setData(adInfo);
//
//            // インプレッションを送信する（重複排除などは SDK がやるため、毎回通信などは発生しない）
//            adPlacer.measureImp(adInfo);
//        } else {
//            final Post post = ((Post) mItems.get(position));
//            final HomeViewHolder mvh = (HomeViewHolder)holder;
//            String excerpt = post.getExcerpt();
//            int index = excerpt.indexOf("mantapps.co.id");
//            if (index > -1)
//                excerpt = excerpt.substring(0, index);
//
//            mvh.id_post_description.setText(Html.fromHtml(excerpt));
//            AlphaAnimation fadeImage = new AlphaAnimation(0, 1);
//            fadeImage.setDuration(3000);
//            fadeImage.setInterpolator(new DecelerateInterpolator());
//
//            Glide.with(context).load(post.getMediumImageUrl())
//                    .animate(fadeImage)
//                    .error(R.drawable.defaultpic)
//                    .into(mvh.photo);
//           // mvh.photo.setImageUrl(post.getMediumImageUrl(), imageLoader);
//            mvh.txvDate.setText(post.getDate());
//            mvh.id_author.setText(post.getAuthor());
//            //id_categoriesDesc.setText("ssssss");
//            mvh.id_post_title.setText(post.getTitle());
//
//            final Author a = new Author();
//            a.setFullName(post.getAuthor());
//            a.setAuthorId(post.getAuthorId());
//            mvh.id_author.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    Intent intent = new Intent(context, ArticleListActivity.class);
//                    Bundle args = new Bundle();
//                    args.putSerializable(ArticleListActivity.AUTHOR, a);
//                    intent.putExtras(args);
//                    context.startActivity(intent);
//                }
//            });
//
//            mvh.c1.setVisibility(View.GONE);
//            mvh.c2.setVisibility(View.GONE);
//            mvh.c3.setVisibility(View.GONE);
//            mvh.c4.setVisibility(View.GONE);
//            mvh.c5.setVisibility(View.GONE);
//            mvh.c6.setVisibility(View.GONE);
//            mvh.c7.setVisibility(View.GONE);
//            mvh.c8.setVisibility(View.GONE);
//            for (int i = 1; i <= post.getCategories().size(); i++) {
//                TextView txv = null;
//                switch (i) {
//                    case 1:
//                        txv = mvh.c1;
//                        break;
//                    case 2:
//                        txv = mvh.c2;
//                        break;
//                    case 3:
//                        txv = mvh.c3;
//                        break;
//                    case 4:
//                        txv = mvh.c4;
//                        break;
//                    case 5:
//                        txv = mvh.c5;
//                        break;
//                    case 6:
//                        txv = mvh.c6;
//                        break;
//                    case 7:
//                        txv = mvh.c7;
//                        break;
//                    default:
//                        txv = mvh.c8;
//                        break;
//                }
//
//                final Category c = post.getCategories().get(i - 1);
//                txv.setText(c.getName());
//                txv.setVisibility(View.VISIBLE);
//                if (colors.containsKey(c.getId())) {
//                    txv.setBackgroundColor(colors.get(c.getId()));
//                } else {
//                    txv.setBackgroundColor(Color.parseColor("#FF777777"));
//                }
//
//                txv.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        Category category = c;
//                        Intent intent = new Intent(context, CategoryListActivity.class);
//                        Bundle args = new Bundle();
//                        args.putSerializable(CategoryListActivity.CATEGORY, category);
//                        intent.putExtras(args);
//                        context.startActivity(intent);
//                    }
//                });
//            }
//
//            mvh.txvTwitter.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    openUrl(post.getUrl() + "/?share=twitter&nb=1");
//                }
//            });
//
//            mvh.txvFacebook.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    openUrl(post.getUrl() + "/?share=facebook&nb=1");
//                }
//            });
//
//            mvh.txvGoogle.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    openUrl(post.getUrl() + "/?share=google-plus-1&nb=1");
//                }
//            });
//
//            mvh.txvPinterest.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    openUrl(post.getUrl() + "/?share=pinterest&nb=1");
//                }
//            });
//
//            mvh.itemView.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    // Send data to MainActivity
//                    HashMap<String, String> map = new HashMap<>();
//                    map.put("id", String.valueOf(post.getId()));
//                    map.put("title", post.getTitle());
//                    map.put("slug", post.getSlug());
//                    map.put("date", post.getDate());
//                    map.put("author", post.getAuthor());
//                    map.put("content", post.getContent());
//                    map.put("url", post.getUrl());
//                    map.put("thumbnailURL", post.getMediumImageUrl());
//                    mListener.onNewsSelected(mvh.photo, map);
//                }
//            });
//
//        }
//    }
//
//    private void openUrl(String s) {
//        Intent intent = new Intent(context, WebViewActivity.class);
//        intent.setData(Uri.parse(s));
//        context.startActivity(intent);
//    }
//
//    @Override
//    public int getItemCount() {
//        return mItems.size();
//    }
//
//    @Override
//    public int getItemViewType(int position) {
//        return isAd(position) ? ITEM_VIEWTYPE_AD : ITEM_VIEWTYPE_CONTENT;
//    }
//
//    @Override
//    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
//        super.onAttachedToRecyclerView(recyclerView);
//    }
//
//    private boolean isAd(int position) {
//        return (mItems.get(position) instanceof ADVSInstreamInfoModel) ? true : false;
//    }
//
//
//    public class HomeViewHolder extends RecyclerView.ViewHolder {
//        TextView id_post_description;
//        TextView txvDate;
//        TextView id_author;
//        ImageView txvTwitter;
//        ImageView txvFacebook;
//        ImageView txvGoogle;
//        ImageView txvPinterest;
//        TextView id_post_title;
//        TextView c1;
//        TextView c2;
//        TextView c3;
//        TextView c4;
//        TextView c5;
//        TextView c6;
//        TextView c7;
//        TextView c8;
//        final ImageView photo;
//
//        public HomeViewHolder(View convertView) {
//            super(convertView);
//            photo = (ImageView) convertView.findViewById(R.id.id_photo);
//            id_author = (TextView) convertView.findViewById(R.id.id_author);
//            id_post_title = (TextView) convertView.findViewById(R.id.id_post_title);
//            txvTwitter = (ImageView) convertView.findViewById(R.id.txvTwitter);
//            txvFacebook = (ImageView) convertView.findViewById(R.id.txvFacebook);
//            txvGoogle = (ImageView) convertView.findViewById(R.id.txvGoogle);
//            txvPinterest = (ImageView) convertView.findViewById(R.id.txvPinterest);
//            //id_categoriesDesc = (TextView)convertView.findViewById(R.id.id_categoriesDesc);
//            c1 = (TextView) convertView.findViewById(R.id.c1);
//            c2 = (TextView) convertView.findViewById(R.id.c2);
//            c3 = (TextView) convertView.findViewById(R.id.c3);
//            c4 = (TextView) convertView.findViewById(R.id.c4);
//            c5 = (TextView) convertView.findViewById(R.id.c5);
//            c6 = (TextView) convertView.findViewById(R.id.c6);
//            c7 = (TextView) convertView.findViewById(R.id.c7);
//            c8 = (TextView) convertView.findViewById(R.id.c8);
//            txvDate = (TextView) convertView.findViewById(R.id.txvDate);
//            id_post_description = (TextView) convertView.findViewById(R.id.id_post_description);
//
//        }
//    }
//
//    static class AdViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
//        ADVSInstreamAdPlacer adPlacer;
//        ADVSInstreamInfoModel adData;
//
//        TextView advertiserName;
//        TextView adText;
//        ImageView adImage;
//        TextView adSponsoredLabel;
//
//        public AdViewHolder(View itemView, ADVSInstreamAdPlacer adPlacer) {
//            super(itemView);
//            this.adPlacer = adPlacer;
//
//            itemView.setOnClickListener(this);
//
//            advertiserName = (TextView) itemView.findViewById(R.id.custom_instream_advertiser_name);
//            adText = (TextView) itemView.findViewById(R.id.custom_instream_ad_text);
//            adImage = (ImageView) itemView.findViewById(R.id.custom_instream_ad_image);
//        }
//
//        void setData(ADVSInstreamInfoModel adData) {
//            this.adData = adData;
//
//            advertiserName.setText(adData.title());
//            adText.setText(adData.content());
//            Glide.with(adImage.getContext()).load(adData.creative_url()).into(adImage);
////            (new ImageLoader(adData.creative_url(), adImage)).execute();
////            (new ImageLoader(adData.icon_creative_url(), iconImage)).execute();
//        }
//
//        @Override
//        public void onClick(View view) {
//            adPlacer.sendClickEvent(adData);
//        }
//    }
//}
