package com.mantapps.reader.adaptor;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.elvishew.xlog.XLog;
import com.mantapps.reader.R;
import com.mantapps.reader.model.Post;
import com.mtburn.android.sdk.instream.ADVSInstreamAdPlacer;
import com.mtburn.android.sdk.model.ADVSInstreamInfoModel;

import java.util.List;

/**
 * Created by Declan on 16/01/15.
 */
public class PostAdaptor extends BaseAdapter {
    private Activity activity;
    private LayoutInflater layoutInflater;
    // A list of posts
  //  private List<Post> posts;
    public List<Object> dataSourceList;
    private ADVSInstreamAdPlacer adPlacer;
    private static final String TAG_DEFAULT = "TAG_DEFAULT";




    private int capacity = -1;
    // Constructor
    public PostAdaptor(Activity activity, List<Object> posts, ADVSInstreamAdPlacer adPlacer) {
        this.activity = activity;
        this.dataSourceList = posts;
        this.adPlacer = adPlacer;
    }

    /**
     * 设置容量
     * @param capacity -1默认使用posts所有内容
     */
    public void setCapacity(int capacity) {
        if (capacity == -1) {
            this.capacity = dataSourceList.size();
            return;
        }
        if (dataSourceList.size() < capacity) {
            this.capacity = dataSourceList.size();
        } else {
            this.capacity = capacity;
        }
    }

    @Override
    public int getCount() {
        if (capacity == -1) {
            return dataSourceList.size();
        }
        return capacity;
    }

    @Override
    public Object getItem(int position) {
        return dataSourceList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getViewTypeCount() {
        return 2;
    }

    @Override
    public int getItemViewType(int position) {
        return isAd(position) ? 0 : 1;
    }

    private boolean isAd(int position) {
        return (getItem(position) instanceof ADVSInstreamInfoModel);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (isAd(position)) {
            try {
                convertView = adPlacer.placeAd((ADVSInstreamInfoModel) getItem(position), convertView, parent);
            }catch (Exception e){
                XLog.e("Isad", "getView: "+e);
            }
        } else {
            // コンテンツ View を表示する
            if (null == convertView || !TAG_DEFAULT.equals(convertView.getTag())) {
                LayoutInflater inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = inflater.inflate(R.layout.list_row, parent,false);
                convertView.setTag(TAG_DEFAULT);
            }
            ImageView networkImageView;
            TextView title;
            TextView bys;

            networkImageView =
                    (ImageView) convertView.findViewById(R.id.thumbnail);
            title = (TextView) convertView.findViewById(R.id.title);
            bys = (TextView) convertView.findViewById(R.id.id_author);

            Post post = ((Post) dataSourceList.get(position));
            Glide.with(activity).load(post.getThumbnailUrl()).asBitmap().into(networkImageView);
            title.setText(post.getTitle());
            bys.setText(post.getAuthor());

        }
        return convertView;

    }
    // (7) Use  ViewHolder pattern to improve performance.
    public static class AdViewHolder {
        TextView advertiserName;
        TextView adText;
        public ImageView adImage;
        //public ImageView iconImage;

        public AdViewHolder(View convertView) {
            advertiserName = (TextView) convertView.findViewById(R.id.custom_instream_advertiser_name);
            adText = (TextView) convertView.findViewById(R.id.custom_instream_ad_text);
            adImage = (ImageView) convertView.findViewById(R.id.custom_instream_ad_image);
            //iconImage = (ImageView) convertView.findViewById(R.id.custom_instream_advertiser_icon);
        }

        public void setData(ADVSInstreamInfoModel adData) {
            advertiserName.setText(adData.title());
            adText.setText(adData.content());
        }
    }
}
