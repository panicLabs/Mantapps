//package com.mantapps.reader.adaptor;
//
//import android.content.Context;
//import android.content.Intent;
//import android.graphics.Color;
//import android.net.Uri;
//import android.os.Bundle;
//import android.text.Html;
//import android.util.Log;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//import android.view.animation.AlphaAnimation;
//import android.view.animation.DecelerateInterpolator;
//import android.widget.BaseAdapter;
//import android.widget.ImageView;
//import android.widget.TextView;
//
//import com.bumptech.glide.Glide;
//import com.mantapps.reader.R;
//import com.mantapps.reader.app.ArticleListActivity;
//import com.mantapps.reader.app.CategoryListActivity;
//import com.mantapps.reader.app.WebViewActivity;
//import com.mantapps.reader.fragment.FlipFragment;
//import com.mantapps.reader.model.Author;
//import com.mantapps.reader.model.Category;
//import com.mantapps.reader.model.Post;
//import com.mtburn.android.sdk.instream.ADVSInstreamAdPlacer;
//import com.mtburn.android.sdk.model.ADVSInstreamInfoModel;
//
//import java.util.HashMap;
//import java.util.List;
//
///**
// * Created by Ali@PergiKuliner on 16/11/16.
// */
//
//public class FlipAdapter extends BaseAdapter {
//
//    public List<Object> dataSourceList;
//    public final List<Post> postList = null;
//    private final Context context;
//    private LayoutInflater layoutInflater;
//
//    private HashMap<Integer, Integer> colors = new HashMap<>();
//    private ADVSInstreamAdPlacer adPlacer;
//
//    public FlipAdapter(Context context, List<Object> postList, ADVSInstreamAdPlacer adPlacer, FlipFragment.OnNewsSelectedListener listener) {
//        this.context = context;
//        this.dataSourceList = postList;
//        this.adPlacer = adPlacer;
//        this.mListener = listener;
//
//        colors.put(1,Color.parseColor("#FF1DD3FA"));
//        colors.put(2,Color.parseColor("#FF2288DD"));
//        colors.put(3,Color.parseColor("#FFA98D09"));
//        colors.put(4,Color.parseColor("#FF7F023D"));
//        colors.put(5,Color.parseColor("#FF01B634"));
//        colors.put(6,Color.parseColor("#FF10E8A4"));
//        colors.put(6857,Color.parseColor("#FF6A05DB"));
//        colors.put(6851,Color.parseColor("#FFE302E1"));
//
//    }
//
//    @Override
//    public int getCount() {
//        return dataSourceList.size();
//    }
//
//    @Override
//    public int getViewTypeCount() {
//        return 2;
//    }
//
//    @Override
//    public Object getItem(int position) {
//        return dataSourceList.get(position);
//    }
//
//    @Override
//    public int getItemViewType(int position) {
//        return isAd(position) ? 0 : 1;
//    }
//
//    private boolean isAd(int position) {
//        return (getItem(position) instanceof ADVSInstreamInfoModel);
//    }
//
//    @Override
//    public long getItemId(int position) {
//        return position;
//    }
//
//    @Override
//    public View getView(final int position, View convertView, ViewGroup parent) {
//        if (isAd(position)) {
//            Log.d("Isad", "getView: ");
//            try {
//                convertView = adPlacer.placeAd((ADVSInstreamInfoModel) getItem(position), convertView, parent);
//            } catch (Error e) {
//                Log.d("Isad", "getView: " + e);
//            }
//        }else {
//
//            if (layoutInflater == null) {
//                layoutInflater = (LayoutInflater)
//                        context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//            }
//            final PostViewHolder holder;
//            if (convertView == null) {
//                convertView = layoutInflater.inflate(R.layout.layout_post, null);
//                holder = new PostViewHolder();
//                holder.id_photo = (ImageView) convertView.findViewById(R.id.id_photo);
//                holder.id_author = (TextView) convertView.findViewById(R.id.id_author);
//                holder.id_post_title = (TextView) convertView.findViewById(R.id.id_post_title);
//                holder.txvTwitter = (ImageView) convertView.findViewById(R.id.txvTwitter);
//                holder.txvFacebook = (ImageView) convertView.findViewById(R.id.txvFacebook);
//                holder.txvGoogle = (ImageView) convertView.findViewById(R.id.txvGoogle);
//                holder.txvPinterest = (ImageView) convertView.findViewById(R.id.txvPinterest);
//                //holder.id_categoriesDesc = (TextView)convertView.findViewById(R.id.id_categoriesDesc);
//                holder.c1 = (TextView) convertView.findViewById(R.id.c1);
//                holder.c2 = (TextView) convertView.findViewById(R.id.c2);
//                holder.c3 = (TextView) convertView.findViewById(R.id.c3);
//                holder.c4 = (TextView) convertView.findViewById(R.id.c4);
//                holder.c5 = (TextView) convertView.findViewById(R.id.c5);
//                holder.c6 = (TextView) convertView.findViewById(R.id.c6);
//                holder.c7 = (TextView) convertView.findViewById(R.id.c7);
//                holder.c8 = (TextView) convertView.findViewById(R.id.c8);
//                holder.txvDate = (TextView) convertView.findViewById(R.id.txvDate);
//                //holder.id_photo.setAdjustViewBounds(true);
//                AlphaAnimation fadeImage = new AlphaAnimation(0, 1);
//                fadeImage.setDuration(3000);
//                fadeImage.setInterpolator(new DecelerateInterpolator());
//                holder.id_post_description = (TextView) convertView.findViewById(R.id.id_post_description);
//                convertView.setTag(holder);
//            } else {
//                holder = (PostViewHolder) convertView.getTag();
//            }
//
//            final Post post = ((Post) dataSourceList.get(position));
//
//            String excerpt = post.getExcerpt();
//            int index = excerpt.indexOf("mantapps.co.id");
//            if (index > -1)
//                excerpt = excerpt.substring(0, index);
//
//            holder.id_post_description.setText(Html.fromHtml(excerpt));
//            Glide.with(context).load(post.getMediumImageUrl())
//                    .thumbnail(0.1f)
//                    .into(holder.id_photo);
////            holder.id_photo.setImageUrl(post.getMediumImageUrl(), imageLoader);
//            holder.txvDate.setText(post.getDate());
//            holder.id_author.setText(post.getAuthor());
//            //holder.id_categoriesDesc.setText("ssssss");
//            holder.id_post_title.setText(post.getTitle());
//
//            final Author a = new Author();
//            a.setFullName(post.getAuthor());
//            a.setAuthorId(post.getAuthorId());
//            holder.id_author.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    Intent intent = new Intent(context, ArticleListActivity.class);
//                    Bundle args = new Bundle();
//                    args.putSerializable(ArticleListActivity.AUTHOR, a);
//                    intent.putExtras(args);
//                    context.startActivity(intent);
//                }
//            });
//
//            holder.c1.setVisibility(View.GONE);
//            holder.c2.setVisibility(View.GONE);
//            holder.c3.setVisibility(View.GONE);
//            holder.c4.setVisibility(View.GONE);
//            holder.c5.setVisibility(View.GONE);
//            holder.c6.setVisibility(View.GONE);
//            holder.c7.setVisibility(View.GONE);
//            holder.c8.setVisibility(View.GONE);
//            for (int i = 1; i <= post.getCategories().size(); i++) {
//                TextView txv = null;
//                switch (i) {
//                    case 1:
//                        txv = holder.c1;
//                        break;
//                    case 2:
//                        txv = holder.c2;
//                        break;
//                    case 3:
//                        txv = holder.c3;
//                        break;
//                    case 4:
//                        txv = holder.c4;
//                        break;
//                    case 5:
//                        txv = holder.c5;
//                        break;
//                    case 6:
//                        txv = holder.c6;
//                        break;
//                    case 7:
//                        txv = holder.c7;
//                        break;
//                    default:
//                        txv = holder.c8;
//                        break;
//                }
//
//                final Category c = post.getCategories().get(i - 1);
//                txv.setText(c.getName());
//                txv.setVisibility(View.VISIBLE);
//                if (colors.containsKey(c.getId())) {
//                    txv.setBackgroundColor(colors.get(c.getId()));
//                } else {
//                    txv.setBackgroundColor(Color.parseColor("#FF777777"));
//                }
//
//                txv.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        Category category = c;
//                        Intent intent = new Intent(context, CategoryListActivity.class);
//                        Bundle args = new Bundle();
//                        args.putSerializable(CategoryListActivity.CATEGORY, category);
//                        intent.putExtras(args);
//                        context.startActivity(intent);
//                    }
//                });
//            }
//
//            holder.txvTwitter.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    openUrl(post.getUrl() + "/?share=twitter&nb=1");
//                }
//            });
//
//            holder.txvFacebook.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    openUrl(post.getUrl() + "/?share=facebook&nb=1");
//                }
//            });
//
//            holder.txvGoogle.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    openUrl(post.getUrl() + "/?share=google-plus-1&nb=1");
//                }
//            });
//
//            holder.txvPinterest.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    openUrl(post.getUrl() + "/?share=pinterest&nb=1");
//                }
//            });
//
//            convertView.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    // Send data to MainActivity
//                    HashMap<String, String> map = new HashMap<>();
//                    map.put("id", String.valueOf(post.getId()));
//                    map.put("title", post.getTitle());
//                    map.put("slug", post.getSlug());
//                    map.put("date", post.getDate());
//                    map.put("author", post.getAuthor());
//                    map.put("content", post.getContent());
//                    map.put("url", post.getUrl());
//                    map.put("thumbnailURL", post.getMediumImageUrl());
//                    mListener.onNewsSelected(holder.id_photo, map);
//                }
//            });
//        }
//        return convertView;
//    }
//
//    private void openUrl(String url) {
//        Intent intent = new Intent(context, WebViewActivity.class);
//        intent.setData(Uri.parse(url));
//        context.startActivity(intent);
//    }
//
//
//    private static class PostViewHolder {
//        TextView id_post_description;
//        TextView txvDate;
//        TextView id_author;
//        ImageView txvTwitter;
//        ImageView txvFacebook;
//        ImageView txvGoogle;
//        ImageView txvPinterest;
//        TextView id_post_title;
//        TextView c1;
//        TextView c2;
//        TextView c3;
//        TextView c4;
//        TextView c5;
//        TextView c6;
//        TextView c7;
//        TextView c8;
//        ImageView id_photo;
//    }
//}