package com.mantapps.reader.util;

/**
 * @author panicLabs on 07/02/17.
 *
 * Exception to be thrown when activities that do not set the screen name
 * for GA screen view events
 */
public class UnsetScreenException extends Exception {

    public UnsetScreenException(String message) {
        super(message);
    }
}
