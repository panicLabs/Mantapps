package com.mantapps.reader.util;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.List;
import java.util.Map;

public class JsonUtils {
	private static GsonBuilder builder = new GsonBuilder();
	private static Gson gson = builder.setDateFormat("yyyy/MM/dd HH:ss").serializeNulls().create();


//	static {
//		//builder.registerTypeAdapter(String.class, );
//       // builder.registerTypeAdapter(ResultEntity.class, );
//	}
	/**
	 * object convert json string
	 * @param src
	 * @return
	 */
	public static String toJson(Object src) {
		
		return gson.toJson(src);
	}
	
	/**
	 * list object convert string
	 * @param list
	 * @return
	 */
	public static <T> String list2Json(List<T> list) {
		
	    return gson.toJson(list);
	}
	
	/**
	 * json string convert object.
	 * @param json
	 * @param classOfT object class
	 * @return
	 */
	public static <T>T fromJson(String json, Class<T> classOfT) {
		return gson.fromJson(json, classOfT);
	}


	/**
	 * json string convert array object.
	 * @param jsonString
	 * @param typeToken
	 * @return
	 */
	public static <T> List<T> fromJsonArray(String jsonString, TypeToken<List<T>> typeToken){
	    Type type = typeToken.getType(); 
	    return gson.fromJson(jsonString, type); 
	}
	
	/**
	 * json string convert map object
	 * @param jsonString
	 * @param typeToken
	 * @return
	 */
	public static <K, V> Map<K, V> json2Map(String jsonString, TypeToken<Map<K, V>> typeToken) {
	    Type type = typeToken.getType(); 
	    return gson.fromJson(jsonString, type);
	}

}  