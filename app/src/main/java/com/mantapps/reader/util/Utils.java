package com.mantapps.reader.util;

import android.app.DownloadManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.PixelFormat;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.support.v4.app.Fragment;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.webkit.CookieManager;
import android.webkit.CookieSyncManager;

import com.mantapps.reader.R;

import java.lang.reflect.Field;

/**
 * Utility.
 * 
 */
public final class Utils {
	private static String sAppName = "";

	public static int darkenColor(int color, float factor) {
		int a = Color.alpha(color);
		int r = Math.round(Color.red(color) * factor);
		int g = Math.round(Color.green(color) * factor);
		int b = Math.round(Color.blue(color) * factor);
		return Color.argb(a,
				Math.min(r,255),
				Math.min(g,255),
				Math.min(b,255));
	}

	public static int getRandomColor(){
		ColorGenerator mColorGenerator = ColorGenerator.MATERIAL;

		return mColorGenerator.getRandomColor();
	}

	public static boolean isWifi(Context context) {
		if (context == null) {
			return false;
		}
		ConnectivityManager connectivityManager = (ConnectivityManager) context
				.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo activeNetInfo = connectivityManager.getActiveNetworkInfo();
		if (activeNetInfo != null
				&& activeNetInfo.getType() == ConnectivityManager.TYPE_WIFI) {
			return true;
		}
		return false;
	}

	public static boolean isOnline(Context context) {
		ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo mobileNetwork = connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
		NetworkInfo wifi = connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);

		return mobileNetwork.isConnected() || wifi.isConnected();
	}

	public static String getMobile(Context context) {
		TelephonyManager telephonyManager = (TelephonyManager) context
				.getSystemService(Context.TELEPHONY_SERVICE);
		return telephonyManager.getLine1Number();
	}

	public static String getProvider(Context context) {
		TelephonyManager telephonyManager = (TelephonyManager) context
				.getSystemService(Context.TELEPHONY_SERVICE);
		return telephonyManager.getSubscriberId();
	}

	public static final String getIMEI(final Context context) {
		TelephonyManager manager = (TelephonyManager) context
				.getSystemService(Context.TELEPHONY_SERVICE);
		return manager.getDeviceId();
	}

	public static int getScreenWidth(Context context) {
		DisplayMetrics metrics = context.getResources().getDisplayMetrics();
		return metrics.widthPixels;
	}

	public static int getScreenHeight(Context context) {
		DisplayMetrics metrics = context.getResources().getDisplayMetrics();
		return metrics.heightPixels;
	}

	public static String getSystemVersion(Context context) {
		return android.os.Build.VERSION.RELEASE;
	}

	public static String getModel(Context context) {
		return android.os.Build.MODEL != null ? android.os.Build.MODEL.replace(
				" ", "") : "unknown";
	}

	public static float getDensity(Context context) {
		return context.getResources().getDisplayMetrics().density;
	}

	public static String getVersionName(Context context) {
		try {
			PackageInfo pinfo = context.getPackageManager()
					.getPackageInfo(context.getPackageName(),
							PackageManager.GET_CONFIGURATIONS);
			return pinfo.versionName;
		} catch (NameNotFoundException e) {
		}

		return "";
	}

	public static String getAppName(Context context) {
		if (TextUtils.isEmpty(sAppName)) {
			sAppName = "com_himax_himax";
			try {
				PackageInfo pinfo = context.getPackageManager().getPackageInfo(
						context.getPackageName(),
						PackageManager.GET_CONFIGURATIONS);
				String packageName = pinfo.packageName;
				if (!TextUtils.isEmpty(packageName)) {
					sAppName = packageName.replaceAll("\\.", "_");
				}
			} catch (NameNotFoundException e) {
			}
		}

		return sAppName;
	}

	public static int getVersionCode(Context context) {
		try {
			PackageInfo pinfo = context.getPackageManager()
					.getPackageInfo(context.getPackageName(),
							PackageManager.GET_CONFIGURATIONS);
			return pinfo.versionCode;
		} catch (NameNotFoundException e) {
		}

		return 1;
	}

    /**
     * 设置childfragmentmanager对象为空
     * @param f
     */
    public static void setChildFragmentManagerToNull(Fragment f) {
        try {
            Field childFragmentManager = Fragment.class.getDeclaredField("mChildFragmentManager");
            childFragmentManager.setAccessible(true);
            childFragmentManager.set(f, null);
        } catch (NoSuchFieldException e) {
        } catch (IllegalAccessException e) {
        }
    }


	public static void clearWebviewCache(Context context) {
		CookieSyncManager.createInstance(context);
		CookieManager.getInstance().removeAllCookie();
		//CookieManager.getInstance().removeSessionCookie();
		CookieSyncManager.getInstance().sync();
	}

	public static void initDownManager(Context context, String downurl){

		DownloadManager Manager = (DownloadManager)context.getSystemService(Context.DOWNLOAD_SERVICE);
		Uri uri = Uri.parse(downurl);
		DownloadManager.Request request = new DownloadManager.Request(uri);
		request.setAllowedNetworkTypes(DownloadManager.Request.NETWORK_MOBILE
				| DownloadManager.Request.NETWORK_WIFI);
		// 设置下载路径和文件名
		request.setTitle("himax");
		request.setDestinationInExternalPublicDir("download", "himax.apk");
		request.setDescription("The new version is being downloaded，Please wait...");
		request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
		request.setMimeType("application/vnd.android.package-archive");
		// 设置为可被媒体扫描器找到
		request.allowScanningByMediaScanner();
		// 设置为可见和可管理
		request.setVisibleInDownloadsUi(true);
		long refernece = Manager.enqueue(request);
		// 把当前下载的ID保存起来
		SharedPreferences sPreferences = context.getSharedPreferences("downloadcomplete", 0);
		sPreferences.edit().putLong("refernece", refernece).commit();

	}

	// calling share intent
	public static void shareData(Context context, String title, String Body) {
		Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
		sharingIntent.setType("text/plain");
		sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, title);
		sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, Body);
		sharingIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		context.startActivity(Intent.createChooser(sharingIntent, context.getResources().getString(R.string.share_with)));
	}


	// saving data to sharedprefrences
	public static void saveData(Context context, String key, String text) {
		SharedPreferences.Editor editor = context.getSharedPreferences(
				"MANTAPPS_PREFS", context.MODE_PRIVATE).edit();
		editor.putString(key, text);
		editor.commit();
	}

	// loading data from sharedprefrences
	public static String loadData(Context context, String key) {
		SharedPreferences prefs = context.getSharedPreferences(
				"MANTAPPS_PREFS", context.MODE_PRIVATE);
		String text = prefs.getString(key, "");
		return text;
	}

    public static Bitmap drawableToBitmap(Drawable drawable) {

        int w = drawable.getIntrinsicWidth();
        int h = drawable.getIntrinsicHeight();

        Bitmap.Config config = drawable.getOpacity() != PixelFormat.OPAQUE ? Bitmap.Config.ARGB_8888
                : Bitmap.Config.RGB_565;

        Bitmap bitmap = Bitmap.createBitmap(w, h, config);

        Canvas canvas = new Canvas(bitmap);
        drawable.setBounds(0, 0, w, h);

        drawable.draw(canvas);
        return bitmap;
    }

	public static int dpToPx(int dp)
	{
		return (int) (dp * Resources.getSystem().getDisplayMetrics().density);
	}

	public static int pxToDp(int px)
	{
		return (int) (px / Resources.getSystem().getDisplayMetrics().density);
	}


}
