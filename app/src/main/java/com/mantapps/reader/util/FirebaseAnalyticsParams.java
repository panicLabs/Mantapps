package com.mantapps.reader.util;

import android.os.Bundle;

/**
 * @author ali@pergikuliner
 * @created 6/12/17.
 * @project Mantapps.
 */

public class FirebaseAnalyticsParams {
    // ① 『Builderパターン』でパラメータ設定を定義
    private Bundle mParams;
    private FirebaseAnalyticsParams(Builder builder) {
        mParams = builder.params;
    }
    public static class Builder {
        private Bundle params = new Bundle();

        public Builder param1(String param1) {
            params.putString("param1", param1);
            return this;
        }

        public Builder param2(Integer param2) {
            params.putInt("param2", param2);
            return this;
        }

        public Builder param3(String param3) {
            params.putString("param3", param3);
            return this;
        }

        public FirebaseAnalyticsParams build() {
            return new FirebaseAnalyticsParams(this);
        }
    }

    // ② Paramsの値をBundle型で返す
    public Bundle getBundleParams() {
        return mParams;
    }
}
