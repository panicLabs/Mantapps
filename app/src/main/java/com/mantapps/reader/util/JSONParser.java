package com.mantapps.reader.util;

import android.util.Log;

import com.mantapps.reader.model.Category;
import com.mantapps.reader.model.Post;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by Declan on 20/06/15.
 */
public class JSONParser {

    private static final String	TAG	= "JSONParser";

    /**
     * Parse JSON data and return an ArrayList of Category objects
     *
     * @param jsonObject JSON data
     */
    public static ArrayList<Category> parseCategories(JSONObject jsonObject) {
        ArrayList<Category> categoryArrayList = new ArrayList<>();

        try {
            // Get "categories" Json array
            JSONArray categories = jsonObject.getJSONArray("categories");

            // We need to add 1 here coz we will manually create an "All" category

            // Create "All" category
//            Category all = new Category();
//            all.setId(0);
//            all.setName(AppController.getInstance().getString(R.string.tab_all));
//            categoryArrayList.add(all);

            // Go through all categories and get their details
            for (int i=0; i<categories.length(); i++) {
                // Get individual category Json object
                JSONObject catObj = categories.getJSONObject(i);
                Log.d(TAG, "Parsing " + catObj.getString("title") + ", ID " + catObj.getInt("id"));
                Category c = new Category();
                c.setId(catObj.getInt("id"));
                c.setName(catObj.getString("title"));
                c.setParent(catObj.getInt("parent"));
                categoryArrayList.add(c);
            }
        } catch (JSONException e) {
            Log.d(TAG, "----------------- Json Exception");
            e.printStackTrace();
            return null;
        }

        return categoryArrayList;
    }

    /**
     * Parse JSON data and return an ArrayList of Post objects
     *
     * @param jsonObject JSON data
     */
    public static ArrayList<Post> parsePosts(JSONObject jsonObject) {
        ArrayList<Post> posts = new ArrayList<>();

        try{
            JSONArray postArray = jsonObject.getJSONArray("posts");
            // Go through each post
            for (int i = 0; i < postArray.length(); i++) {
                JSONObject postObject = postArray.getJSONObject(i);

                Post post = new Post();
                String type = postObject.getString("type");
                if (StringUtils.isSameIgnoreCase(type, "post")) {
                    // Configure the Post object
                    post.setTitle(postObject.optString("title", "N/A"));
                    // Use a default thumbnail image if one doesn't exist
                    post.setThumbnailUrl(postObject.optString("thumbnail",
                            Config.DEFAULT_THUMBNAIL_URL));
                    post.setSlug(postObject.optString("slug"));
                    //thumbnail_images
                    if(postObject.has("thumbnail_images")) post.setMediumImageUrl(
                            postObject.getJSONObject("thumbnail_images")
                            .getJSONObject("thumb-medium")//"thumb-medium")
                            .optString("url", Config.DEFAULT_THUMBNAIL_URL));

                    post.setCommentCount(postObject.optInt("comment_count", 0));
                    //post.setViewCount(postObject.getJSONObject("custom_fields")
                    //        .getJSONArray("post_views_count").getString(0));

                    post.setDate(postObject.optString("date", "N/A"));
                    post.setContent(postObject.optString("content", "N/A"));
                    post.setExcerpt(postObject.optString("excerpt", "N/A"));
                    post.setAuthor(postObject.getJSONObject("author").optString("name", "N/A"));
                    post.setAuthorId(postObject.getJSONObject("author").optInt("id", 0));
                    //post.setAuthor(postObject.getJSONObject("author").optString("name", "N/A"));
                    post.setId(postObject.optInt("id"));
                    post.setUrl(postObject.optString("url"));

                    //category
                    //categories
                    JSONArray categoryArray = postObject.getJSONArray("categories");
                    int categoryNum = categoryArray.length();
                    // StringBuilder categoriesBuf = new StringBuilder();
//                    for (int j = categoryNum-1; j >= 0; j--) {
//                        JSONObject cObject = categoryArray.getJSONObject(j);
//                        categoriesBuf.append(cObject.optString("title"));
//                        if (j != 0) {
//                            categoriesBuf.append(" / ");
//                        }
//                    }
//                    post.setCategoriesDesc(categoriesBuf.toString());

                    ArrayList<Category> clist = new ArrayList<Category>();
                    for (int j = 0; j < categoryNum; j++) {
                        JSONObject cObject = categoryArray.getJSONObject(j);
                        Category c = new Category();
                        c.setName(cObject.optString("title"));
                        c.setId(cObject.optInt("id"));
                        clist.add(c);
                    }

                    post.setCategories(clist);
                    posts.add(post);
                }
            }
        } catch (JSONException e) {
            Log.d(TAG, "----------------- Json Exception");
            Log.d(TAG, e.getMessage());
            return null ;
        }

        return posts;
    }
}
