package com.mantapps.reader.util;

/**
 * Configuration class
 */
public class    Config {
    // Fill in your own WordPress URL, don't forget the "/" at the end
    public static final String SERVER = "http://www.mantapps.co.id/";
    public static final String FEED = "http://www.mantapps.co.id/feed";

    public static final int BRAND_ID = 6891;
    public static final int NEWS_ID = 6628;
    public static final int TECH_ID = 6630;

    public static final Integer[] categoryIndex = {BRAND_ID,NEWS_ID,TECH_ID};


    public static final String BASE_URL = SERVER + "api/get_recent_posts/";
    public static final String PUSH = SERVER + "push/savetoken/?";

    public static final String POSTS_URL = SERVER + "jasp234iK2423DF932WER19/get_recent_posts/";
    public static final String CATEGORY_URL = SERVER + "jasp234iK2423DF932WER19/get_category_index/";//BASE_URL + "?json=get_category_index";
    public static final String CATEGORY_PORTS = SERVER + "jasp234iK2423DF932WER19/get_category_posts/?id=";
    public static final String CATEGORY_POST = SERVER + "jasp234iK2423DF932WER19/get_category_posts/";
    public static final String GET_POST_ID = SERVER + "jasp234iK2423DF932WER19/get_posts/?id=";
    public static final String AUTHOR_POST = SERVER + "jasp234iK2423DF932WER19/get_author_posts/?id=";
    public static final String SEARCH = SERVER + "jasp234iK2423DF932WER19/get_search_results/?search=";

    public static final String DEFAULT_THUMBNAIL_URL = "http://i.imgur.com/D2aUC3s.png";


    public static final String UPDATE_URL = SERVER + "apk/update.txt";
    public static final String SLUG = "jasp234iK2423DF932WER19/get_post/";
    public static final String CATEGORY_API = "jasp234iK2423DF932WER19/get_category_index/";
    public static final String IMG_ID = "IMG_ID_";
    public static final String FIRST_LEVEL = "FIRST_LEVEL";
    public static final String SECOND_LEVEL = "SECOND_LEVEL";
    public static final String POST_ID = "Post_Id_";
}
