//package com.mantapps.reader.util;
//
//import android.os.AsyncTask;
//
//import com.elvishew.xlog.XLog;
//import com.mantapps.reader.mvp.category.CategoryPresenter;
//import com.mantapps.reader.mvp.category.model.CategoriesItem;
//import com.paniclabs.libs.categoryview.SubItem;
//
//import java.util.ArrayList;
//import java.util.List;
//
///**
// * @author paniclabs.
// * @created on 2/2/17.
// * @email panic.inc.dev@gmail.com
// * @projectName mtps-n
// */
//public class ATHelper {
//    private static final String TAG = "ATHelper";
//
//    public interface OnCompletedListener {
//        void onResult(List<SubItem> result);
//
//        void onError();
//    }
//
//    public void downloadData(int url,CategoryPresenter mpresent, OnCompletedListener downloadListener) {
//        new MyTask(mpresent,downloadListener).execute(url);
//    }
//
//    class MyTask extends AsyncTask<Integer, Void, List<SubItem>> {
//        private final CategoryPresenter mPresenter;
//        private OnCompletedListener downloadListener;
//        private List<CategoriesItem> mList;
//
//        public MyTask(CategoryPresenter presenter, OnCompletedListener downloadListener) {
//            this.downloadListener = downloadListener;
//            this.mPresenter = presenter;
//            this.mList = new ArrayList<>();
//        }
//
//        @Override
//        protected List<SubItem> doInBackground(Integer... params) {
//            //mList = mPresenter.loadDataApi(params[0]);
//            List<SubItem> mItem = new ArrayList<>();
//            for (int i = 0; i < mList.size(); i++) {
//                SubItem item = new SubItem();
//                item.setTitle(mList.get(i).getTitle());
//                mItem.add(item);
//            }
//            return mItem;
//        }
//
//        @Override
//        protected void onPostExecute(List<SubItem> result) {
//            super.onPostExecute(result);
//            XLog.w(result.toString());
//            if (result != null && result.size() > 0) {
//                downloadListener.onResult(result);
//            }else downloadListener.onError();
//        }
//    }
//}
