/*
 * Copyright (C) 2012 Shenzhen SMT online Co., Ltd.
 * 
 * 项目名称:SMT移动信息化解决方案系统
 * 创建日期:2012-3-27
 */
package com.mantapps.reader.util;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.List;
import java.util.regex.Pattern;

/**
 * 字符串工具类
 *
 * @author gus@sinomaster.com
 * @version 1.0
 */
public class StringUtils {
    /**
     * 判断字符串是否为null或 ""
     *
     * @param str
     * @return
     */
    public static boolean isEmpty(String str) {
        if (str == null || str.equals("")) {
            return true;
        }
        return false;
    }

    /**
     * 对比两个字符串
     * StringUtils.equalsIgnoreCase(null, null)   = true
     * StringUtils.equalsIgnoreCase(null, "abc")  = false
     * StringUtils.equalsIgnoreCase("abc", null)  = false
     * StringUtils.equalsIgnoreCase("abc", "abc") = true
     * StringUtils.equalsIgnoreCase("abc", "ABC") = true
     *
     * @param str1
     * @param str2
     * @return
     */
    public static boolean isSame(String str1, String str2) {
        if (str1 == null || str2 == null) {
            return false;
        }
        if (str1 == str2 || str1.equals(str2)) {
            return true;
        }
        return false;
    }

    /**
     * 对比两个字符串,忽略大小写
     *
     * @param str1
     * @param str2
     * @return
     */
    public static boolean isSameIgnoreCase(String str1, String str2) {
        return str1 == null ? str2 == null : str1.equalsIgnoreCase(str2);
    }

    /*
     * 判断是否为整数
     *
     * @param str 传入的字符串
     * @return 是整数返回true,否则返回false
     */
    public static boolean isInteger(String str) {
        Pattern pattern = Pattern.compile("^[-\\+]?[\\d]*$");
        return pattern.matcher(str).matches();
    }

    /*
     * 判断是否为浮点数，包括double和float
     *
     * @param str 传入的字符串
     * @return 是浮点数返回true,否则返回false
     */
    public static boolean isDouble(String str) {
        Pattern pattern = Pattern.compile("^[-\\+]?[.\\d]*$");
        return pattern.matcher(str).matches();
    }

    /**
     * String类型转boolean类型
     *
     * @param str
     * @return
     */
    public static boolean stringToboolean(String str) {
        if (isSameIgnoreCase("true", str)) {
            return true;
        }
        return false;
    }

    /**
     * String类型转int类型
     *
     * @param str
     * @return
     */
    public static int stringToint(String str) {
        try {
            return Integer.parseInt(str);
        } catch (NumberFormatException e) {
            throw e;
        }
    }


    /**
     * 返回urlencode值
     *
     * @param str
     * @return
     */
    public static String urlEncode(String str) {
        try {
            return URLEncoder.encode(str, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            return "";
        }
    }

    /**
     * 返回UrlDecode值
     *
     * @param str
     * @return
     */
    public static String urldecode(String str) {
        try {
            return URLDecoder.decode(str, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            return "";
        }
    }

    /**
     * 转换为货币格式
     *
     * @param price
     * @return
     */
    public static String convert2Rp(int price) {
        return convert2Rp(String.valueOf(price));
    }

    /**
     * 转换为货币格式
     *
     * @param price
     * @return
     */
    public static String convert2Rp(String price) {
        if (null == price)
            return "";

        StringBuilder sb = new StringBuilder();
        int length = price.length();
        int count = length / 3;
        int startIndex = length % 3;
        if (startIndex > 0) {
            sb.append(price.substring(0, startIndex));
        }

        for (int i = 0; i < count; i++) {
            String item = "." + price.substring(startIndex, startIndex + 3);
            startIndex += 3;
            sb.append(item);
        }

        if (sb.indexOf(".") == 0) {
            sb = sb.replace(0, 1, "");
        }

        return sb.toString();
    }

    /**
     * 把字符串第一个字母大写     *
     *
     * @param source
     * @return
     */
    public static String getFirstUpper(String source) {
        source = source.toLowerCase();
        byte[] items = source.getBytes();
        items[0] = (byte) ((char) items[0] - 'a' + 'A');
        return new String(items);
    }

    /**
     * 链接集合
     *
     * @param list
     * @param separator
     * @return
     */
    public static String join(List<String> list, String separator) {
        StringBuilder sb = new StringBuilder();
        for (String item : list) {
            sb.append(separator + item);
        }

        String value = sb.toString();
        if (value.indexOf(separator) == 0) {
            value = value.substring(1);
        }

        return value;
    }

}
