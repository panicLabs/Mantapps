package com.mantapps.reader.presenter;

import com.elvishew.xlog.XLog;
import com.mantapps.reader.api.MantappsApi;
import com.mantapps.reader.app.CategoryListActivity;
import com.mantapps.reader.model.Category;
import com.mantapps.reader.model.Post;
import com.mantapps.reader.mvp.category.model.CategoriesItem;
import com.mantapps.reader.util.Config;
import com.mantapps.reader.util.JSONParser;
import com.mantapps.reader.util.RestUtil;
import com.mantapps.reader.util.StringUtils;
import com.mantapps.reader.view.CategoryImageView;
import com.mantapps.reader.view.CategoryView;
import com.paniclabs.libs.datastorage.DS;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.mantapps.reader.util.Config.FIRST_LEVEL;
import static com.mantapps.reader.util.Config.SECOND_LEVEL;

/**
 * Created by panicLabs on 2017/1/1.
 */
public class CategoryPresenter {

    private static final String TAG = CategoryPresenter.class.getName();

    CompositeDisposable compositeDisposable;
    private CategoryView categoryView;
    private CategoryImageView categoryImageView;
    public CategoryPresenter(CategoryView categoryView) {
        compositeDisposable = new CompositeDisposable();
        this.categoryView = categoryView;
    }

    public CategoryPresenter(CategoryImageView categoryImageView) {
        if(compositeDisposable == null) {
            compositeDisposable = new CompositeDisposable();
        }
        this.categoryImageView = categoryImageView;
    }

    public void getAllCategories() {

        if(DS.isExist(FIRST_LEVEL) && DS.isExist(SECOND_LEVEL)){
            categoryView.hideProgress();
            CategoryListModel listModel = new CategoryListModel();
            listModel.setFirstLevel(DS.grab(FIRST_LEVEL));
            listModel.setSecondLevel(DS.grab(SECOND_LEVEL));

            onNext(listModel);
        }else {
            getCategory();
        }

    }

    public void getCategory() {
        categoryView.showProgress();
        Disposable disp = RestUtil.withRx().getCategoryPosts(Config.CATEGORY_URL)
                .map(new Function<Response<ResponseBody>, CategoryListModel>() {
                    /**
                     * Apply some calculation to the input value and return some other value.
                     *
                     * @param response the input value
                     * @return the output value
                     * @throws Exception on error
                     */
                    @Override
                    public CategoryListModel apply(@NonNull Response<ResponseBody> response) throws Exception {
                        CategoryListModel listModel = new CategoryListModel();
                        if (response.isSuccessful()) {
                            try {
                                JSONObject jsonObject = new JSONObject(response.body().string());
                                ArrayList<Category> all = JSONParser.parseCategories(jsonObject);
                                ArrayList<Category> firstLevel = new ArrayList<>();
                                ArrayList<Category> secondLevel = new ArrayList<>();
                                assert all != null;
                                for (Category ca : all) {
                                    if (ca.getParent() == 0) {
                                        firstLevel.add(ca);
                                    } else {
                                        secondLevel.add(ca);
                                    }
                                }

                                DS.entry(FIRST_LEVEL, firstLevel);
                                DS.entry(SECOND_LEVEL, secondLevel);
                                listModel.setFirstLevel(firstLevel);
                                listModel.setSecondLevel(secondLevel);
                            } catch (JSONException | IOException e) {
                                e.printStackTrace();
                            }
                        }
                        return listModel;
                    }
                })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(categoryListModel -> {
                    categoryView.hideProgress();
                    if (categoryListModel.getFirstLevel() != null && categoryListModel.getSecondLevel() != null) {
                        onNext(categoryListModel);
                    } else {
                        categoryView.onFailure("Network error. Please try again later...");
                    }
                }, throwable -> {
                    categoryView.hideProgress();
                    categoryView.onFailure("Network error. Please try again later...");
                });

        compositeDisposable.add(disp);
    }

    private void onNext(CategoryListModel listModel) {
        categoryView.onSuccessCategories(listModel.getFirstLevel(), listModel.getSecondLevel());
    }

    public static ArrayList<Category> getSelfSecondLevelList(Category category, List<Category> secondLevel) {
        ArrayList<Category> selfSecondLevel = new ArrayList<>();
        for(Category ca : secondLevel) {
            if(ca.getParent() == category.getId()) {
                selfSecondLevel.add(ca);
            }
        }
        return selfSecondLevel;
    }

    public void getPosts(int categoryID, String page, String count) {
        categoryView.showProgress();

        String url = Config.CATEGORY_PORTS + categoryID;
        if(!StringUtils.isEmpty(page)){
            url = url + "&page=" + page;
        }
        if(!StringUtils.isEmpty(count)){
            url = url + "&count=" + count;
        }

        if(DS.isExist(Config.POST_ID + categoryID)){
            categoryView.onSuccessPosts(DS.grab(Config.POST_ID + categoryID));
        }else {
            Disposable dips = RestUtil.withRx().getCategoryPosts(url)
                    .map(response -> {
                        ArrayList<Post> listPost = new ArrayList<>();
                        if (response.isSuccessful()) {
                            try {
                                JSONObject jsonObject = new JSONObject(response.body().string());
                                XLog.w("hasil " + jsonObject.toString());
                                listPost.addAll(JSONParser.parsePosts(jsonObject));
                            } catch (JSONException | IOException e) {
                                e.printStackTrace();
                            }
                        }
                        return listPost;
                    })
                    .subscribeOn(Schedulers.computation())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(posts -> {
                        categoryView.hideProgress();
                        if (posts != null && !posts.isEmpty()) {

                            categoryView.onSuccessPosts(posts);
                        } else {
                            categoryView.onFailure("Network error. Please try again later...");
                        }
                    }, throwable -> {
                        categoryView.hideProgress();
                        categoryView.onFailure("Network error. Please try again later...");
                    });

            compositeDisposable.add(dips);
        }

    }

    public void getCategoryTempPic(int categoryID) {
        CategoryPresenter cp = new CategoryPresenter(new CategoryView() {
            @Override
            public void showProgress() { }
            @Override
            public void hideProgress() { }
            @Override
            public void onSuccessCategories(ArrayList<Category> firstLevel, ArrayList<Category> secondLevel) { }
            @Override
            public void onSuccessPosts(ArrayList<Post> list) {
                if (list== null || list.size() == 0) {
                    return;
                }

                DS.entry(Config.IMG_ID + categoryID, list.get(0).getMediumImageUrl());
                categoryImageView.onSuccessCategoryTempPicUrl(list.get(0).getMediumImageUrl());
            }
            @Override
            public void onFailure(String errMsg) { }
        });

        if(DS.isExist(Config.IMG_ID + categoryID)){
            categoryImageView.onSuccessCategoryTempPicUrl(DS.grab(Config.IMG_ID + categoryID));
        }else {
            cp.getPosts(categoryID, "1", "1");
        }

    }


    public void clearDispose(){
        compositeDisposable.clear();
    }
}
