//package com.mantapps.reader.presenter;
//
//import android.content.Context;
//import android.util.Log;
//import android.widget.Toast;
//
//import com.android.volley.DefaultRetryPolicy;
//import com.android.volley.Request;
//import com.android.volley.Response;
//import com.android.volley.VolleyError;
//import com.android.volley.toolbox.JsonObjectRequest;
//import com.mantapps.reader.app.AppController;
//import com.mantapps.reader.model.Post;
//import com.mantapps.reader.util.Config;
//import com.mantapps.reader.util.JSONParser;
//import com.mantapps.reader.view.PostView;
//
//import org.json.JSONObject;
//
//import java.util.List;
//
//import static com.mantapps.reader.util.JSONParser.parsePosts;
//
///**
// * Created by gus on 15/8/19.
// */
//public class PostPresenter {
//    private static final String TAG = PostPresenter.class.getName();
//
//    private PostView mPostView;
//    private List<Post> mPosts;
//    private Context context;
//    public PostPresenter(Context context, PostView postView, List<Post> posts) {
//        this.context = context;
//        mPostView = postView;
//        mPosts = posts;
//    }
//    /**
//     * Load posts from a specific page number
//     *
//     * @param page Page number
//     * @param isLoadMore
//     */
//    public void loadPosts(final int page, final boolean isLoadMore) {
//        Log.d(TAG, "----------------- Loading All News, page:" + String.valueOf(page));
//
//        final int scrollPage = page *4;
//        if (isLoadMore && mPosts.size() >= scrollPage) {
//            mPostView.hideProgress();
//            mPostView.onRefreshData();
//            return;
//        }
//
//        mPostView.showProgress();
//        // Construct the proper API Url
//        String url = Config.POSTS_URL + "?json=get_posts&page=" + String.valueOf(page);
//
//        Log.d(TAG, url);
//        // Request post JSON
//        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, url, null,
//                new Response.Listener<JSONObject>() {
//                    @Override
//                    public void onResponse(JSONObject jsonObject) {
//                        mPostView.hideProgress();
//                        if (page == 1) {
//                            mPosts.clear();
//                        }
//                        mPosts.addAll(JSONParser.parsePosts(jsonObject));
//                        mPostView.onRefreshData();
//                    }
//                },
//                new Response.ErrorListener() {
//                    @Override
//                    public void onErrorResponse(VolleyError volleyError) {
//                        mPostView.hideProgress();
//                        volleyError.printStackTrace();
//                        Log.d(TAG, "----- Error: " + volleyError.getMessage());
//                        Toast.makeText(context, "Network error. Please try again later...",
//                                Toast.LENGTH_SHORT).show();
//                    }
//                });
//
//        // Set timeout to 10 seconds instead of the default value 5 since my
//        // crappy server is quite slow
//        request.setRetryPolicy(new DefaultRetryPolicy(10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
//                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
//
//        // Add request to request queue
//        AppController.getInstance().addToRequestQueue(request, TAG);
//    }
//
//}
