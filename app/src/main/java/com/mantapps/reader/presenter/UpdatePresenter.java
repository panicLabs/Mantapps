package com.mantapps.reader.presenter;


import android.util.Log;

import com.elvishew.xlog.XLog;
import com.mantapps.reader.api.MantappsApi;
import com.mantapps.reader.model.VersionInfo;
import com.mantapps.reader.util.Config;
import com.mantapps.reader.util.JsonUtils;
import com.mantapps.reader.util.RestUtil;
import com.mantapps.reader.view.UpdateView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;

/**
 * Created by panicLabs on 2017/1/1.
 */
public class UpdatePresenter {

    private static final String TAG = UpdatePresenter.class.getName();

    private UpdateView updateView;

    public UpdatePresenter(UpdateView updateView){
        this.updateView = updateView;
    }
    public void getVersion(){
        if(updateView != null){
            updateView.showProgress();
        }
        Map<String, String> params = new HashMap<>();

        RestUtil.getInstance().create(MantappsApi.class).getUpdate(Config.UPDATE_URL).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, retrofit2.Response<ResponseBody> response) {
                if(response.isSuccessful()) {

                    try {
                        JSONObject jsonObject = new JSONObject(response.body().string());
                        XLog.w("hasil " + jsonObject.toString());

                        updateView.hideProgress();
                        VersionInfo info = JsonUtils.fromJson(jsonObject.toString(), VersionInfo.class);
                        updateView.onSuccess(info);
                    } catch (JSONException | IOException e) {
                        e.printStackTrace();
                        updateView.hideProgress();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.e(TAG, "----- Error: " + t.getMessage());
                updateView.hideProgress();
                updateView.onFailure("Network error. Please try again later...");
            }
        });

    }



}
