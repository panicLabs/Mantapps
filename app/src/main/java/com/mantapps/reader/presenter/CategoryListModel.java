package com.mantapps.reader.presenter;

import com.mantapps.reader.model.Category;

import java.util.ArrayList;
import java.util.List;

/**
 * @author ali@pergikuliner
 * @created 7/28/17.
 * @project Mantapps.
 */

public class CategoryListModel {
    private ArrayList<Category> firstLevel;
    private ArrayList<Category> secondLevel;

    public void setFirstLevel(ArrayList<Category> firstLevel) {
        this.firstLevel = firstLevel;
    }

    public ArrayList<Category> getFirstLevel() {
        return firstLevel;
    }

    public void setSecondLevel(ArrayList<Category> secondLevel) {
        this.secondLevel = secondLevel;
    }

    public ArrayList<Category> getSecondLevel() {
        return secondLevel;
    }
}
