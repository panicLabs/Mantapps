package com.mantapps.reader.presenter;

import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;

import com.mantapps.reader.app.MainActivity;
import com.mantapps.reader.model.VersionInfo;
import com.mantapps.reader.view.UpdateView;


public class UpdateVersion {

    private int mVersionCode;
    private  String downUrl;
    private boolean force;

    private final MainActivity mainActivity;

    public UpdateVersion(MainActivity activity) {
        mainActivity = activity;
    }

    public void checkVersion(){
        try {
            PackageInfo pf = mainActivity.getPackageManager().getPackageInfo(mainActivity.getPackageName(), 0);
            int versioncode = pf.versionCode;
            if(versioncode < mVersionCode){
                mainActivity.showNoticeDialog(downUrl, force);
            }
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
    }

    UpdatePresenter updatePresenter = new UpdatePresenter(new UpdateView() {
        @Override
        public void showProgress() { }
        @Override
        public void hideProgress() { }
        @Override
        public void onSuccess(VersionInfo info) {
            mVersionCode = info.getVesionNumber();
            downUrl = info.getDownUrl();
            force = info.isForce();
            checkVersion();
        }
        @Override
        public void onFailure(String errMsg) { }
    });

    public void update(){
        updatePresenter.getVersion();
    }

}
