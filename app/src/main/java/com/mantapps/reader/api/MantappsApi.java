package com.mantapps.reader.api;

import com.mantapps.reader.model.slug.SlugResponse;
import com.mantapps.reader.mvp.category.model.CategoryIndexResponse;
import com.mantapps.reader.util.Config;

import io.reactivex.Flowable;
import io.reactivex.Observable;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.http.GET;
import retrofit2.http.Query;
import retrofit2.http.Url;

/**
 * Created by paniclabs on 11/13/16.
 */

public interface MantappsApi {

    @GET(Config.SLUG)
    Call<SlugResponse> getSlug(@Query("slug") String slug);
    //public void getSlug(@Query("slug") String slug, Callback<SlugResponse> responseCallback);

    @GET()
    Call<ResponseBody> getRecentPost(@Url String url);

    @GET()
    Flowable<Response<ResponseBody>> getRecentPosts(@Url String url);

    @GET(Config.CATEGORY_API)
    Call<CategoryIndexResponse> getCategoryIndex(@Query("parent") int id);

    @GET(Config.CATEGORY_API)
    Flowable<CategoryIndexResponse> getCategoryIndexs(@Query("parent") int id);

    @GET()
    Call<ResponseBody> getCategoryPost(@Url String url);

    @GET()
    Flowable<Response<ResponseBody>> getCategoryPosts(@Url String url);

    @GET(Config.CATEGORY_POST)
    Flowable<Response<ResponseBody>> getCatePost(@Query("id") int id, @Query("page") String page, @Query("count") String count);

    @GET(Config.CATEGORY_POST)
    Observable<Response<ResponseBody>> getKategoriPost(@Query("id") int id, @Query("page") String page, @Query("count") String count);


    @GET()
    Call<ResponseBody> getUpdate(@Url String url);

    @GET()
    Call<ResponseBody> getGCM(@Url String url);

    @GET()
    Call<ResponseBody> getSearch(@Url String url);


    @GET()
    Call<ResponseBody> getAuthotPost(@Url String url);
}
