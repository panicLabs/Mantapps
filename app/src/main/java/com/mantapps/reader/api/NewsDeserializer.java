//package com.mantapps.reader.api;
//
//import com.google.gson.Gson;
//import com.google.gson.JsonDeserializationContext;
//import com.google.gson.JsonElement;
//import com.google.gson.JsonObject;
//import com.google.gson.JsonParseException;
//import com.mantapps.reader.model.News;
//
//import java.lang.reflect.Type;
//
///**
// * Created by PanicLabs on 11/21/2016.
// */
//
//public class NewsDeserializer implements JsonDeserializer<News> {
//    @Override
//    public News deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
//        Gson gson = new Gson();
//        JsonObject news = json.getAsJsonObject();
//
//        return extractNewsFromJsonArray(news);
//    }
//
//    private News extractNewsFromJsonArray(JsonObject item){
//
//        News currentNews = new News();
//        currentNews.setTitle(item.get(JsonKeys.NEWS_TITLE).getAsString());
//        currentNews.setImage(item.get(JsonKeys.NEWS_IMAGES_URL).getAsString());
//        currentNews.setContent(item.get(JsonKeys.NEWS_CONTENT).getAsJsonObject().get(JsonKeys.RENDERED).getAsString());
//        currentNews.setId(item.get(JsonKeys.NEWS_ID).getAsInt());
//
//        return currentNews;
//    }
//}
