ECLIPSE ANDROID PROJECT IMPORT SUMMARY
======================================

Ignored Files:
--------------
The following files were *not* copied into the new Gradle project; you
should evaluate whether these are still needed in your project and if
so manually move them:

* doc/
* doc/allclasses-frame.html
* doc/allclasses-noframe.html
* doc/com/
* doc/com/mtburn/
* doc/com/mtburn/android/
* doc/com/mtburn/android/sdk/
* doc/com/mtburn/android/sdk/AppDavis.ADVSInitStatusType.html
* doc/com/mtburn/android/sdk/AppDavis.html
* doc/com/mtburn/android/sdk/appimp/
* doc/com/mtburn/android/sdk/appimp/ADVSAppImpLoadListener.html
* doc/com/mtburn/android/sdk/appimp/ADVSAppImpLoader.html
* doc/com/mtburn/android/sdk/appimp/AppImpLoaderImpl.html
* doc/com/mtburn/android/sdk/appimp/package-frame.html
* doc/com/mtburn/android/sdk/appimp/package-summary.html
* doc/com/mtburn/android/sdk/appimp/package-tree.html
* doc/com/mtburn/android/sdk/icon/
* doc/com/mtburn/android/sdk/icon/ADVSIconAdLoadListener.html
* doc/com/mtburn/android/sdk/icon/ADVSIconAdLoader.html
* doc/com/mtburn/android/sdk/icon/ADVSIconAdViewListener.html
* doc/com/mtburn/android/sdk/icon/IconAdLoaderImpl.html
* doc/com/mtburn/android/sdk/icon/IconAdView.html
* doc/com/mtburn/android/sdk/icon/package-frame.html
* doc/com/mtburn/android/sdk/icon/package-summary.html
* doc/com/mtburn/android/sdk/icon/package-tree.html
* doc/com/mtburn/android/sdk/instream/
* doc/com/mtburn/android/sdk/instream/ADVSInstreamAdAdapter.html
* doc/com/mtburn/android/sdk/instream/ADVSInstreamAdLoadListener.html
* doc/com/mtburn/android/sdk/instream/ADVSInstreamAdPlacer.html
* doc/com/mtburn/android/sdk/instream/ADVSInstreamAdPlacerListener.html
* doc/com/mtburn/android/sdk/instream/ADVSInstreamAdViewBinder.html
* doc/com/mtburn/android/sdk/instream/InstreamAdAdapterImpl.html
* doc/com/mtburn/android/sdk/instream/InstreamAdPlacerImpl.html
* doc/com/mtburn/android/sdk/instream/InstreamAdViewBinderImpl.html
* doc/com/mtburn/android/sdk/instream/package-frame.html
* doc/com/mtburn/android/sdk/instream/package-summary.html
* doc/com/mtburn/android/sdk/instream/package-tree.html
* doc/com/mtburn/android/sdk/model/
* doc/com/mtburn/android/sdk/model/ADVSClickURLInfoModel.html
* doc/com/mtburn/android/sdk/model/ADVSInstreamInfoModel.html
* doc/com/mtburn/android/sdk/model/ADVSInstreamWebViewInfoModel.html
* doc/com/mtburn/android/sdk/model/AppConfInfoModel.IconAdConf.html
* doc/com/mtburn/android/sdk/model/AppConfInfoModel.WallAdConf.html
* doc/com/mtburn/android/sdk/model/AppConfInfoModel.html
* doc/com/mtburn/android/sdk/model/IconInfoModelImpl.html
* doc/com/mtburn/android/sdk/model/InstreamInfoModelImpl.html
* doc/com/mtburn/android/sdk/model/WallInfoModelImpl.html
* doc/com/mtburn/android/sdk/model/package-frame.html
* doc/com/mtburn/android/sdk/model/package-summary.html
* doc/com/mtburn/android/sdk/model/package-tree.html
* doc/com/mtburn/android/sdk/package-frame.html
* doc/com/mtburn/android/sdk/package-summary.html
* doc/com/mtburn/android/sdk/package-tree.html
* doc/com/mtburn/android/sdk/wall/
* doc/com/mtburn/android/sdk/wall/ADVSWallAdLoadListener.html
* doc/com/mtburn/android/sdk/wall/ADVSWallAdLoader.html
* doc/com/mtburn/android/sdk/wall/WallAdActivity.html
* doc/com/mtburn/android/sdk/wall/WallAdLoaderImpl.html
* doc/com/mtburn/android/sdk/wall/package-frame.html
* doc/com/mtburn/android/sdk/wall/package-summary.html
* doc/com/mtburn/android/sdk/wall/package-tree.html
* doc/constant-values.html
* doc/deprecated-list.html
* doc/help-doc.html
* doc/index-all.html
* doc/index.html
* doc/overview-frame.html
* doc/overview-summary.html
* doc/overview-tree.html
* doc/package-list
* doc/resources/
* doc/resources/background.gif
* doc/resources/tab.gif
* doc/resources/titlebar.gif
* doc/resources/titlebar_end.gif
* doc/stylesheet.css
* proguard-project.txt

Moved Files:
------------
Android Gradle projects use a different directory structure than ADT
Eclipse projects. Here's how the projects were restructured:

* AndroidManifest.xml => mTBurnAndroidSDKRelease/src/main/AndroidManifest.xml
* libs/mtburn-android-sdk-release.jar => mTBurnAndroidSDKRelease/libs/mtburn-android-sdk-release.jar
* res/ => mTBurnAndroidSDKRelease/src/main/res/
* src/ => mTBurnAndroidSDKRelease/src/main/java/

Next Steps:
-----------
You can now build the project. The Gradle project needs network
connectivity to download dependencies.

Bugs:
-----
If for some reason your project does not build, and you determine that
it is due to a bug or limitation of the Eclipse to Gradle importer,
please file a bug at http://b.android.com with category
Component-Tools.

(This import summary is for your information only, and can be deleted
after import once you are satisfied with the results.)
